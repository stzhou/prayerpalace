# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
* A web-based application that manage prayer singups.
* email (and possibly phone) account.
* Prayer Session Leads
* Time Slot Picker
* Multi-language and Time Zone
* Mobile View Available

### Technical choices:
  * MYSQL as database. popular, open source, multiple platform, very available on cloud provider, docker, etc.
  * BackEnd will use golang. Extremely fast, no dependency. Simple and feature stable for decades. Cross platform.
    (I ended up not using dotnet core because I am Sr. dotnet developer, but adapting to .net core's advancement takes more time than go, dotnet culture is always overcomplicating things)
	+ use github.com/julienschmidt/httprouter as server engine.
    + use github.com/shiyanhui/hero/hero as template engine, --> it doesn't use {}, never conflict with any javascript template.
  * FrontEnd will use 
    + ReactJS (small and fast) syntax is easier than angular + RXJS for global events.
    + Kendo UI core for widgets (Top quality, open source)
    + ECMA 2017. I will not use typescript for faster development.
    + Webpack.
  * Dev Environment
    I use Mac, VSCode.
  
### How do I get set up? ###

* Summary of set up
- You need to have either Linux or Mac OSX platform, build-script is bash.
- Nodejs 5.+ 
- Golang 1.7+, $GOPATH should be setup in your
- git

* Configuration
The 'global' tool required to build go application.
1. set up go environment and $GOPATH
2. install go vendor, this should put binary in your $GOPATH/bin/

```bash
go get -u github.com/kardianos/govendor
```

3. install go hero, this should put two binary in your $GOPATH/bin/

```bash
go get github.com/shiyanhui/hero/hero
go get golang.org/x/tools/cmd/goimports
```

4. make sure $GOPATH/bin/ in your bash $PATH

5. Consulting nodejs doc for installing nodejs and npm
If on Mac, you can install either by direct downloading from nodejs official installer, or by homebrew.
If on Linux, recommand official installer, your distro's packagemange may not have up-to-date node version.

6. install global nodejs tool
+ for minification

```bash
npm install -g uglify-es
```

* Dependencies
- golang dependency is managed by govendor at ./webservice/prayerpalace/vendor/
install in vendor/
```bash
govendor install +local
```
- nodejs dependency is managed by npm at ./webservice/wwww/
install in www folder:
```bash
npm install
```

* dev build instructions
TBA

* Database configuration
you need mysql 5.x version.
You can use MySQL Workbench to import the schema from ./database/schema.sql
TODO: Add deployment built-in data to db.

* How to run tests
TBA

* Deployment instructions
To deploy fully built go and javascript project, copy the go binary (usually at your $GOPATH/ ),
and copy the webpack built folder webservice/www/dist into same directory, so folder structure will be 

```
..
prayerpalace. # your go binary
www/
   dist/
```

# AWS Deployment 
## Install Elastic Beanstalk CommandLine
Description: Use eb-cli to check deployed application, read logs and use eb-cli's ssh channel to map mysql database url to localhost, so myql database will not expose to public ip, and local mysql-workbench GUI tool can maintain database.


## Installation of eb-cli
Detailed steps are documented by aws. Here are critical steps for current use case.
### (1) run the pip python script by aws to install eb-cli and add eb-cli tools to path as instruction.
### (2) initialize eb-cli tool to production account
* 2.1 Generate the aws account level access, go to your acocunt -> "Your Security Credentials" -> "Create New Access Key", the took will generate accountID and accountKey, eb will need this to log you in, via ssh on top of ssh certificate. The account level info will be saved to $HOME/.aws/ path of your Linux OS.
* 2.2 run > eb init  
if you don't know what data-center (region) name, log into aws-website, look at URL of the instances. For this case it is "us-east-2". eb init will save the rest of settings in $HOME/.elasticbeanstalks/config.yml
* 2.3 when eb init ask enable ssh or not, choose YES.
### (3) eb cli will prompt you to generate ssh key, you can use your own or eb cli will generate for you. Either way, you have to upload your public ssh key to your account level security settings. you can also copy the content of public key instead of importing the file.
* 3.1 before using ssh, chmod 400 the private key. 
### (4) in AWS console, click "connection", it will instruct to setup ssh and upload public certificate, if you already did it, it will show you the example of ssh command, you will be prompt if keypair is not yet selected, you have just did is only uploading the certificate.
### (5) go to the elasticbeanstalk/Configuration to select the keypair
After adding Key-pairs, it takes minutes to restart.
Then in EC2 Dashboard Instances Panel, "connect" will list ssh command to connect.
### (6) enable ssh port

By default, you can't ssh into elasticbeanstalk, the port 22 is not open.

* 6.1 create an security group, and set elasticbeanstalk (the web application) to use it, and in the group, open TCP port 22 for inbound.

* 6.2. set elasticbeanstalk (eb) to use the security group.

* 6.3. Assuming you have created an RDS instance and running mysql there and you have already setup master username and password.
Go to create another Security Group, and at inbound rule field, fill in the security group name of group used by eb, and set TCP inbound rule to allow port 3306.

### (7) Set Mysql instance to use this rule, so the inbound query from eb can open 3306 port of mysql.
* 7.1 tunnelling 
```bash
# Database Deployment using ssh tunnelling
# creat this bash script to open the tunnel, terminate this script will close the tunnel.
# URL can be found when you login "aws console" via web interface.
mysqlURL=xxxxx.xxxxxxxx.us-east-2.rds.amazonaws.com
ebURL=ec2-user@xxxxxxx.us-east-2.compute.amazonaws.com
localPORT=3307
ssh -i ~/.ssh/aws-eb -L $localPORT:$mysqlURL:3306 $ebURL
```
* 7.2 now open the production mysql database by Mysql Workbench via localhost:3307
using your master username and master password, you need master credential to create database.
* 7.3 Create database, populate default tables and values using deployment scripts.
* 7.4 Create an application user that can only select/insert/delete data and execute stored procedure. This will be used by web application.


### Who do I talk to? ###

* shermantzhou@gmail.com

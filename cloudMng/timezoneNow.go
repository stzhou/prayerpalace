package main

import (
	"fmt"
	"os"
	"time"
)

var TIME_FORMAT string = "2006/1/2 15:04:05"

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Expect a timezone, but not provided, exit now")
		return
	}
	args1 := os.Args[1:]

	locStr := args1[0]
	if len(locStr) == 0 {
		fmt.Println("Expect a timezone, but not provided, exit now.")
		return
	} else {
		fmt.Printf("Use timezone %s\n", locStr)
	}
	location, err := time.LoadLocation(locStr)
	if err != nil {
		fmt.Println("The os could not find the timezone, exit now.")
		return
	}
	timeNow := time.Now().In(location)
	fmt.Printf("Current time in this timezone is %s\n", timeNow.Format(TIME_FORMAT))
}

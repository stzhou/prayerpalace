# download aws eb console, use accesskey and accesspassword to init, then use eb ssh to generate certificate.
# this will connect to eb network (routed to db internally)
##ssh -i ~/.ssh/aws-eb  ec2-user@prayerpalace-dev.us-east-2.elasticbeanstalk.com
# this will map localhost:3307 to remotehost:mysqldb:3306 via the aws-eb network.
ssh -i ~/.ssh/aws-eb -L 3307:mysql57.ce2knidwwc1y.us-east-2.rds.amazonaws.com:3306 \
    ec2-user@prayerpalace-dev.us-east-2.elasticbeanstalk.com

------  Language ------
Insert into Language (LanID, LanName, LanCode) Values
( 1, 'En', 'en'),
( 2, N'简体中文','zh'),
( 3, N'繁體中文','zh-tw');

------ Operation ------
Insert into Operation (OperationID, OperationName, TransID) Values
(1, 'Edit-PrayerSession', null),
(2, 'Edit-UserRole', null),
(3, 'Activate-User', null),
(4, 'Assign-Leader', null),
(5, 'Clear-Cache', null);

------ PrayerCat ------
Insert into PrayerCat (PrayerCatID, CatName, TransID) Values
 (1, 'General', 1),
 (2, 'Kingdom', 2),
 (3, 'Local Church', 3),
 (4, 'Personal', 4);

------ RecurringMode -------
Insert into RecurringMode (ModeId, ModeName, TransID) Values
(1, 'By Day of Month', null),
(2, 'By Week', null),
(3, 'By Biweek', null),
(4, 'By Weekday of Month', null);

------ Role ------
Insert into Role (RoleID, RoleName, TransID, IsBuiltIn) Values
(1,'Admin', null, 1),
(2,'Manager', null, 1),
(3,'Default', null, 1),
(4,'Leader', null, 1);

------ TimeZone ------
Insert into TimeZone (ID, ZoneCode, `Name`, TransID) Values
(1, 'America/Toronto', 'Eastern Time (Toronto)', null),
(2, 'Asia/Beijing', 'Beijing Time UTC+9', null);

------ WkdOfMonth ------
Insert into WkdOfMonth (`ID`,`RecurringName`,`TransID`) Values
(1,'1st Sunday',NULL),
(2,'1st Monday',NULL),
(3,'1st Tuesday',NULL),
(4,'1st Wednesday',NULL),
(5,'1st Thursday',NULL),
(6,'1st Friday',NULL),
(7,'1st Saturday',NULL),
(8,'2nd Sunday',NULL),
(9,'2nd Monday',NULL),
(10,'2nd Tuesday',NULL),
(11,'2nd Wednesday',NULL),
(12,'2nd Thursday',NULL),
(13,'2nd Friday',NULL),
(14,'2nd Saturday',NULL),
(15,'3rd Sunday',NULL),
(16,'3rd Monday',NULL),
(17,'3rd Tuesday',NULL),
(18,'3rd Wednesday',NULL),
(19,'3rd Thursday',NULL),
(20,'3rd Friday',NULL),
(21,'3rd Saturday',NULL),
(22,'4th Sunday',NULL),
(23,'4th Monday',NULL),
(24,'4th Tuesday',NULL),
(25,'4th Wednesday',NULL),
(26,'4th Thursday',NULL),
(27,'4th Friday',NULL),
(28,'4th Saturday',NULL),
(29,'5th Sunday',NULL),
(30,'5th Monday',NULL),
(31,'5th Tuesday',NULL);


---- Translations ----
Insert INTO Translation (`TransID`, `Lan2`, `Lan3`) Values
(1, N'综合类', N'綜合類'),
(2, N'国家区域类', N'國家區域類'),
(3, N'神的国度类', N'神的國度類'),
(4, N'个体类', N'個體類');

---- RoleOperation ----
Insert Into RoleOperation (`RoleID`, `OperationID`) Values
VALUES (1,1),
VALUES (2,1),
VALUES (4,1),
VALUES (1,2),
VALUES (2,2),
VALUES (1,3),
VALUES (2,3),
VALUES (1,4),
VALUES (2,4),
VALUES (1,5),
VALUES (2,5),
VALUES (4,5);

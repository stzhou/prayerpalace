CREATE PROCEDURE `CopySigners`(fromSessionID int)
BEGIN
-- Given a sessionID, all user signed up to that session, will be signed up to all its copied sessions,
-- with necessary user signup effective range check.
DROP TEMPORARY TABLE IF EXISTS CopiedSession;
Create temporary Table CopiedSession (FromSID int not null, ToSID int not null, Date DateTime) ENGINE=MEMORY;
	-- find all active, not-deleted and copied prayer sessions by parentID.
	Insert into CopiedSession (FromSID, toSID, `Date`)
	Select fromSessionID, SessionID, `Date` From PrayerSession
	Where CopiedFrom = fromSessionID
	AND IsActive = 1 AND deleted = 0
    ;

-- Select * from CopiedSession;

    Insert INTO SessionSignup(SessionID, SignerID, EffectiveTo, Updated)
	Select distinct cs.toSID, ss.SignerID, ss.EffectiveTo, utc_timestamp() as Updated From SessionSignup AS ss
	INNER JOIN CopiedSession AS cs ON cs.FromSID = ss.SessionID
	-- user effectiveDate check here.
	where ( ss.EffectiveTo is null OR (ss.EffectiveTo <= cs.Date) )
	-- prevent duplicated key insert exception:
	AND Not Exists (Select 1 From SessionSignup as t1 where t1.SessionID = cs.toSID AND t1.SignerID = ss.SignerID)
	;
DROP TEMPORARY TABLE CopiedSession;
END
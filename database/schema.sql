
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `PrayerPalace` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;

USE `PrayerPalace`;
DROP TABLE IF EXISTS `Alert`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Alert` (
  `AlertID` int(11) NOT NULL AUTO_INCREMENT,
  `AuthorID` int(11) NOT NULL,
  `Created` datetime NOT NULL,
  `Title` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `Content` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `Effective` datetime NOT NULL,
  `EffectiveTo` datetime DEFAULT NULL,
  `isDraft` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`AlertID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Language` (
  `LanID` int(11) NOT NULL,
  `LanName` varchar(45) COLLATE utf8_bin NOT NULL,
  `LanCode` varchar(45) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`LanID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Operation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Operation` (
  `OperationID` int(11) NOT NULL,
  `OperationName` varchar(45) COLLATE utf8_bin NOT NULL,
  `TransID` int(11) DEFAULT NULL,
  PRIMARY KEY (`OperationID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `PrayerCat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PrayerCat` (
  `PrayerCatID` int(11) NOT NULL,
  `CatName` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `TransID` int(11) DEFAULT NULL,
  PRIMARY KEY (`PrayerCatID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `PrayerSession`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PrayerSession` (
  `SessionID` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(200) COLLATE utf8_bin NOT NULL,
  `Description` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `CreatedBy` int(11) NOT NULL,
  `Date` datetime NOT NULL,
  `TimeZoneID` int(11) NOT NULL DEFAULT '1',
  `FromHH` tinyint(4) NOT NULL,
  `ToHH` tinyint(4) NOT NULL,
  `PrayerCatID` int(11) NOT NULL,
  `ColorCode` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `RecurringToDate` datetime DEFAULT NULL,
  `IsActive` tinyint(1) NOT NULL DEFAULT '0',
  `Created` datetime NOT NULL,
  `CopiedFrom` int(11) DEFAULT NULL,
  `Recurring` json DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`SessionID`),
  KEY `PrayerCatID` (`PrayerCatID`),
  KEY `CopiedFrom` (`CopiedFrom`),
  KEY `CreatedBy` (`CreatedBy`),
  KEY `TimeZoneID` (`TimeZoneID`),
  CONSTRAINT `PrayerSession_ibfk_1` FOREIGN KEY (`PrayerCatID`) REFERENCES `PrayerCat` (`PrayerCatID`),
  CONSTRAINT `PrayerSession_ibfk_2` FOREIGN KEY (`CopiedFrom`) REFERENCES `PrayerSession` (`SessionID`),
  CONSTRAINT `PrayerSession_ibfk_3` FOREIGN KEY (`CreatedBy`) REFERENCES `User` (`UserID`),
  CONSTRAINT `PrayerSession_ibfk_4` FOREIGN KEY (`TimeZoneID`) REFERENCES `TimeZone` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `RecurringMode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RecurringMode` (
  `ModeID` int(11) NOT NULL,
  `ModeName` varchar(45) COLLATE utf8_bin NOT NULL,
  `TransID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ModeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Role` (
  `RoleID` int(11) NOT NULL,
  `RoleName` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `TransID` int(11) DEFAULT NULL,
  `IsBuiltIn` tinyint(1) NOT NULL,
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `RoleOperation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RoleOperation` (
  `RoleID` int(11) NOT NULL,
  `OperationID` int(11) NOT NULL,
  PRIMARY KEY (`RoleID`,`OperationID`),
  KEY `OperationID` (`OperationID`),
  CONSTRAINT `RoleOperation_ibfk_1` FOREIGN KEY (`RoleID`) REFERENCES `Role` (`RoleID`),
  CONSTRAINT `RoleOperation_ibfk_2` FOREIGN KEY (`OperationID`) REFERENCES `Operation` (`OperationID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `SessionLeader`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SessionLeader` (
  `SessionID` int(11) NOT NULL,
  `LeaderID` int(11) NOT NULL,
  `EffectiveTo` datetime DEFAULT NULL,
  `UpdatedBy` int(11) NOT NULL,
  `Updated` datetime NOT NULL,
  PRIMARY KEY (`SessionID`,`LeaderID`),
  KEY `LeaderID` (`LeaderID`),
  KEY `UpdatedBy` (`UpdatedBy`),
  CONSTRAINT `SessionLeader_ibfk_1` FOREIGN KEY (`LeaderID`) REFERENCES `User` (`UserID`),
  CONSTRAINT `SessionLeader_ibfk_2` FOREIGN KEY (`UpdatedBy`) REFERENCES `User` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `SessionSignup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SessionSignup` (
  `SessionID` int(11) NOT NULL,
  `SignerID` int(11) NOT NULL,
  `EffectiveTo` datetime DEFAULT NULL,
  `Updated` datetime NOT NULL,
  PRIMARY KEY (`SignerID`,`SessionID`),
  KEY `SessionID` (`SessionID`),
  CONSTRAINT `SessionSignup_ibfk_1` FOREIGN KEY (`SessionID`) REFERENCES `PrayerSession` (`SessionID`),
  CONSTRAINT `SessionSignup_ibfk_2` FOREIGN KEY (`SignerID`) REFERENCES `User` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `TimeZone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TimeZone` (
  `ID` int(11) NOT NULL,
  `ZoneCode` varchar(45) COLLATE utf8_bin NOT NULL,
  `Name` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `TransID` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Translation` (
  `TransID` int(11) NOT NULL,
  `Lan2` varchar(200) COLLATE utf8_bin DEFAULT '',
  `Lan3` varchar(200) COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`TransID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(60) COLLATE utf8_bin NOT NULL,
  `Password` varchar(100) COLLATE utf8_bin NOT NULL,
  `Salt` varchar(45) COLLATE utf8_bin NOT NULL,
  `NickName` varchar(60) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Email` varchar(60) COLLATE utf8_bin DEFAULT NULL,
  `Phone` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `RecoverBy` varchar(10) COLLATE utf8_bin NOT NULL DEFAULT 'Email',
  `LanID` int(11) NOT NULL DEFAULT '1',
  `TimeZoneCode` varchar(60) COLLATE utf8_bin NOT NULL,
  `TempPwd` varchar(60) COLLATE utf8_bin DEFAULT NULL,
  `TempPwdCreated` datetime DEFAULT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT '0',
  `Created` datetime NOT NULL,
  `Updated` datetime NOT NULL,
  PRIMARY KEY (`UserID`),
  UNIQUE KEY `UserID_UNIQUE` (`UserID`),
  UNIQUE KEY `UserName_UNIQUE` (`UserName`),
  UNIQUE KEY `Email_UNIQUE` (`Email`),
  UNIQUE KEY `Phone_UNIQUE` (`Phone`),
  KEY `LanID` (`LanID`),
  CONSTRAINT `User_ibfk_1` FOREIGN KEY (`LanID`) REFERENCES `Language` (`LanID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `UserRole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserRole` (
  `UserID` int(11) NOT NULL,
  `RoleID` int(11) NOT NULL,
  PRIMARY KEY (`RoleID`,`UserID`),
  KEY `UserID` (`UserID`),
  CONSTRAINT `UserRole_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `User` (`UserID`),
  CONSTRAINT `UserRole_ibfk_2` FOREIGN KEY (`RoleID`) REFERENCES `Role` (`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `WkdOfMonth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WkdOfMonth` (
  `ID` int(11) NOT NULL,
  `RecurringName` varchar(60) COLLATE utf8_bin NOT NULL,
  `TransID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
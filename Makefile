tsc = webservice/www/node_modules/.bin/tsc
solutionfolder := prayerpalace
clean:
	rm -r webservice/www/dist/*
lib:
	cd webservice/www; \
	mkdir -p dist/vendor/kendo;\
	cp node_modules/kendo-ui-core/css/web/kendo.common.core.min.css dist/vendor/kendo ;\
	cp node_modules/kendo-ui-core/css/web/kendo.common-bootstrap.core.min.css dist/vendor/kendo ;\
	cp node_modules/kendo-ui-core/css/web/kendo.bootstrap.min.css dist/vendor/kendo ;\
	cp node_modules/kendo-ui-core/css/web/kendo.bootstrap.mobile.min.css dist/vendor/kendo ;\
	cp -r node_modules/kendo-ui-core/css/web/Bootstrap dist/vendor/kendo ;\
	cp -r node_modules/kendo-ui-core/css/web/fonts dist/vendor/kendo ;\
	cp -r node_modules/kendo-ui-core/css/web/textures dist/vendor/kendo ;\
	cp -r node_modules/kendo-ui-core/css/mobile/images dist/vendor/kendo ;\
	cp node_modules/jquery/dist/jquery.min.js dist/vendor/ ;\
	cp node_modules/react/umd/react.production.min.js dist/vendor/ ;\
	cp node_modules/react-dom/umd/react-dom.production.min.js dist/vendor/ ;\
	cp node_modules/react-router/umd/react-router.min.js dist/vendor/ ;\
	cp node_modules/react-router-dom/umd/react-router-dom.min.js dist/vendor/ ;\
	mkdir -p dist/vendor/bootstrap/ ;\
	cp -r node_modules/bootstrap/dist/. dist/vendor/bootstrap/ ;

js:
	# to make production do $ env PROD_ENV=1  make js
	#rm -r webservice/www/tsdist/*
	$(tsc) -p "webservice/www/tsconfig.json";
	cd webservice/www  ;\
	./node_modules/less/bin/lessc --include-path=style style/index.less dist/style/index.css ;\
	./node_modules/.bin/webpack ;

	cd webservice/www  ;\
	./node_modules/less/bin/lessc --include-path=style style/login.less dist/style/login.css ;\
	./node_modules/.bin/webpack --config login.webpack.config.js;
strings:
	cd webservice/www ;\
	./node_modules/.bin/extract-strings --outfile=./src/messages.en.json './src/**/*.ts*' ;\
	echo 'export var en=' > ./src/messages.en.ts ;\
	cat ./src/messages.en.json >> ./src/messages.en.ts ;
golib:
	# only needed when vendor source code is not built.
	cd webservice/prayerpalace/ ;\
	govendor install +local
go:
	export PATH=$$PATH:$$GOPATH/bin; \
	cd $$GOPATH  ;\
	hero -source="$$GOPATH/src/$(solutionfolder)/webservice/prayerpalace/template" ;\
	govendor build -o $$GOPATH/src/$(solutionfolder)/webservice/www/prayerpalace $(solutionfolder)/webservice/prayerpalace/

aws:
	env GOOS=linux GOARCH=amd64 \
	govendor build -o cloudMng/aws_dist_buildsrc/prayerpalace $(solutionfolder)/webservice/prayerpalace/ ;\
	cp -r webservice/www/dist cloudMng/aws_dist_buildsrc/ ;
	cp cloudMng/appconfig.json cloudMng/aws_dist_buildsrc/ ;
	cd cloudMng/aws_dist_buildsrc; \
	zip -1 -q -r ../../aws_dist.zip  *

staging:
	env GOOS=linux GOARCH=amd64 \
	govendor build -o cloudMng/aws_dist_buildsrc/prayerpalace $(solutionfolder)/webservice/prayerpalace/ ;\
	cp -r webservice/www/dist cloudMng/aws_dist_buildsrc/ ;
	cd cloudMng/aws_dist_buildsrc; \
	rm appconfig.json;\
	scp -r . sherman@server.home:/home/sherman/Downloads/prayerpalace_dist;\
	echo "run '$ ./prayerpalace &; disown' in ssh and exit to keep the service running."
package main

type AppConfigDB struct {
	MYSQLUSER string
	MYSQLPWD  string
	MYSQLHOST string
	MYSQLDB   string
	MYSQLPORT string
}
type AppConfigMail struct {
	MAILSMTP string
	MAILFROM string
	MAILACC  string
	MAILPWD  string
	METHOD   string
}

type AppConfig struct {
	PORT       string
	HOSTURL    string
	MACHINEKEY string
	DB         AppConfigDB
	MAIL       AppConfigMail
}

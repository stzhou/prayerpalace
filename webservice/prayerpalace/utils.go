package main

import (
	"encoding/base64"
	"fmt"
	"log"
	"math"
	"os" //"strconv"
	"path/filepath"
	"strings"
	"time"

	"golang.org/x/crypto/bcrypt"
)
import (
	"crypto/rand"
	"math/big"
)

type Base64 string

func openLogFile() *os.File {
	fileTemplate := "prayerpalace_%s.log"
	curUtcTime := time.Now().UTC().Format(time.RFC3339)
	//it return UTC time safe for file name: 1999-01-01T12-21-23Z
	curUtcTime = strings.Replace(curUtcTime, ":", "-", 3)
	f, err := os.OpenFile(fmt.Sprintf(fileTemplate, curUtcTime), os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err == nil {
		return f
	}
	panic("Cann't open log file! %s \n" + err.Error())
}
func removeLogFiles() {
	files, err := checkExt("log")
	if err != nil {
		return
	}
	for _, f := range files {
		os.Remove(f)
	}
}
func checkExt(ext string) ([]string, error) {
	pathS, err := os.Getwd()
	if err != nil {
		return nil, err
	}
	var files []string
	filepath.Walk(pathS, func(path string, f os.FileInfo, _ error) error {
		if !f.IsDir() {
			n := f.Name()
			if strings.HasSuffix(n, ext) {
				files = append(files, n)
			}
		}
		return nil
	})
	return files, nil
}

//return an array of slices
// [ sundaySlice, mondayslice ]
// to get 2nd sunday, do result[0][2]
func daysOfWeekOfMonth(date time.Time) [7][]int {
	ret := [7][]int{}
	y, m, _ := date.Date()
	maxDay := time.Date(y, m+1, 0, 0, 0, 0, 0, time.Local).Day()

	firstWkd := int(time.Date(y, m, 1, 0, 0, 0, 0, time.Local).Weekday())
	for i := 1; i <= maxDay; i++ {
		wkd := int(math.Mod(float64(firstWkd+i-1), 7))
		ret[wkd] = append(ret[wkd], i)
	}
	return ret
}

func wkdIdToWkDay(id int) (int, int) {
	//id 1 is first sunday, 2 is first tuesday..., 8 is 2nd sunday ...
	var (
		frec int = int(math.Mod(float64(id-1), 7))
		seq  int = (id-frec-1)/7 + 1
	)
	return seq, frec
}
func ToBase64(src []byte) Base64 {
	return Base64(base64.StdEncoding.EncodeToString(src))
}
func ToByteSlice(src Base64) ([]byte, error) {
	s := string(src)
	return base64.StdEncoding.DecodeString(s)
}

// CalculatePwdHash generate hash and salt ready to save to database
func CalculatePwdHash(plain string) (Base64, error) {
	dk, err := bcrypt.GenerateFromPassword([]byte(plain), bcrypt.DefaultCost)
	if err != nil {
		log.Printf("Cannot generate hash %s\n", err.Error())
		return "", err
	}
	return ToBase64(dk), nil
}
func VerifyPassword(hash Base64, pwd string) bool {
	bytes, err := ToByteSlice(hash)
	if err != nil {
		log.Printf("Invalid casting to base64 %s\n", err.Error())
		return false
	}
	err = bcrypt.CompareHashAndPassword(bytes, []byte(pwd))
	if err != nil {
		log.Printf("Cannot compare hash and password %s\n", err.Error())
		return false
	}
	return true
}
func Slice_Int_Contains(slice []int, val int) bool {
	for _, v := range slice {
		if v == val {
			return true
		}
	}
	return false
}
func Slice_Int32_Contains(slice []int32, val int32) bool {
	for _, v := range slice {
		if v == val {
			return true
		}
	}
	return false
}

func GenerateRandomStringEven(length int) (pass string, err error) {
	for {
		if length <= _lenString(pass) {
			return pass, nil
		}
		num, err := rand.Int(rand.Reader, big.NewInt(int64(127)))
		if err != nil {
			return "", err
		}
		n := num.Int64()
		if n >= 48 && n <= 57 || (n >= 65 && n <= 90) || (n >= 97 && n <= 122) {
			pass += string(n)
		}
	}
}
func _lenString(s string) int {
	return len([]rune(s))
}

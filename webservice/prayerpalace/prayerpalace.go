package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"prayerpalace/webservice/prayerpalace/template"
	"time"

	"github.com/julienschmidt/httprouter"
	"github.com/tkanos/gonfig"
)

/*
func Index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Fprint(w, "Welcome!\n")
}

func Hello(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	urlQuery := r.URL.Query()

	if abc, ok := urlQuery["abc"]; ok {
		fmt.Printf("url query [abc]= %s\n", abc)
	}

	fmt.Fprintf(w, "from goHttpRouter: hello, %s!\n", ps.ByName("name"))
}*/
func scheduleRecurring(dbm *DbManager) *time.Ticker {
	var (
		lastExecuteTime = time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC)
		ticker          = time.NewTicker(time.Hour)
	)
	const GeneratingMonth int16 = 2
	//choose to do at 2:00AM toronto time
	const executionUtcHour int = 7
	go func() {
		for range ticker.C {
			utcNow := time.Now().UTC()
			diff := utcNow.Sub(lastExecuteTime)
			if diff < time.Hour*12 {
				//just executed within 12 hours, do nothing
				continue
			}
			log.Printf("@Scheduler/GenerateSession %s", "Checking cycle started.")
			if utcNow.Hour() == executionUtcHour || utcNow.Hour() == executionUtcHour+1 {
				sessionsToRecur, err := dbm.SessionForRecurring(time.Now().Add(-time.Hour * 24 * 3))
				if err != nil {
					log.Printf("@Scheduler/GetSessionForRecurring failed %s ", err.Error())
					continue
				}
				err = dbm.SessionGenerateRecur(sessionsToRecur, GeneratingMonth)
				if err != nil {
					log.Printf("@Scheduler/GenerateSession failed %s ", err.Error())
					continue
				}
				log.Printf("@Scheduler/GenerateSession %s", "Session Genererated")
				lastExecuteTime = utcNow
			}
		}
	}()
	return ticker
}

var BOOT_TIME_STAMP int64 = time.Now().Unix()

func main() {
	log.Println("try to remove log file")
	removeLogFiles()
	log.Println("try to create log file")
	fLog := openLogFile()
	log.SetOutput(fLog)
	cfg := &AppConfig{}
	log.Println("try to read from current directory appconfig.json")
	gonfig.GetConf("appconfig.json", cfg)

	defer fLog.Close()
	var dbM = DbManager{}
	dbM.initCache()
	defer dbM.close()

	//generateSessionsOnStart(&dbM)
	/*
		//ecurringToDate := time.Now().UTC().AddDate(0, 0, 28)
		/*dbM.InsertPrayerSession("test title", "", 1, time.Now().UTC().AddDate(0, 1, 2), 12, 14, 1, "#002520", 1, &recurringToDate, 4, []int16{1, 3, 5, 25})
		ps, err := dbM.GetSessionForRecurring(time.Now().AddDate(0, 0, -3))
		if err != nil {
			fmt.Printf("%v --------------err\n", err)
		}
		err = dbM.GenerateRecurSession(ps, 2)
		if err != nil {
			fmt.Printf("%v --------------err\n", err)
		}*/
	/*ret, _ := dbM.SessionSignupBySessionID([]int32{23})
	fmt.Printf("ret: %v  \n", ret)
	*/

	//dbM.SetPassword(1, "Password1")

	router := httprouter.New()

	//	router.GET("/", Index)
	router.HandlerFunc("GET", "/index/*whatever", func(w http.ResponseWriter, req *http.Request) {
		template.Index(BOOT_TIME_STAMP, w)
	})
	router.HandlerFunc("GET", "/login/*whatever", func(w http.ResponseWriter, req *http.Request) {
		template.Login(BOOT_TIME_STAMP, w)
	})

	router.HandlerFunc("GET", "/", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/index", 303)
	})

	router.GET("/api/myevent", ApiMiddleware(&dbM, MyEventsGetApi))
	//do upsert and delete all by json content
	router.POST("/api/myevent", ApiMiddleware(&dbM, MyEventsCUDApi))

	router.POST("/api/login", ApiMiddleware(&dbM, TryLoginPostAPI))
	router.PUT("/api/user", ApiMiddleware(&dbM, RegisterUser))

	router.GET("/api/session", ApiMiddleware(&dbM, SessionGetApi))
	router.GET("/api/signupcount", ApiMiddleware(&dbM, SignupCountGetApi))

	//post is for update
	router.POST("/api/session/:id", ApiMiddleware(&dbM, SessionUpsertApi))
	//put is for insert
	router.PUT("/api/session", ApiMiddleware(&dbM, SessionUpsertApi))
	//for delete and activate/deactive
	router.POST("/api/sessiononoff", ApiMiddleware(&dbM, SessionSwitchApi))

	router.GET("/api/admuser", ApiMiddleware(&dbM, AdmUsersGetApi))
	router.POST("/api/admuser", ApiMiddleware(&dbM, AdmUserUpdateApi))

	router.POST("/api/resetpwd", ApiMiddleware(&dbM, ResetPasswordPOSTApi))

	router.POST("/api/changepwd", ApiMiddleware(&dbM, ChangeMyPasswordApi))

	router.GET("/api/cat", ApiMiddleware(&dbM, PrayerCategoryGetApi))
	router.GET("/api/permission", ApiMiddleware(&dbM, PermissionsGetApi))
	router.GET("/api/translation", ApiMiddleware(&dbM, TranslationGetApi))

	router.GET("/api/sessionleader", ApiMiddleware(&dbM, SessionLeaderGetApi))

	router.DELETE("/api/cache", ApiMiddleware(&dbM, PurgeCacheDelApi))

	//handle static files from root
	//It is really bad to serve application itself in served folder:
	//should not put anything other than user downloadable stuff in the served folder.
	port := os.Getenv("PORT")
	log.Printf("Check environment variable PORT %s\n", port)
	if len(port) == 0 {
		port = cfg.PORT
	}

	log.Printf("Serve static directory %s\n", "dist/")
	router.NotFound = http.FileServer(http.Dir("dist/"))
	//debug folder:
	//router.NotFound = http.FileServer(http.Dir("../www/dist/"))

	//start the scheduler to generate each day.
	log.Println("Starts scheduleRecurring")
	ticker := scheduleRecurring(&dbM)
	defer ticker.Stop()

	fmt.Printf("Listening on port: %s\n\n", port)
	log.Printf("Listening on port: %s\n\n", port)
	log.Fatal(http.ListenAndServe(":"+port, router))
}

func generateSessionsOnStart(dbM *DbManager) {
	sessionsToRecur, err := dbM.SessionForRecurring(time.Now().Add(-time.Hour * 24 * 30))
	if err != nil {
		log.Printf("@Scheduler/GetSessionForRecurring failed %s ", err.Error())
	}
	err = dbM.SessionGenerateRecur(sessionsToRecur, 3)
	if err != nil {
		log.Printf("@Scheduler/GenerateSession failed %s ", err.Error())
	}
}

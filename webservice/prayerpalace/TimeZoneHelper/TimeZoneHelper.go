package TimeZoneHelper

import (
	"sync"
	"time"
)

var locationCache = struct {
	m map[string]*time.Location
	sync.RWMutex
}{m: make(map[string]*time.Location)}

//truncate to day beginning using its own timezone.
func BeginOfDay(t time.Time) time.Time {
	y, m, d := t.Date()
	return time.Date(y, m, d, 0, 0, 0, 0, t.Location())
}

//truncate to day begin when you don't trust the timezone, return time with corrected timezone
func BeginOfDayAsTimeZone(timezone string, t time.Time) (time.Time, error) {
	var (
		client *time.Location
		err    error
	)
	locationCache.RLock()
	if tz, ok := locationCache.m[timezone]; ok {
		client = tz
		locationCache.RUnlock()
	} else {
		locationCache.RUnlock()
		client, err = time.LoadLocation(timezone)
		if err != nil {
			return time.Time{}, err
		}
		locationCache.Lock()
		if _, ok := locationCache.m[timezone]; ok == false {
			locationCache.m[timezone] = client
		}
		locationCache.Unlock()
	}
	y, m, d := t.Date()
	return time.Date(y, m, d, 0, 0, 0, 0, client), nil
}

//convert utc time to given timezone's time
func ToTimeZoneTime(timezone string, utc time.Time) (time.Time, error) {
	var (
		client *time.Location
		err    error
	)
	locationCache.RLock()
	if tz, ok := locationCache.m[timezone]; ok {
		client = tz
		locationCache.RUnlock()
	} else {
		locationCache.RUnlock()
		client, err = time.LoadLocation(timezone)
		if err != nil {
			return time.Time{}, err
		}
		locationCache.Lock()
		if _, ok := locationCache.m[timezone]; ok == false {
			locationCache.m[timezone] = client
		}
		locationCache.Unlock()
	}

	local := utc.In(client)
	return local, nil
}

// disgard timezone, treated as given timezone and convert to utc.
func ToUtcTime(timezone string, zonelessTime time.Time) (time.Time, error) {
	var (
		client *time.Location
		err    error
	)
	locationCache.RLock()
	if tz, ok := locationCache.m[timezone]; ok {
		client = tz
		locationCache.RUnlock()
	} else {
		locationCache.RUnlock()
		client, err = time.LoadLocation(timezone)
		if err != nil {
			return time.Time{}, err
		}
		locationCache.Lock()
		if _, ok := locationCache.m[timezone]; ok == false {
			locationCache.m[timezone] = client
		}
		locationCache.Unlock()
	}

	_time := time.Date(zonelessTime.Year(), zonelessTime.Month(), zonelessTime.Day(), zonelessTime.Hour(),
		zonelessTime.Minute(), zonelessTime.Second(), zonelessTime.Nanosecond(), client)
	return _time.In(time.UTC), nil
}

package main

import (
	//"database/sql"
	//"time"
	//"github.com/kisielk/sqlstruct"
	//"github.com/go-sql-driver/mysql"
	"gopkg.in/guregu/null.v3"
)

// Role model
type Role struct {
	RoleID    int32    `json:"roleID"`
	RoleName  string   `json:"roleName"`
	TransID   null.Int `json:"transID"`
	IsBuiltin bool     `json:"isBuiltin"`
}

type AuthenToken struct {
	UserID int32  `json:"userID"`
	Issued string `json:"issued"`
}

type PrayerSession struct {
	SessionID       int32       `sql:"sessionid" json:"sessionID"`
	Title           string      `sql:"title" json:"title"`
	Description     string      `sql:"description" json:"desc"`
	CreatedBy       int32       `sql:"createdby"  json:"createdBy"`
	Date            string      `sql:"date" json:"date"`
	TimeZoneID      int32       `sql:"timezoneid" json:"timeZoneID"`
	FromHH          int8        `sql:"fromhh" json:"fromHH"`
	ToHH            int8        `sql:"tohh" json:"toHH"`
	PrayerCatID     int32       `sql:"prayercatid" json:"prayerCatID"`
	ColorCode       string      `sql:"colorcode" json:"colorCode"`
	RecurringToDate null.String `sql:"recurringtodate" json:"recurringToDate"`
	IsActive        bool        `sql:"isactive" json:"isActive"`
	Created         string      `sql:"created" json:"created"`
	CopiedFrom      null.Int    `sql:"copiedfrom" json:"copiedFrom"`
	Recurring       string      `sql:"recurring" json:"recurring"`
	Deleted         bool        `sql:"deleted" json:"deleted"`
	Leaders         []int32     `sql:"-" json:"leaders"`
}
type RecurringSetting struct {
	Mode  int16   `json:"mode"`
	Value []int16 `json:"value"`
}
type TimeZone struct {
	ID       int32    `sql:"id"`
	ZoneCode string   `sql:"zonecode"`
	Name     string   `sql:"name"`
	TransID  null.Int `sql:"transid"`
}

type SessionSignup struct {
	SessionID   int32       `sql:"sessionid" json:"sessionID"`
	SignerID    int32       `sq:"signerid" json:"signerID"`
	EffectiveTo null.String `sql:"effectiveto" json:"effectiveTo"`
	Updated     string      `sql:"updated" json:"updated"`
}

type RecurringMode struct {
	ModeID   int32    `sql:"modeID"`
	ModeName string   `sql:"modeName"`
	TransID  null.Int `sql:"transID"`
}

type DBUser struct {
	UserID         int32
	UserName       string
	Password       string
	Salt           string
	NickName       string
	Email          null.String
	Phone          null.String
	RecoverBy      string
	LanID          int32
	TimeZoneCode   string
	TempPwd        null.String
	TempPwdCreated null.String
	IsActive       bool
	Created        string
	Updated        string
}

type AdmUser struct {
	UserID    int32       `json:"userID"`
	UserName  string      `json:"userName"`
	NickName  string      `json:"nickName"`
	Email     null.String `json:"email"`
	Phone     null.String `json:"phone"`
	RecoverBy string      `json:"recoverBy"`
	IsActive  bool        `json:"isActive"`
	Created   string      `json:"created"`
	Updated   string      `json:"updated"`
	TempPwd   null.String `json:"tempPwd"`
	Roles     []int32     `json:"roles"`
}

type DBUserRole struct {
	UserID int32 `json:"userID"`
	RoleID int32 `json:"roleID"`
}

type Operation struct {
	OperationID   int32
	OperationName string
	TransID       null.Int
}
type PrayerCat struct {
	PrayerCatID int32    `json:"catID" sql:"prayercatid"`
	CatName     string   `json:"catName" sql:"catname"`
	TransID     null.Int `json:"transID" sql:"transid"`
}

type Translation struct {
	TransID int32  `json:"id" sql:"transid"`
	Lan2    string `json:"lan2" sql:"lan2"`
	Lan3    string `json:"lan3" sql:"lan3"`
}

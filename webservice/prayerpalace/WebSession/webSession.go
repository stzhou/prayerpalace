package WebSession

import (
	"sync"
	"time"
)

/*
* web session implements sliding expire. Without this, user can be kicked out during usage.
* e.g. when user opens website 1 minutes before cookie expire, he will be kicked out after 1 minute.
* The right behavior is to record last visit time + 20 minutes, so user's usage will be extented.
 */

var webSession = struct {
	sync.RWMutex
	m    map[int32]time.Time
	span time.Duration
}{m: make(map[int32]time.Time), span: time.Minute * 20}

func SessionDuration() time.Duration {
	return webSession.span
}

func Clear() {
	webSession.Lock()
	webSession.m = make(map[int32]time.Time)
	webSession.Unlock()
}

// GetCreateWebSession goroutine safe. it uses read/write lock seperately.
// It always create expired session. Call Update to give a SessionDuration() time.
func GetCreateWebSession(userID int32) time.Time {
	webSession.RLock()
	if item, ok := webSession.m[userID]; ok {
		defer webSession.RUnlock()
		return item
	} else {
		webSession.RUnlock()
		webSession.Lock()
		defer webSession.Unlock()
		if item, ok = webSession.m[userID]; ok {
			return item
		} else {
			t := time.Now().UTC()
			//Always create expired session!
			webSession.m[userID] = time.Now().UTC().AddDate(-1, 0, 0)
			return t
		}
	}
}

// UpdateWebSession go routine safe.
func UpdateWebSession(userID int32) {
	webSession.Lock()
	webSession.m[userID] = time.Now().UTC().Add(webSession.span)
	webSession.Unlock()
}

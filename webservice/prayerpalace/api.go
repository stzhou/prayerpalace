package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	EncHelper "prayerpalace/webservice/prayerpalace/EncryptionHelper"
	"prayerpalace/webservice/prayerpalace/TimeZoneHelper"
	"prayerpalace/webservice/prayerpalace/WebSession"
	"regexp"
	"strconv"
	"strings"
	"time" //"fmt"

	"github.com/julienschmidt/httprouter"
	"github.com/tkanos/gonfig"
	"gopkg.in/guregu/null.v3"
)

var CookieExpireTime time.Duration = time.Hour * 6

type UserContext struct {
	UserID  int32
	RoleIDs []int32
}

const (
	ZONE_Toronto = "America/Toronto"
)

//ApiContextHandlerFunc defines all api function type
type ApiContextHandlerFunc func(UserContext, *DbManager, http.ResponseWriter, *http.Request, httprouter.Params)

// AssureSessionExpire true if expired, false if not expired from both token and session.
func AssureWebSessionExpire(tokenUtc time.Time, userID int32) bool {

	sessionUtc := WebSession.GetCreateWebSession(userID)
	utcNow := time.Now().UTC()
	tokenExpireSpan := CookieExpireTime

	if utcNow.Sub(tokenUtc) > tokenExpireSpan {
		//token expired
		if utcNow.After(sessionUtc) {
			//both token and session expired: return expire
			return true
		}
		//extend session expire only when close to expired, since write-lock reduces API performance
		if diff := sessionUtc.Sub(utcNow).Minutes(); diff >= 0 && diff < WebSession.SessionDuration().Minutes()*0.5 {
			WebSession.UpdateWebSession(userID)
		}
		//token expire but session not expire, return false.
		return false
	}

	//token did not expire alway return false

	//!! Only update session expired already and will soon expire
	if diff := sessionUtc.Sub(utcNow).Minutes(); diff < WebSession.SessionDuration().Minutes()*0.5 {
		WebSession.UpdateWebSession(userID)
	}
	return false
}

//ApiMiddleware: transfers to httpRouter handlerfunc and inject required context
func ApiMiddleware(m *DbManager, h ApiContextHandlerFunc) httprouter.Handle {
	config := AppConfig{}
	err := gonfig.GetConf("appconfig.json", &config)
	if err != nil {
		panic(err)
	}
	userRoles, err := m.UserRoles()
	if err != nil {
		panic(err)
	}
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		//Find Auth token from both Header and Cookie. If Header is empty, Cookie take control.
		auth := r.Header.Get("Auth")
		_authCookie, err := r.Cookie("Auth")
		if err == nil && len(_authCookie.Value) > 0 && len(auth) == 0 {
			auth = _authCookie.Value
		}
		userContext := UserContext{}
		//Chalenge header :Auth: 3WdC-H9C4uVk6GEQCPLUDNo2m......
		if len(auth) == 0 {
			h(userContext, m, w, r, p)
			return
		}
		jsonB, err := EncHelper.DecryptFromURLSafeSecret(config.MACHINEKEY, auth)
		if err != nil {
			log.Printf("@ApiMiddleware cannot decrypt auth token from header %s \n", err.Error())
			w.Header().Set("Content-Type", "application/json")
			h(userContext, m, w, r, p)
			return
		}
		token := &AuthenToken{}
		err = json.Unmarshal(jsonB, token)
		if err != nil {
			log.Printf("@ApiMiddleware cannot decrypt auth token from header %s \n", err.Error())
			w.Header().Set("Content-Type", "application/json")
			h(userContext, m, w, r, p)
			return
		}

		tokenIssueUtc, _ := time.Parse(time.RFC3339, token.Issued)
		expired := AssureWebSessionExpire(tokenIssueUtc, token.UserID)
		if !expired {
			userContext.UserID = token.UserID
			userContext.RoleIDs = userRoles[token.UserID]
			//todo: assign roles to userContext
		} else {
			userContext.UserID = 0
			userContext.RoleIDs = nil
			//todo: assign roles to userContext
		}
		w.Header().Set("Content-Type", "application/json")
		h(userContext, m, w, r, p)
	}
}

func setAPIUnAuthorized(hp *http.ResponseWriter, reason string) {
	http.Error(*hp, reason, http.StatusUnauthorized)
}
func setAPIInternalError(hp *http.ResponseWriter, reason string) {
	http.Error(*hp, reason, http.StatusInternalServerError)
}
func setAPIBadRequest(hp *http.ResponseWriter, reason string) {
	http.Error(*hp, reason, http.StatusBadRequest)
}
func toJSON(v interface{}) []byte {
	byts, err := json.Marshal(v)
	if err != nil {
		log.Printf("@toJSON failed %v %s\n", v, err)
		return nil
	}
	return byts
}
func ChallengeOperation(m *DbManager, h *http.ResponseWriter, ctx *UserContext, operationName string) bool {
	allRoleOperations, _ := m.AuthRoleOperations()
	for _, roleID := range ctx.RoleIDs {
		opsPerRole := allRoleOperations[roleID]
		_, ok := opsPerRole[operationName]
		if ok {
			return true
		}
	}
	setAPIUnAuthorized(h, "User lacks role for this operation")
	return false
}
func readJSONRequest(target interface{}, r *http.Request) error {
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(target)
	defer r.Body.Close()
	if err != nil {
		allbody, err := ioutil.ReadAll(r.Body)
		log.Printf("decode json from body failed %s", allbody)
		return err
	}
	return nil
}
func AssureJSONContent(h *http.ResponseWriter, r *http.Request) bool {
	contentType := r.Header.Get("Content-type")
	if contentType != "application/json" {
		(*h).WriteHeader(http.StatusBadRequest)
		(*h).Write([]byte("Requires json content type"))
		return false
	}
	return true
}

func AuthenTokenGetApi(h http.ResponseWriter, r *http.Request, p httprouter.Params) {
	/*
		config := AppConfig{}
		gonfig.GetConf("appconfig.json", &config)
		auth := &AuthenToken{}
		sUserID := p.ByName("userID")
		userID, err := strconv.Atoi(sUserID)
		if err != nil || userID < 0 {
			setAPIBadRequest(&h, "Not a valid id")
		}

		auth.UserID = int32(userID)
		utcWhileLater := time.Now().AddDate(0, 0, 2).UTC()
		auth.Issued = utcWhileLater.Format(time.RFC3339)

		jByts, _ := json.Marshal(auth)
		token, err := EncHelper.EncryptToURLSafeString(config.MACHINEKEY, jByts)
		if err != nil {
			h.WriteHeader(http.StatusInternalServerError)
			log.Printf("@AuthenTokenGetApi %s\n", err.Error())
			return
		}
		h.Header().Set("Set-Cookie",
			fmt.Sprintf("Auth=%s;Expires=%s;Path=/;",
				token,
				//webstandard "Wed, 21 Oct 2015 07:28:00 GMT"
				//RFC1123   = "Mon, 02 Jan 2006 15:04:05 MST"
				strings.Replace(utcWhileLater.Format(time.RFC1123), "UTC", "GMT", -1),
			))
		fmt.Fprintf(h, "Token=%s;", token)
	*/
}
func TryLoginPostAPI(u UserContext, dbm *DbManager, h http.ResponseWriter, r *http.Request, p httprouter.Params) {
	var (
		err error
	)
	var loginDTO = struct {
		Account string `json:"account"`
		Pwd     string `json:"pwd"`
	}{}
	err = readJSONRequest(&loginDTO, r)
	if err != nil {
		setAPIBadRequest(&h, "Invalid parameter for login")
		return
	}
	//normalize:
	loginDTO.Account = strings.ToLower(loginDTO.Account)
	users, err := dbm.DbUsers()
	var foundUser *DBUser
	for i, _ := range users {
		//email should be saved as lower case all the time.
		if users[i].IsActive && (strings.ToLower(users[i].UserName) == loginDTO.Account ||
			(users[i].Email.Valid && users[i].Email.String == loginDTO.Account)) {
			foundUser = &users[i]
			break
		}
	}
	if foundUser == nil {
		log.Printf("None exist user tried to login: %s with password length: %d\n", loginDTO.Account, len(loginDTO.Pwd))
		setAPIBadRequest(&h, "Invalid parameter for login")
		return
	}
	//now compare password hash:
	isMatch := VerifyPassword(Base64(foundUser.Password), loginDTO.Pwd)
	if !isMatch {
		setAPIBadRequest(&h, "Invalid parameter for login")
		return
	}
	//generate token:

	config := AppConfig{}
	gonfig.GetConf("appconfig.json", &config)
	auth := &AuthenToken{}

	auth.UserID = foundUser.UserID
	utcWhileLater := time.Now().AddDate(0, 0, 2).UTC()
	auth.Issued = utcWhileLater.Format(time.RFC3339)
	jByts, _ := json.Marshal(auth)
	token, err := EncHelper.EncryptToURLSafeString(config.MACHINEKEY, jByts)
	if err != nil {
		h.WriteHeader(http.StatusInternalServerError)
		log.Printf("@LoginAPI failed: %s\n", err.Error())
		return
	}
	h.Header().Set("Set-Cookie",
		fmt.Sprintf("Auth=%s;Expires=%s;Path=/;",
			token,
			//webstandard "Wed, 21 Oct 2015 07:28:00 GMT"
			//RFC1123   = "Mon, 02 Jan 2006 15:04:05 MST"
			strings.Replace(utcWhileLater.Format(time.RFC1123), "UTC", "GMT", -1),
		))
}

func MyEventsGetApi(u UserContext, dbm *DbManager, h http.ResponseWriter, r *http.Request, p httprouter.Params) {
	if u.UserID == 0 {
		setAPIUnAuthorized(&h, "Not authenticated.")
		return
	}

	//get sessions from 1 month ago to 3 monthes ahea
	myevents, err := dbm.SessionsByUserID(u.UserID,
		time.Now().UTC().AddDate(-5, 1, 0),
		time.Now().UTC().AddDate(5, 1, 0))

	if err != nil {
		setAPIInternalError(&h, "Database query error")
		return
	}
	h.Write(toJSON(myevents))
	h.WriteHeader(http.StatusOK)
}

func SessionGetApi(u UserContext, dbm *DbManager, h http.ResponseWriter, r *http.Request, p httprouter.Params) {
	if u.UserID == 0 {
		setAPIUnAuthorized(&h, "Not authenticated.")
		return
	}
	urlQuery := r.URL.Query()
	var (
		err   error
		sfrom = urlQuery.Get("from")
		from  time.Time
		sto   = urlQuery.Get("to")
		to    time.Time
	)

	if len(sfrom) == 0 {
		from = time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC)
	} else {
		from, err = time.Parse(time.RFC3339, sfrom)
		if err != nil {
			setAPIInternalError(&h, "from timestring is not RFC3339")
			return
		}
	}
	if len(sto) == 0 {
		to = time.Date(2118, 0, 0, 0, 0, 0, 0, time.UTC)
	} else {
		to, err = time.Parse(time.RFC3339, sto)
		if err != nil {
			setAPIInternalError(&h, "from time param string is not RFC3339")
			return
		}
	}
	sessions, err := dbm.Sessions(from, to)
	if err != nil {
		setAPIInternalError(&h, "to time param string is not RFC3339")
		return
	}
	h.Write(toJSON(sessions))
}

func SessionUpsertApi(u UserContext, dbm *DbManager, h http.ResponseWriter, r *http.Request, p httprouter.Params) {
	if u.UserID == 0 {
		setAPIUnAuthorized(&h, "Not authenticated.")
		return
	}
	if ChallengeOperation(dbm, &h, &u, "Edit-PrayerSession") == false {
		return
	}

	var (
		err error
	)
	if !AssureJSONContent(&h, r) {
		setAPIBadRequest(&h, "content is not json")
		return
	}
	id := p.ByName("id")
	session := PrayerSession{}
	err = readJSONRequest(&session, r)
	if err != nil {
		setAPIBadRequest(&h, "session data cannot be parsed")
		return
	}
	if len(session.Recurring) == 0 && session.RecurringToDate.Valid {
		setAPIBadRequest(&h, "Must provide recurring value when recurringtoDate is set")
		return
	}
	var recurringObj = RecurringSetting{}
	if len(session.Recurring) > 0 {
		err = json.Unmarshal([]byte(session.Recurring), &recurringObj)
		if err != nil {
			setAPIBadRequest(&h, "session data recurring cannot be parsed")
			return
		}
	}
	sessionDate, err := time.Parse(BROWSER_ZONELESS_FMT, session.Date)
	if err != nil {
		setAPIBadRequest(&h, "Wrong format for session Date "+err.Error())
		return
	}

	var sessionRecurDate *time.Time
	if session.RecurringToDate.Valid {
		t, _ := time.Parse(BROWSER_ZONELESS_FMT, session.RecurringToDate.String)
		sessionRecurDate = &t

	}
	if len(id) != 0 {
		iID, err := strconv.Atoi(id)
		if err != nil || iID == 0 {
			setAPIBadRequest(&h, "url segment is not a number")
			return
		}
		session.SessionID = int32(iID)
	} else {
		//make sure if id is not set from /:id, it should not be set at all.
		session.SessionID = 0
	}

	err = dbm.SessionUpsert(session.SessionID, session.Title, session.Description, u.UserID, sessionDate,
		session.FromHH, session.ToHH, session.PrayerCatID,
		session.ColorCode, session.IsActive, session.TimeZoneID, sessionRecurDate,
		recurringObj.Mode, recurringObj.Value,
	)
	_, overlapOccurred := err.(ERROR_SessionOverlap)
	if overlapOccurred {
		err = nil
	}

	if err != nil {
		setAPIInternalError(&h, "create/update failed")
		return
	}

	dbm.SessionLeadersUpdate(session.SessionID, session.Leaders, u.UserID)
	if overlapOccurred {
		http.Error(h, "Session Time Overlapped, Saved as inactive.", http.StatusConflict)
	} else {
		h.WriteHeader(http.StatusCreated)
	}
}
func SessionSwitchApi(u UserContext, dbm *DbManager, h http.ResponseWriter, r *http.Request, p httprouter.Params) {
	if u.UserID == 0 {
		setAPIUnAuthorized(&h, "Not authenticated.")
		return
	}
	if ChallengeOperation(dbm, &h, &u, "Edit-PrayerSession") == false {
		return
	}
	var (
		err error
	)
	if !AssureJSONContent(&h, r) {
		setAPIBadRequest(&h, "content is not json")
		return
	}
	content := struct {
		Deletes []int        `json:"deletes"`
		Actives map[int]bool `json:"actives"`
	}{}
	err = readJSONRequest(&content, r)
	if err != nil {
		setAPIBadRequest(&h, "bad json format")
		return
	}
	err = dbm.SessionOnOffUpdate(content.Deletes, content.Actives)
	if err != nil {
		setAPIInternalError(&h, "internal error db operation failed.")
		return
	}
	h.WriteHeader(http.StatusOK)
}

func MyEventsCUDApi(u UserContext, dbm *DbManager, h http.ResponseWriter, r *http.Request, p httprouter.Params) {
	if u.UserID == 0 {
		setAPIUnAuthorized(&h, "Not authenticated.")
		return
	}
	var (
		err error
	)
	if !AssureJSONContent(&h, r) {
		setAPIBadRequest(&h, "content is not json")
		return
	}
	signup := struct {
		SessionID   int32  `json:"sessionID"`
		IsDel       bool   `json:"isDel"`
		EffectiveTo string `json:"effectiveTo"`
	}{}
	err = readJSONRequest(&signup, r)
	if err != nil {
		setAPIBadRequest(&h, "cannnot parse json content")
		return
	}
	var tEffectiveTo time.Time
	if len(signup.EffectiveTo) != 0 {
		tEffectiveTo, err = time.Parse(time.RFC3339, signup.EffectiveTo)
	}
	err = dbm.MyEventsCUD(signup.SessionID, u.UserID, tEffectiveTo, signup.IsDel)
	if err != nil {
		setAPIInternalError(&h, "Database operation failed")
	}
	h.WriteHeader(http.StatusCreated)
}

func PrayerCategoryGetApi(u UserContext, dbm *DbManager, h http.ResponseWriter, r *http.Request, p httprouter.Params) {
	if u.UserID == 0 {
		setAPIUnAuthorized(&h, "Not authenticated.")
		return
	}
	cats, err := dbm.PrayerCat()
	if err != nil {
		setAPIInternalError(&h, "Database operation failed")
		return
	}
	h.Write(toJSON(cats))
	h.WriteHeader(http.StatusOK)
}

func TranslationGetApi(u UserContext, dbm *DbManager, h http.ResponseWriter, r *http.Request, p httprouter.Params) {
	if u.UserID == 0 {
		setAPIUnAuthorized(&h, "Not authenticated.")
		return
	}
	translations, err := dbm.Translation()
	if err != nil {
		setAPIInternalError(&h, "Database operation failed")
		return
	}
	h.Write(toJSON(translations))
	h.WriteHeader(http.StatusOK)
}

func RegisterUser(u UserContext, dbm *DbManager, h http.ResponseWriter, r *http.Request, p httprouter.Params) {
	var (
		err error
	)
	var registerDTO = struct {
		FirstName string      `json:"firstName"`
		LastName  string      `json:"lastName"`
		UserName  string      `json:"userName"`
		Email     null.String `json:"email"`
		Pwd       string      `json:"pwd"`
		TimeZone  string      `json:"tz"`
		LanID     int         `json:"lanid"`
	}{}
	err = readJSONRequest(&registerDTO, r)
	if err != nil {
		setAPIBadRequest(&h, "Bad Data")
		return
	}

	/*
	    client code for validation:
	   const passwordRule={
	     expr:/(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}/,
	     desc:"8 characters with at least 1 uppercase, 1 lowercase, 1 number."
	   };
	   const emailValidator=/\w+@\w+\.\w+/;
	*/
	emailPtrn := regexp.MustCompile(`\w+@\w+\.\w+`)
	//server side validation and normalization:
	var validationErrBuf bytes.Buffer
	if len(registerDTO.UserName) == 0 {
		validationErrBuf.WriteString("User Name should not be empty.\n")
	}
	if registerDTO.Email.IsZero() || !emailPtrn.MatchString(registerDTO.Email.String) {
		validationErrBuf.WriteString("Email is not valid.\n")
	}
	if len(registerDTO.Pwd) >= 8 {
		invalid := false

		if passed, _ := regexp.MatchString("[a-z]+", registerDTO.Pwd); !passed {
			invalid = true
		} else if passed, _ := regexp.MatchString("[A-Z]+", registerDTO.Pwd); !passed {
			invalid = true
		} else if passed, _ := regexp.MatchString("\\d+", registerDTO.Pwd); !passed {
			invalid = true
		}
		if invalid {
			validationErrBuf.WriteString("Password doesn't meet complexity rule.\n")
		}
	} else {
		validationErrBuf.WriteString("Password too short\n")
	}
	registerDataInvalid := validationErrBuf.String()
	if len(registerDataInvalid) > 0 {
		setAPIBadRequest(&h, registerDataInvalid)
		return
	}
	hash, err := CalculatePwdHash(registerDTO.Pwd)
	if err != nil {
		setAPIInternalError(&h, "Internal error Calculate hash failed")
		return
	}
	const activateWhenRegister bool = false

	err = dbm.UpsertDbUsers(DBUser{
		UserName:     registerDTO.UserName,
		Password:     string(hash),
		Salt:         "",
		NickName:     registerDTO.FirstName + " " + registerDTO.LastName,
		Email:        registerDTO.Email,
		RecoverBy:    "EMAIL",
		LanID:        1,
		TimeZoneCode: ZONE_Toronto,
		IsActive:     activateWhenRegister,
	})
	if err != nil {
		setAPIInternalError(&h, "Cannot save user due to database problem.")
		return
	}
	if registerDTO.Email.Valid {
		go func(toAddr string) {
			email := MailHelper{}
			email.LoadConfig()
			err := email.Send(toAddr,
				"Prayer Palace Registration Confirmation",
				fmt.Sprintf(`
					<body>
						<b>Thank you</b> for registering to our website. <br/>
						Please remember your login email: %s. <br/>
						<p></p>
						<p>You will be able to login after System Administrator activate your account. </p>
						<p></p>
						<p>We do not record your password, but you will need to use your email to reset your password. </p>
					</body>`,
					toAddr),
			)
			if err != nil {
				log.Printf("@email sending failed: %s\n", err)
			}
		}(registerDTO.Email.String)

		go func(userAddress string) {
			email := MailHelper{}
			email.LoadConfig()
			//sending email to itself:
			err := email.Send(email.from,
				"Prayer Palace Account Activation Request",
				fmt.Sprintf(`
					<body>
						A user requested you to activate the account with email %s.
						Please login to the system and go to UserAdmin page to change the active status.
					</body>`,
					userAddress),
			)
			if err != nil {
				log.Printf("@email sending failed: %s\n", err)
			}
		}(registerDTO.Email.String)
	}
}
func ResetPasswordPOSTApi(u UserContext, dbm *DbManager, h http.ResponseWriter, r *http.Request, p httprouter.Params) {
	var dto = struct {
		Account string `json:"account"`
	}{}
	err := readJSONRequest(&dto, r)
	if err != nil {
		setAPIBadRequest(&h, "Cannot read json content")
		return
	}
	dto.Account = strings.ToLower(dto.Account)
	users, err := dbm.DbUsers()
	if err != nil {
		setAPIInternalError(&h, "Internal data issue")
		return
	}
	var user DBUser
	for i, _ := range users {
		if users[i].Email.Valid && users[i].Email.String == dto.Account {
			user = users[i]
			break
		}
	}
	if user.UserID != 0 && user.Email.Valid {
		newPWD, err := GenerateRandomStringEven(20)
		if err != nil {
			setAPIInternalError(&h, "Cannot generate password, algorithm error")
			return
		}
		dbm.SetPassword(user.UserID, newPWD)
		go func(user DBUser) {
			mailman := MailHelper{}
			mailman.LoadConfig()
			mailBodyTemplate := `
<html>
<head>
<meta charset='UTF-8'>
</head>
<body style='font-family:Arial,Helvetica Neue,Helvetica,sans-serif; margin:20px 20px 20px 40px;'>
<p/><p/><p>您好，</p>
<p>您已请求重置密码，这是您的临时密码。</p>
<p><b>%s</b></p>
<p>您可使用它来登录并更改为新密码。</p>
<p>您的祷告之殿网站 <a href='https://www.ipp24prayer.com'>https://www.ipp24prayer.com</a></p>
<hr/>
<p>您好，</p>
<p>您已請求重置密碼，這是您的臨時密碼。</p>
<p><b>%s</b></p>
<p>您可使用它來登錄並更改為新密碼。</p>
<p>您的禱告之殿網站 <a href='https://www.ipp24prayer.com'>https://www.ipp24prayer.com</a></p>
<hr/>
<p>Hello,</p>
<p>You have requested a password reset, here is your temporary password. </p>
<p><b>%s</b></p>
<p>You may want to use it to log in and change to a new password.</p>
<p>Yours<br/>
Prayer Palace Website <a href='https://www.ipp24prayer.com'>https://www.ipp24prayer.com</a></p>
		`
			err := mailman.Send(user.Email.String, "Prayer Palace System Your New Password", fmt.Sprintf(mailBodyTemplate, newPWD, newPWD, newPWD))
			if err != nil {
				//we can rollback, but for now don't worry.
				log.Printf("@ResetPassword Failed to send reset password email: %s\n", err.Error())
			}
		}(user)
	} else {
		setAPIBadRequest(&h, "User can't be found or user has no email")
	}
}
func ChangeMyPasswordApi(u UserContext, dbm *DbManager, h http.ResponseWriter, r *http.Request, p httprouter.Params) {
	if u.UserID == 0 {
		setAPIUnAuthorized(&h, "Not authenticated.")
		return
	}
	var dto = struct {
		OldPwd string `json:"oldPwd"`
		NewPwd string `json:"newPwd"`
	}{}
	err := readJSONRequest(&dto, r)
	if err != nil {
		setAPIBadRequest(&h, "Cannot read json content")
		return
	}
	var (
		user DBUser
	)
	users, err := dbm.DbUsers()
	for i, _ := range users {
		if users[i].UserID == u.UserID {
			user = users[i]
			break
		}
	}
	if !VerifyPassword(Base64(user.Password), dto.OldPwd) {
		setAPIBadRequest(&h, "Old password didn't match our record")
		return
	}

	var validationErrBuf bytes.Buffer
	if len(dto.NewPwd) >= 8 {
		invalid := false
		if passed, _ := regexp.MatchString("[a-z]+", dto.NewPwd); !passed {
			invalid = true
		} else if passed, _ := regexp.MatchString("[A-Z]+", dto.NewPwd); !passed {
			invalid = true
		} else if passed, _ := regexp.MatchString("\\d+", dto.NewPwd); !passed {
			invalid = true
		}
		if invalid {
			validationErrBuf.WriteString("Password doesn't meet complexity rule.\n")
		}
	} else {
		validationErrBuf.WriteString("Password too short\n")
	}
	registerDataInvalid := validationErrBuf.String()
	if len(registerDataInvalid) > 0 {
		setAPIBadRequest(&h, registerDataInvalid)
		return
	}
	err = dbm.SetPassword(u.UserID, dto.NewPwd)
	if err != nil {
		log.Printf("@SaveMyPassword error cannot save user's new password for user %d, error is:%s", u.UserID, err.Error())
		setAPIInternalError(&h, "For some reason, cann't save your new password")
		return
	}
	h.WriteHeader(http.StatusAccepted)
}

//AdmUsersGetApi return all side loaded data for maintain users.
func AdmUsersGetApi(u UserContext, dbm *DbManager, h http.ResponseWriter, r *http.Request, p httprouter.Params) {
	if u.UserID == 0 {
		setAPIUnAuthorized(&h, "Not authenticated.")
		return
	}
	if ChallengeOperation(dbm, &h, &u, "Edit-UserRole") == false {
		setAPIUnAuthorized(&h, "Not authorized.")
		return
	}
	if ChallengeOperation(dbm, &h, &u, "Activate-User") == false {
		setAPIUnAuthorized(&h, "Not authorized.")
		return
	}
	var dto = struct {
		Roles []Role    `json:"roles"`
		Users []AdmUser `json:"users"`
	}{}
	_roles, err := dbm.Roles()
	if err != nil {
		setAPIInternalError(&h, "Retrieve roles failed.")
		return
	}
	dto.Roles = _roles
	_userRolesAll, _ := dbm.UserRoles()
	_users, err := dbm.DbUsers()
	if err != nil {
		setAPIInternalError(&h, "Retrieve users failed.")
		return
	}
	dto.Users = make([]AdmUser, len(_users), len(_users))
	for i, _ := range _users {
		var userRoles []int32
		if _roles, OK := _userRolesAll[_users[i].UserID]; OK {
			userRoles = _roles
		}
		dto.Users[i] = AdmUser{
			UserID:    _users[i].UserID,
			UserName:  _users[i].UserName,
			NickName:  _users[i].NickName,
			Email:     _users[i].Email,
			Phone:     _users[i].Phone,
			RecoverBy: _users[i].RecoverBy,
			IsActive:  _users[i].IsActive,
			Created:   _users[i].Created,
			Updated:   _users[i].Updated,
			TempPwd:   _users[i].TempPwd,
			Roles:     userRoles,
		}
	}
	h.Write(toJSON(dto))
}

func AdmUserUpdateApi(u UserContext, dbm *DbManager, h http.ResponseWriter, r *http.Request, p httprouter.Params) {
	if u.UserID == 0 {
		setAPIUnAuthorized(&h, "Not authenticated.")
		return
	}
	if ChallengeOperation(dbm, &h, &u, "Edit-UserRole") == false {
		setAPIUnAuthorized(&h, "Not authorized.")
		return
	}
	if ChallengeOperation(dbm, &h, &u, "Activate-User") == false {
		setAPIUnAuthorized(&h, "Not authorized.")
		return
	}
	admUser := AdmUser{}
	err := readJSONRequest(&admUser, r)
	if err != nil {
		setAPIBadRequest(&h, "content's json object is not valid")
		return
	}
	if admUser.UserID == 0 {
		setAPIBadRequest(&h, "this api is for update existing user only.")
		return
	}
	dbUser := DBUser{
		UserID:    admUser.UserID,
		UserName:  admUser.UserName,
		NickName:  admUser.NickName,
		Email:     admUser.Email,
		Phone:     admUser.Phone,
		RecoverBy: admUser.RecoverBy,
		IsActive:  admUser.IsActive,
		Created:   admUser.Created,
		Updated:   time.Now().UTC().Format(MYSQL_DateTimeFmt),
	}
	cachedDbUsers, _ := dbm.DbUsers()
	for i, _ := range cachedDbUsers {
		if cachedDbUsers[i].UserID == admUser.UserID {
			_u := cachedDbUsers[i]
			dbUser.LanID = _u.LanID
			dbUser.Password = _u.Password
			dbUser.Salt = _u.Salt
			dbUser.TimeZoneCode = _u.TimeZoneCode
			dbUser.Created = _u.Created
			break
		}
	}

	err = dbm.UpsertDbUsers(dbUser)
	if err != nil {
		log.Printf("Updating user failed. %s\n", err.Error())
		setAPIInternalError(&h, "internal error, cannot update user.")
		return
	}
	err = dbm.UserRolesUpdate(admUser.UserID, admUser.Roles)
	if err != nil {
		log.Printf("Updating user's role failed. %s\n", err.Error())
		setAPIInternalError(&h, "internal error, cannot update user role.")
		return
	}
	h.WriteHeader(http.StatusAccepted)
}
func PermissionsGetApi(u UserContext, dbm *DbManager, h http.ResponseWriter, r *http.Request, p httprouter.Params) {
	if u.UserID == 0 {
		setAPIUnAuthorized(&h, "Not authenticated.")
		return
	}
	allRoleOperations, _ := dbm.AuthRoleOperations()
	uniqueOps := make(map[string]bool)
	ret := []string{}
	for _, roleID := range u.RoleIDs {
		opsPerRole := allRoleOperations[roleID]
		for key, _ := range opsPerRole {
			if exists := uniqueOps[key]; !exists {
				uniqueOps[key] = true
				ret = append(ret, key)
			}
		}
	}
	h.Write(toJSON(ret))
}

func SessionLeaderGetApi(u UserContext, dbm *DbManager, h http.ResponseWriter, r *http.Request, p httprouter.Params) {
	var (
		err error
	)
	const LeaderRoleID = 4
	if u.UserID == 0 {
		setAPIUnAuthorized(&h, "Not authenticated.")
		return
	}
	userRoles, err := dbm.UserRoles()
	if err != nil {
		setAPIInternalError(&h, "@internal db error")
	}
	dbUsers, err := dbm.DbUsers()
	if err != nil {
		setAPIInternalError(&h, "internal db error")
	}
	type DTO struct {
		ID   int32  `json:"id"`
		Name string `json:"name"`
	}
	leaderDTOs := []DTO{}
	for userID, roles := range userRoles {
		if Slice_Int32_Contains(roles, LeaderRoleID) {
			dto := DTO{ID: userID}
			for _, _dbUser := range dbUsers {
				if _dbUser.UserID == userID {
					dto.Name = fmt.Sprintf("%s", _dbUser.NickName)
					break
				}
			}
			leaderDTOs = append(leaderDTOs, dto)
		}
	}
	h.Write(toJSON(leaderDTOs))
}

func PurgeCacheDelApi(u UserContext, dbm *DbManager, h http.ResponseWriter, r *http.Request, p httprouter.Params) {
	const PermissionRequired = "Clear-Cache"
	const LeaderRoleID = 4
	if u.UserID == 0 {
		setAPIUnAuthorized(&h, "Not authenticated.")
		return
	}
	//temporarily disable permission check.
	if false && !ChallengeOperation(dbm, &h, &u, PermissionRequired) {
		setAPIBadRequest(&h, fmt.Sprintf("Requires Permission %s:", PermissionRequired))
		return
	}
	dbm.cache.Flush()
	log.Printf("@PurgeCacheDelApi cache Flushed\n")
}
func SignupCountGetApi(u UserContext, dbm *DbManager, h http.ResponseWriter, r *http.Request, p httprouter.Params) {
	if u.UserID == 0 {
		setAPIUnAuthorized(&h, "Not authenticated.")
		return
	}
	urlQuery := r.URL.Query()
	var (
		err   error
		sfrom = urlQuery.Get("from")
		from  time.Time
		sto   = urlQuery.Get("to")
		to    time.Time
	)
	if len(sfrom) != 0 {
		from, err = time.Parse(time.RFC3339, sfrom)
		if err != nil {
			setAPIInternalError(&h, "from timestring is not RFC3339")
			return
		}
	} else {
		//default from = Yesterday Eastern Time, (db function default to year 2000)
		_now, _ := TimeZoneHelper.BeginOfDayAsTimeZone(ZONE_Toronto, time.Now())
		from = _now.AddDate(0, 0, -1).In(time.UTC)
	}
	if len(sto) != 0 {
		to, err = time.Parse(time.RFC3339, sto)
		if err != nil {
			setAPIInternalError(&h, "to timestring is not RFC3339")
			return
		}
	}
	ret, err := dbm.SignupCount(from, to)
	if err != nil {
		setAPIInternalError(&h, "Cannot fetch data from db.")
		return
	}
	h.Write(toJSON(ret))
}

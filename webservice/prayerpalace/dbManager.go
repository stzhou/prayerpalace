package main

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	tzh "prayerpalace/webservice/prayerpalace/TimeZoneHelper"
	"sort"
	"strings"
	"sync"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/kisielk/sqlstruct"
	Cache "github.com/patrickmn/go-cache"
	"github.com/tkanos/gonfig"
	"gopkg.in/guregu/null.v3"
)

const MYSQL_DateTimeFmt string = "2006-01-02 15:04:05"
const BROWSER_ZONELESS_FMT string = "2006/1/2 15:04:05"

type DbManager struct {
	mysql  *sql.DB
	rwLock sync.RWMutex
	cache  *Cache.Cache
}

type ERROR_SessionOverlap struct {
	s string
}

func (er ERROR_SessionOverlap) Error() string {
	return er.s
}

func (m *DbManager) initCache() {
	c := Cache.New(12*time.Hour, 10*time.Minute)
	m.cache = c
}

//open concurrently safe, when already opened, it does nothing.
func (m *DbManager) open() error {
	rLocker := m.rwLock.RLocker()
	rLocker.Lock()
	if m.mysql != nil && m.mysql.Ping() == nil {
		defer rLocker.Unlock()
		return nil
	}
	rLocker.Unlock()

	config := AppConfig{}
	err := gonfig.GetConf("appconfig.json", &config)
	if err != nil {
		log.Printf("DB pen failure: %s\n", err.Error())
		return err
	}
	dbConfig := config.DB
	//mysql, err := sql.Open("mysql", "root:sherman-test-123@tcp(127.0.0.1:3306)/prayerpalace?charset=utf8")
	mysqlConStr := fmt.Sprintf("%[1]s:%[2]s@tcp(%[3]s:%[4]s)/%[5]s?charset=utf8",
		dbConfig.MYSQLUSER, dbConfig.MYSQLPWD, dbConfig.MYSQLHOST, dbConfig.MYSQLPORT, dbConfig.MYSQLDB)

	m.rwLock.Lock()
	defer m.rwLock.Unlock()
	if m.mysql != nil && m.mysql.Ping() == nil {
		return nil
	}
	mysql, err := sql.Open("mysql", mysqlConStr)
	m.mysql = mysql
	if err != nil {
		log.Fatalln(err.Error())
		return err
	}
	err = m.mysql.Ping()
	if err != nil {
		log.Fatalln(err.Error())
		return err
	}
	return nil
}

func (m *DbManager) close() {
	if m.mysql != nil {
		m.mysql.Close()
	}
}

//=================== User Events
func (m *DbManager) SessionsByUserID(userID int32, from time.Time, to time.Time) (interface{}, error) {
	sql := `
		select t1.sessionID, t1.date, t2.effectiveTo, t2.signerID from PrayerSession t1
		join SessionSignup t2 on t1.SessionID = t2.SessionID
		and t2.SignerID = ?
		where
		t1.deleted=0 and
		t1.Date >= ? and t1.Date <= ?
	`
	err := m.open()
	if err != nil {
		return nil, err
	}
	rows, err := m.mysql.Query(sql, userID, from.Format(time.RFC3339), to.Format(time.RFC3339))
	if err != nil {
		log.Printf("@GetSessionsByUserID query error: %s\n", err.Error())
		return nil, err
	}
	type RetItem struct {
		SessionID   int         `sql:"sessionID" json:"sessionID"`
		Date        string      `sql:"date" json:"date"`
		EffectiveTo null.String `sql:"effectiveTo" json:"effectiveTo"`
		SignerID    int         `sql:"signerID" json:"signerID"`
	}
	ret := []RetItem{}
	for rows.Next() {
		r := RetItem{}
		err = sqlstruct.ScanAliased(&r, rows, "t2")
		if err != nil {
			log.Printf("@GetSessionsByUserID query error: %s\n", err.Error())
			return nil, err
		}
		ret = append(ret, r)
	}
	return ret, nil
}

func (m *DbManager) Sessions(from time.Time, to time.Time) ([]PrayerSession, error) {
	sql := `
		select %s from PrayerSession t1
		where
		t1.deleted=0 and
		t1.Date >= ? and t1.Date <= ?
	`
	sql = fmt.Sprintf(sql, sqlstruct.ColumnsAliased(PrayerSession{}, "t1"))
	err := m.open()
	if err != nil {
		return nil, err
	}
	rows, err := m.mysql.Query(sql, from.Format(MYSQL_DateTimeFmt), to.Format(MYSQL_DateTimeFmt))
	if err != nil {
		log.Printf("@GetSessions query error: %s\n", err.Error())
		return nil, err
	}
	ret := []PrayerSession{}
	for rows.Next() {
		r := PrayerSession{}
		err = sqlstruct.ScanAliased(&r, rows, "t1")
		if err != nil {
			log.Printf("@GetSessions query error: %s\n", err.Error())
			return nil, err
		}
		ret = append(ret, r)
	}
	//get leaders:
	sql = `
		select t1.SessionID as SessionID, sl.LeaderID as LeaderID from PrayerSession t1
		join SessionLeader sl on t1.SessionID = sl.SessionID
		where
		t1.deleted=0 and
		t1.Date >= ? and t1.Date <= ?
	`
	type SessionLeader struct {
		SessionID int32 `sql:"sessionID"`
		LeaderID  int32 `sql:"leaderID"`
	}
	var sessionLeaderMap = make(map[int32][]int32)
	rows, err = m.mysql.Query(sql, from.Format(MYSQL_DateTimeFmt), to.Format(MYSQL_DateTimeFmt))

	for rows.Next() {
		r := &SessionLeader{}
		err = sqlstruct.Scan(r, rows)
		if err != nil {
			log.Printf("@GetSessionLeader query error: %s \n", err.Error())
			return nil, err
		}
		sessionLeaderMap[r.SessionID] = append(sessionLeaderMap[r.SessionID], r.LeaderID)
	}
	for i, _ := range ret {
		if leaders, ok := sessionLeaderMap[ret[i].SessionID]; ok {
			ret[i].Leaders = leaders
		}
	}

	return ret, nil
}

/*
func (m *DbManager) SessionLeadingsByUserID(userID int32, from time.Time, to time.Time) ([]PrayerSession, error) {
	sql := `
		select %s from PrayerSession ps
		join SessionLeader sl on ps.SessionID = sl.SessionID and sl.LeaderID = ?
		where ps.deleted=0 and ps.Date >= ? and ps.Date <= ?
	`
	sql = fmt.Sprintf(sql, sqlstruct.ColumnsAliased(PrayerSession{}, "ps"))
	err := m.open()
	if err != nil {
		return nil, err
	}
	rows, err := m.mysql.Query(sql, userID, from.Format(time.RFC3339), to.Format(time.RFC3339))
	if err != nil {
		log.Printf("@GetLeadingSessionsByUserID query error: %s\n", err.Error())
		return nil, err
	}
	ret := []PrayerSession{}
	for rows.Next() {
		r := PrayerSession{}
		err = sqlstruct.ScanAliased(&r, rows, "t1")
		if err != nil {
			log.Printf("@GetLeadingSessionsByUserID query error: %s\n", err.Error())
			return nil, err
		}
		ret = append(ret, r)
	}
	return ret, nil
}*/

func (m *DbManager) SessionUpsert(
	sessionID int32,
	title string,
	desc string,
	createdBy int32,
	localDate time.Time,
	fromHH int8,
	toHH int8,
	catID int32,
	color string,
	isActive bool,
	timezoneID int32,
	recurringToLocalDate *time.Time,
	recurringMode int16,
	recurringValue interface{},
) error {
	var err error

	if len(title) == 0 {
		return fmt.Errorf("InsertPrayerSession: title cannot be empty")
	}
	if fromHH > 23 || fromHH < 0 {
		return fmt.Errorf("InsertPrayerSession: fromHH out of range")
	}
	if toHH > 23 || toHH < 0 {
		//to handle cross day session, toHH can be less than from hh.
		return fmt.Errorf("InsertPrayerSession: toHH out of range")
	}
	timeZones, err := m.TimeZone()
	if err != nil {
		return err
	}
	var timezone string
	for i := range timeZones {
		if timeZones[i].ID == timezoneID {
			timezone = timeZones[i].ZoneCode
		}
	}
	if len(timezone) == 0 {
		return fmt.Errorf("InsertPrayerSession timezone ID cannot be found %d", timezoneID)
	}

	//treat as given timezone, truncate to dayBegin
	localDate, err = tzh.BeginOfDayAsTimeZone(timezone, localDate)
	if err != nil {
		log.Printf("@InsertPrayerSession wrong timezone %s %s\n", timezone, err)
		return err
	}

	if recurringToLocalDate != nil {
		*recurringToLocalDate, _ = tzh.BeginOfDayAsTimeZone(timezone, *recurringToLocalDate)
	}

	//recurring: if mode=1(by day of month), it is a int value of day.
	//if mode= 2, 3 week/biweek, there is no value.
	//if mode=4, weekday of a month, it is an array of WkdOfMonth ids.
	recurringJSON := RecurringSetting{Mode: recurringMode}

	if recurringMode == 2 || recurringMode == 3 {
		if recurringValue != nil {
			log.Printf("InsertPrayerSession: for recurring mode 2 or 3 don't expect a value sessionid=%d, mode=%d, value=%v ", sessionID, recurringMode, recurringValue)
			recurringValue = nil
		}
	}

	if recurringMode == 1 {
		dayOfMonth := recurringValue.([]int16)
		recurringJSON.Value = dayOfMonth
	} else if recurringMode == 4 {
		wkdOfMonthItems := recurringValue.([]int16)
		recurringJSON.Value = wkdOfMonthItems
	}

	strRecurringJSON, _ := json.Marshal(recurringJSON)
	var pRecurringToDate interface{}
	if recurringToLocalDate != nil {
		pRecurringToDate = recurringToLocalDate.In(time.UTC).Format(MYSQL_DateTimeFmt)
	}
	err = m.open()
	if err != nil {
		return err
	}

	//detecting conflicts: pull all sessions on the same date.
	noTimeConflict := true
	if isActive {
		existingSessions, err := m.Sessions(localDate.In(time.UTC), localDate.In(time.UTC))
		if err != nil {
			return err
		}
		for i, _ := range existingSessions {
			_fromHH := existingSessions[i].FromHH
			_toHH := existingSessions[i].ToHH
			if existingSessions[i].SessionID == sessionID || !existingSessions[i].IsActive || existingSessions[i].Deleted {
				continue
			}
			if _fromHH > _toHH {
				_toHH = _toHH + 24
			}
			if (fromHH <= _fromHH && toHH >= _fromHH) || (fromHH <= _toHH && toHH >= _toHH) {
				noTimeConflict = false
			}
		}
		if !noTimeConflict {
			//turn off active if conflict found
			isActive = false
		}
	}

	if sessionID == 0 {
		sqlInsert := `
		insert PrayerSession (Title, Description, CreatedBy,
			Date, FromHH, toHH,
			PrayerCatID, RecurringToDate, isActive,
			Created, Recurring, ColorCode, TimeZoneID
		) values (
			?, ?, ?,
			?, ?, ?,
			?, ?, ?,
			?, ?, ?, ?
		)
	`
		_, err = m.mysql.Exec(sqlInsert,
			title, desc, createdBy,
			localDate.In(time.UTC).Format(MYSQL_DateTimeFmt), fromHH, toHH,
			catID, pRecurringToDate, 0,
			time.Now().UTC().Format(MYSQL_DateTimeFmt), string(strRecurringJSON), color, timezoneID,
		)
	} else {
		sqlUpdate := `
			Update PrayerSession
			Set title = ?, description = ?, date = ?,
				fromHH = ?, toHH = ?, prayerCatID = ?,
				recurringToDate = ?, isActive = ?,
				recurring = ?, colorCode = ?, timeZoneID = ?
			where sessionID = ?
			`
		iIsActive := 0
		if isActive {
			iIsActive = 1
		}
		_, err = m.mysql.Exec(sqlUpdate,
			title, desc, localDate.In(time.UTC).Format(MYSQL_DateTimeFmt),
			fromHH, toHH, catID,
			pRecurringToDate, iIsActive,
			string(strRecurringJSON), color, timezoneID,
			sessionID,
		)
		if err != nil {
			log.Printf("db error update Prayersession, %s\n", err.Error())
			return err
		}
		//now get sibling and child PrayerSession and update all of them for these properties only:
		//title, description, fromHH, toHH, prayerCatID, recurringToDate, recurring, isActive
		relatedSessionIDs, err := m.GetRelatedSessionIDs(sessionID, localDate)
		if err != nil {
			return err
		}
		sqlUpdateRelated := `
			Update PrayerSession
			Set title = ?, description = ?, fromHH = ?,
				toHH = ?, recurringToDate = ?, isActive = ?, prayerCatID=?,
				recurring = ?, colorCode = ?, timeZoneID = ?
			where sessionID = ?
		`
		errs := []string{}
		if len(relatedSessionIDs) > 0 {
			for _, sid := range relatedSessionIDs {
				_, err = m.mysql.Exec(sqlUpdateRelated,
					title, desc, fromHH,
					toHH, pRecurringToDate, iIsActive, catID,
					string(strRecurringJSON), color, timezoneID,
					sid,
				)
				if err != nil {
					errs = append(errs, err.Error())
				}
			}
		}
		if len(errs) > 0 {
			err = fmt.Errorf(strings.Join(errs, ","))
			log.Printf("@InsertPrayerSession SQL error %s\n", err.Error())
		}
		if err != nil {
			log.Printf("@InsertPrayerSession SQL error %s\n", err.Error())
		}
	}

	if err != nil {
		log.Printf("@InsertPrayerSession SQL error %s\n", err.Error())
	}
	if noTimeConflict == false {
		return ERROR_SessionOverlap{s: "Time overlapped, set to inactive"}
	}
	return err
}

func (m *DbManager) GetRelatedSessionIDs(copiedFrom int32, from time.Time) ([]int32, error) {
	var (
		err error
	)
	err = m.open()
	if err != nil {
		log.Printf("db cannot open %s\n", err.Error())
		return nil, err
	}
	//find all the sessions that copiedFrom=@sessionID or copiedFrom=sessions of @sessionID's CopiedFrom
	getRelatedSessionQuery := `
			Select SessionID  From PrayerSession
			Where isActive=1 And deleted=0
			And CopiedFrom =? AND Date >= ?
			UNION
			Select t1.SessionID SessionID
			From PrayerSession t1
			JOIN PrayerSession t2 ON t1.CopiedFrom = t2.CopiedFrom
			Where t2.SessionID = ? And t1.Date >= ?
			And t1.IsActive =1 and t1.deleted = 0
		`
	rows, err := m.mysql.Query(getRelatedSessionQuery, copiedFrom, from.Format(MYSQL_DateTimeFmt), copiedFrom, from.Format(MYSQL_DateTimeFmt))
	if err != nil {
		log.Printf("db query error GetRelatedSessionIDs: %s", err.Error())
		return nil, err
	}
	ret := []int32{}
	for rows.Next() {
		var dbsessionID int32
		rows.Scan(&dbsessionID)
		if dbsessionID != copiedFrom {
			ret = append(ret, dbsessionID)
		}
	}
	return ret, nil
}

//Now don't worry about effectiveTo.
func (m *DbManager) SessionLeadersUpdate(sessionID int32, leaderIDs []int32, updatedBy int32) error {
	var (
		err error
	)
	err = m.open()
	if err != nil {
		log.Printf("db cannot open %s\n", err.Error())
		return err
	}

	sql_get := `Select LeaderID From SessionLeader Where SessionID=?`
	rows, err := m.mysql.Query(sql_get, sessionID)
	if err != nil {
		log.Printf("db query error %s \n", err.Error())
		return err
	}
	db_leaderIDs := []int{}
	for rows.Next() {
		var leaderID int
		rows.Scan(&leaderID)
		db_leaderIDs = append(db_leaderIDs, leaderID)
	}
	errors := []string{}
	for _, newID := range leaderIDs {
		if !Slice_Int_Contains(db_leaderIDs, int(newID)) {
			_, err = m.mysql.Exec(`
						Insert SessionLeader(SessionID, LeaderID, EffectiveTo, UpdatedBy, Updated)
						value(?, ?, ?, ?, ?)
					`, sessionID, newID, nil,
				updatedBy, time.Now().UTC().Format(MYSQL_DateTimeFmt))
			if err != nil {
				errors = append(errors, err.Error())
			}
		}
	}
	for _, dbID := range db_leaderIDs {
		if !Slice_Int32_Contains(leaderIDs, int32(dbID)) {
			_, err = m.mysql.Exec("delete From SessionLeader where SessionID=? AND LeaderID=?", sessionID, dbID)
			if err != nil {
				errors = append(errors)
			}
		}
	}
	if len(errors) > 0 {
		log.Printf("Update Session Leaders has db error %s", strings.Join(errors, "* "))
		return fmt.Errorf("One or more errors occurred: %s", strings.Join(errors, "\n"))
	}
	return nil
}

/* SessionForRecurring
 * To be used by session recurring generator. User may create one session that repeats every day, we
 * have no choice but to run the generator every day.
 * But lookbackTime doesn't need to be large. 3 days are good.
 * In emergency, should allow user not to wait the task runner but generate right away.
 * lookback certain days, is good, find out active sessions that have Recurring JSON
 * and not yet get copied to future or having future siblings that are copied from same parent.
 * Since we don't generate too much 'future' records, this will not be heavy.
 */
func (m *DbManager) SessionForRecurring(lookbackTime time.Time) ([]PrayerSession, error) {
	//lookback time is rough time, don't worry about timezone.
	if lookbackTime.After(time.Now()) {
		return nil, fmt.Errorf("lookbackTime should be in the past but you gave %s", lookbackTime.Format(time.RFC3339))
	}
	var err error
	_query := `
	SELECT %s FROM PrayerSession t1
	WHERE t1.Date > ?
	  AND t1.IsActive = 1 And t1.deleted=0
	  AND t1.Recurring IS NOT NULL AND JSON_VALID(t1.Recurring)
	  AND NOT EXISTS (
		  SELECT 1 FROM PrayerSession t2 WHERE (
				t2.CopiedFrom = t1.sessionID
				OR t2.CopiedFrom = t1.CopiedFrom)
			AND t2.Date >= t1.Date
			AND t2.deleted = 0
	  );
	`
	err = m.open()
	if err != nil {
		return nil, err
	}
	_query = fmt.Sprintf(_query, sqlstruct.ColumnsAliased(PrayerSession{}, "t1"))
	rows, err := m.mysql.Query(_query, lookbackTime.Format(MYSQL_DateTimeFmt))
	if err != nil {
		log.Printf("@GetSessionForRecurring query error: %s\n", err.Error())
		return nil, err
	}
	ret := []PrayerSession{}
	for rows.Next() {
		r := PrayerSession{}
		err = sqlstruct.ScanAliased(&r, rows, "t1")
		if err != nil {
			log.Printf("@GetSessionForRecurring query error: %s\n", err.Error())
			return nil, err
		}
		ret = append(ret, r)
	}
	return ret, nil
}

func (m *DbManager) TimeZone() ([]TimeZone, error) {
	const cchKey = "TimeZoneAll"
	if cch, ok := m.cache.Get(cchKey); ok {
		return cch.([]TimeZone), nil
	}

	var (
		err error
	)
	//get timezones
	err = m.open()
	if err != nil {
		return nil, err
	}

	_query := fmt.Sprintf("Select %s from TimeZone Order by ID", sqlstruct.Columns(TimeZone{}))
	rows, err := m.mysql.Query(_query)
	ret := []TimeZone{}
	for rows.Next() {
		r := TimeZone{}
		err := sqlstruct.Scan(&r, rows)
		if err != nil {
			log.Printf("@GetZone query error: %s\n", err.Error())
			return nil, err
		}
		ret = append(ret, r)
	}
	m.cache.Set(cchKey, ret, Cache.NoExpiration)
	return ret, nil
}

/*
* Tricky part is DayLight Saving change.
* Since the host may change time but the signer may not, no matter I adjust the time or not, it always causes problem.
* And also if I auto adjust the time, I associate the timezone of the leader with the session itself.
* But when the leader is not known, prayer session really has no timezone to use.
 */
func (m *DbManager) SessionGenerateRecur(sessions []PrayerSession, monthToGen int16) error {
	//Since when DST mode changes for some people, people from different timezone will always have problem, I will choose not to adjust the time for DST.
	//Which is keeping the same UTC time.
	//We might just let user manually adjust the time for all future occurrance and somehow notify signup users or broadcasting time change and check their schedule.
	//This will make programming easier.
	//1 by Day of Month, 2: By Week; 3: By Biweek; 4: ByWeeday of Month;
	var (
		err error
	)
	insertSession := []PrayerSession{}

	timeZones, err := m.TimeZone()
	//the timezone will be used to do auto DST during generation:
	findTimeZoneCode := func(zid int32) string {
		for i := range timeZones {
			if timeZones[i].ID == zid {
				return timeZones[i].ZoneCode
			}
		}
		return ""
	}

	if err != nil {
		return err
	}

	for i := range sessions {
		s := sessions[i]
		var (
			err          error
			_recurringTo time.Time
			_date        time.Time
		)
		if s.RecurringToDate.Valid {
			_recurringTo, err = time.Parse(MYSQL_DateTimeFmt, s.RecurringToDate.String)
		}
		//to handle DST change _date is local time
		_date, err = time.Parse(MYSQL_DateTimeFmt, s.Date)
		sessionTZ := findTimeZoneCode(s.TimeZoneID)
		_date, err = tzh.ToTimeZoneTime(sessionTZ, _date)
		//************** _date becomes Local Time from here *******************//

		recurSetting := &RecurringSetting{}
		err = json.Unmarshal([]byte(s.Recurring), recurSetting)
		if err != nil {
			log.Printf("Invalid recurring json :%s \n", s.Recurring)
			continue
		}

		maxByFunParam := _date.AddDate(0, int(monthToGen), 0)
		if recurSetting.Mode == 1 || recurSetting.Mode == 2 || recurSetting.Mode == 3 {
			y, m, _ := _date.Date()
			for jj := 1; true; jj++ {
				var nextTime time.Time
				switch recurSetting.Mode {
				case 1:
					nextTime = time.Date(y, m+time.Month(jj), int(recurSetting.Value[0]), _date.Hour(), 0, 0, 0, time.Local)
					break
				case 2:
					nextTime = _date.AddDate(0, 0, jj*7)
					break
				case 3:
					nextTime = _date.AddDate(0, 0, jj*14)
				}
				//over generation param limit:
				if nextTime.After(maxByFunParam) || nextTime.IsZero() {
					break
				}
				// over recurring setting limit:
				if _recurringTo.IsZero() == false && nextTime.After(_recurringTo) {
					break
				}
				newSession := s
				newSession.CopiedFrom.SetValid(int64(s.SessionID))
				//convert nextTime back to utc:
				nextTime = nextTime.In(time.UTC)
				newSession.RecurringToDate.String = s.RecurringToDate.String
				newSession.RecurringToDate.Valid = s.RecurringToDate.Valid
				newSession.Date = nextTime.Format(MYSQL_DateTimeFmt)
				newSession.Created = time.Now().UTC().Format(MYSQL_DateTimeFmt)
				insertSession = append(insertSession, newSession)
			}

		} else if recurSetting.Mode == 4 {
			//Weekday of months' setting values are database pk. 1=1st Sunday, 8 = 2nd Sunday, 9 = 2nd Monday, etc.
			y, m, _ := _date.Date()
			for jj := 0; jj <= int(monthToGen); jj++ {
				//logic to handle current month:
				for _, wkId := range recurSetting.Value {
					wkdSeq, wkd := wkdIdToWkDay(int(wkId))
					_dOfWeekOfMDic := daysOfWeekOfMonth(time.Date(y, m+time.Month(jj), 1, 0, 0, 0, 0, time.Local))
					day := _dOfWeekOfMDic[wkd][wkdSeq-1]
					nextTime := time.Date(y, m+time.Month(jj), day, 0, 0, 0, 0, time.Local)
					if nextTime.After(maxByFunParam) {
						break
					}
					if _recurringTo.IsZero() == false && nextTime.After(_recurringTo) {
						break
					}
					newSession := s
					newSession.CopiedFrom.SetValid(int64(s.SessionID))
					newSession.RecurringToDate.String = s.RecurringToDate.String
					newSession.RecurringToDate.Valid = s.RecurringToDate.Valid
					//convert nextTime back to UTC
					nextTime = nextTime.In(time.UTC)
					newSession.Date = nextTime.Format(MYSQL_DateTimeFmt)
					newSession.Created = time.Now().UTC().Format(MYSQL_DateTimeFmt)
					insertSession = append(insertSession, newSession)
				}
			}

		}
	}
	if len(insertSession) > 0 {
		//insert to db:
		err = m.open()
		if err != nil {
			return err
		}
		_query := `
		insert PrayerSession (Title, Description, CreatedBy,
			Date, FromHH, toHH,
			PrayerCatID, ColorCode, RecurringToDate,
			isActive, Created, CopiedFrom,
			Recurring, TimeZoneID
		) values (
			?, ?, ?,
			?, ?, ?,
			?, ?, ?,
			?, ?, ?,
			?, ?
		)
		`

		stmt, err := m.mysql.Prepare(_query)
		defer stmt.Close()
		if err != nil {
			log.Printf("@Insert Prayer session, Prepare query error: %s\n", err.Error())
		}

		var errCollection bytes.Buffer
		for _, s := range insertSession {

			_, err := stmt.Exec(
				s.Title, s.Description, s.CreatedBy,
				s.Date, s.FromHH, s.ToHH,
				s.PrayerCatID, s.ColorCode, s.RecurringToDate,
				s.IsActive, s.Created, s.CopiedFrom,
				s.Recurring, s.TimeZoneID,
			)
			if err != nil {
				errii := fmt.Sprintf("...Insert PrayerSession Failed: %s\n", err.Error())
				errCollection.WriteString(errii)
				continue
			}
		}
		if errCollection.Len() > 0 {
			return fmt.Errorf("%s", errCollection.String())
		}
	}

	if len(insertSession) > 0 {
		var lastError error
		//assume mysql is already opened by last test
		for _, s := range sessions {
			_, err = m.mysql.Exec("call CopySigners( ? )", s.SessionID)
			if err != nil {
				log.Printf("call CopySigners failed with id %d", s.SessionID)
				lastError = err
			}
		}
		if lastError != nil {
			return lastError
		}
	}
	return nil
}

func (m *DbManager) SessionSignupBySessionID(sessionIDs []int32) (map[int32][]SessionSignup, error) {
	var (
		ret = make(map[int32][]SessionSignup)
		err error
	)
	_query := `
			select %s from SessionSignup where SessionID = ?
		`
	err = m.open()
	if err != nil {
		log.Printf("@GetSessionSignupBySessionID db open error %s \n", err.Error())
		return nil, err
	}
	_query = fmt.Sprintf(_query, sqlstruct.Columns(SessionSignup{}))
	stmt, err := m.mysql.Prepare(_query)
	defer stmt.Close()
	if err != nil {
		log.Printf("@GetSessionSignupBySessionID db error %s \n", err.Error())
		return nil, err
	}
	//considering scaling, each session could have tons of signup, just query users for each session.
	for _, sid := range sessionIDs {
		rows, err := stmt.Query(sid)
		if err != nil {
			return nil, err
		}
		for rows.Next() {
			ss := SessionSignup{}
			err = sqlstruct.Scan(&ss, rows)
			if err != nil {
				log.Printf("@GetSessionSignupBySessionID db scan error %s \n", err.Error())
				return nil, err
			}
			if ret[ss.SessionID] == nil {
				ret[ss.SessionID] = []SessionSignup{}
			}
			ret[ss.SessionID] = append(ret[ss.SessionID], ss)
		}
	}
	return ret, nil
}
func (m *DbManager) SessionOnOffUpdate(deleteIDs []int, active map[int]bool) error {
	var (
		err error
	)
	err = m.open()
	if err != nil {
		return err
	}
	_deleteSQL := "Update PrayerSession Set deleted=1 where sessionID=?;"
	_delstmt, err := m.mysql.Prepare(_deleteSQL)
	defer _delstmt.Close()
	_activeSwitchSQL := "Update PrayerSession Set isActive=? where sessionID=?;"
	_activestmt, err := m.mysql.Prepare(_activeSwitchSQL)
	defer _activestmt.Close()
	var lastError error
	for _, id := range deleteIDs {
		_, err = _delstmt.Exec(id)
		if err != nil {
			log.Printf("deletePrayerSession ID %d query error: %s\n", id, err.Error())
			lastError = err
		}
	}
	for k, v := range active {
		activeInt := 0
		if v {
			activeInt = 1
		}
		_, err = _activestmt.Exec(activeInt, k)
		if err != nil {
			log.Printf("active/deactive PrayerSession ID %d query error: %s\n", k, err.Error())
			lastError = err
		}
	}
	return lastError
}

func (m *DbManager) MyEventsCUD(sessionID int32, userID int32, effectiveTo time.Time, isDel bool) error {
	var (
		err error
	)
	err = m.open()
	if err != nil {
		return err
	}
	if isDel {
		_, err = m.mysql.Exec("Delete From SessionSignup where signerID=? and sessionID=?",
			userID, sessionID,
		)
		if err != nil {
			return err
		}
		return nil
	}
	_sqlGet := "Select %s from SessionSignup where signerId=? and sessionId= ? limit 1"
	_sqlGet = fmt.Sprintf(_sqlGet, sqlstruct.Columns(SessionSignup{}))

	rows, err := m.mysql.Query(_sqlGet, userID, sessionID)
	if err != nil {
		return err
	}
	var nullableEffectiveTo sql.NullString
	if !effectiveTo.IsZero() {
		nullableEffectiveTo.Valid = true
		nullableEffectiveTo.String = effectiveTo.Format(MYSQL_DateTimeFmt)
	}

	dbSignup := SessionSignup{}
	sNow := time.Now().UTC().Format(MYSQL_DateTimeFmt)
	if rows.Next() {
		// needs update
		err = sqlstruct.Scan(&dbSignup, rows)
		if err != nil {
			return err
		}
		_, err = m.mysql.Exec(
			`Update SessionSignup
			 	set effectiveTo=?, updated=?
			 where sessionID=? and SignerID=?
			`,
			nullableEffectiveTo, sNow,
			sessionID, userID,
		)
		if err != nil {
			return err
		}
	} else {
		// needs insert
		_, err = m.mysql.Exec(`Insert SessionSignup(sessionID, SignerID, effectiveTo, Updated)
			value(?, ?, ?, ?)
			`,
			sessionID, userID, nullableEffectiveTo, sNow,
		)
		if err != nil {
			return err
		}
	}
	return nil
}

func (m *DbManager) RecurringMode() ([]RecurringMode, error) {
	const cacheKey = "RecurringModeAll"
	if c, ok := m.cache.Get(cacheKey); ok {
		return c.([]RecurringMode), nil
	}
	var (
		err error
	)
	err = m.open()
	if err != nil {
		return nil, err
	}
	_sql := `Select %s from RecurringMode`
	_sql = fmt.Sprintf(_sql, sqlstruct.Columns(RecurringMode{}))
	rows, err := m.mysql.Query(_sql)
	if err != nil {
		log.Printf("RecurringMode db query failed: %s", err.Error())
		return nil, err
	}
	ret := []RecurringMode{}
	for rows.Next() {
		r := RecurringMode{}
		err = sqlstruct.Scan(&r, rows)
		if err != nil {
			log.Printf("RecurringMode db query failed: %s", err.Error())
			return nil, err
		}
		ret = append(ret, r)
	}
	m.cache.Set(cacheKey, ret, Cache.DefaultExpiration)
	return ret, nil
}

func (m *DbManager) UserRoles() (map[int32][]int32, error) {
	const cacheKey = "UserRoleAll"
	if c, ok := m.cache.Get(cacheKey); ok {
		return c.(map[int32][]int32), nil
	}
	var (
		err error
	)
	err = m.open()
	if err != nil {
		return nil, err
	}
	_sql := `Select %s from UserRole`
	_sql = fmt.Sprintf(_sql, sqlstruct.Columns(DBUserRole{}))
	rows, err := m.mysql.Query(_sql)
	if err != nil {
		log.Printf("UserRole db query failed: %s", err.Error())
		return nil, err
	}
	ret := map[int32][]int32{}
	for rows.Next() {
		r := DBUserRole{}
		err = sqlstruct.Scan(&r, rows)
		if err != nil {
			log.Printf("UserRole db query failed: %s", err.Error())
			return nil, err
		}
		ret[r.UserID] = append(ret[r.UserID], r.RoleID)
	}
	m.cache.Set(cacheKey, ret, Cache.DefaultExpiration)
	return ret, nil
}

// result is {roleID: mapof OperationNames:int}
func (m *DbManager) AuthRoleOperations() (map[int32]map[string]int32, error) {
	const cacheKey = "AuthRoleOperationAll"
	if c, ok := m.cache.Get(cacheKey); ok {
		return c.(map[int32]map[string]int32), nil
	}
	var (
		err error
	)
	err = m.open()
	if err != nil {
		return nil, err
	}
	_sql := `Select o.OperationID as OperationID,
		  o.OperationName as OperationName,
		  ro.RoleID as RoleID
	     from Operation o
		Join RoleOperation ro ON  o.OperationID = ro.OperationID
	`
	type DTO struct {
		RoleID        int32
		OperationID   int32
		OperationName string
	}
	rows, err := m.mysql.Query(_sql)
	if err != nil {
		log.Printf("AuthRoleOperation db query failed: %s", err.Error())
		return nil, err
	}
	dbData := []DTO{}
	for rows.Next() {
		dto := DTO{}
		err = sqlstruct.Scan(&dto, rows)
		if err != nil {
			log.Printf("AuthRoleOperation db query failed: %s", err.Error())
			return nil, err
		}
		dbData = append(dbData, dto)
	}
	ret := map[int32]map[string]int32{}
	for _, row := range dbData {
		_, ok := ret[row.RoleID]
		if !ok {
			ret[row.RoleID] = map[string]int32{}
		}
		ret[row.RoleID][row.OperationName] = row.OperationID
	}
	m.cache.Add(cacheKey, ret, Cache.DefaultExpiration)
	return ret, nil
}

func (m *DbManager) PrayerCat() ([]PrayerCat, error) {
	const cacheKey = "PrayerCatAll"
	if c, ok := m.cache.Get(cacheKey); ok {
		return c.([]PrayerCat), nil
	}
	var (
		err error
	)
	err = m.open()
	if err != nil {
		return nil, err
	}
	_sql := fmt.Sprintf("Select %s from PrayerCat", sqlstruct.Columns(PrayerCat{}))

	rows, err := m.mysql.Query(_sql)
	if err != nil {
		log.Printf("PrayerCat db query failed: %s", err.Error())
		return nil, err
	}
	dbData := []PrayerCat{}
	for rows.Next() {
		dto := PrayerCat{}
		err = sqlstruct.Scan(&dto, rows)
		if err != nil {
			log.Printf("PrayerCat db query failed: %s", err.Error())
			return nil, err
		}
		dbData = append(dbData, dto)
	}
	m.cache.Add(cacheKey, dbData, Cache.DefaultExpiration)
	return dbData, nil
}

func (m *DbManager) Translation() ([]Translation, error) {
	const cacheKey = "translation-all"
	if c, ok := m.cache.Get(cacheKey); ok {
		return c.([]Translation), nil
	}
	var (
		err error
	)
	err = m.open()
	if err != nil {
		return nil, err
	}
	_sql := fmt.Sprintf("Select %s from Translation", sqlstruct.Columns(Translation{}))

	rows, err := m.mysql.Query(_sql)
	if err != nil {
		log.Printf("Translation db query failed: %s", err.Error())
		return nil, err
	}
	dbData := []Translation{}
	for rows.Next() {
		dto := Translation{}
		err = sqlstruct.Scan(&dto, rows)
		if err != nil {
			log.Printf("Translation db query failed: %s", err.Error())
			return nil, err
		}
		dbData = append(dbData, dto)
	}
	m.cache.Add(cacheKey, dbData, Cache.DefaultExpiration)
	return dbData, nil
}

func (m *DbManager) DbUsers() ([]DBUser, error) {
	const cacheKey = "DbUsers"
	if c, ok := m.cache.Get(cacheKey); ok {
		return c.([]DBUser), nil
	}
	var (
		err error
	)
	err = m.open()
	if err != nil {
		return nil, err
	}
	_sql := fmt.Sprintf("Select %s from User", sqlstruct.Columns(DBUser{}))
	rows, err := m.mysql.Query(_sql)
	if err != nil {
		log.Printf("DbUsers db query failed: %s", err.Error())
		return nil, err
	}
	dbData := []DBUser{}
	for rows.Next() {
		dto := DBUser{}
		err = sqlstruct.Scan(&dto, rows)
		if err != nil {
			log.Printf("DbUsers db query failed: %s", err.Error())
			return nil, err
		}
		dbData = append(dbData, dto)
	}
	m.cache.Add(cacheKey, dbData, Cache.DefaultExpiration)
	return dbData, nil
}
func (m *DbManager) UpsertDbUsers(user DBUser) error {
	const cacheKey = "DbUsers"
	var (
		err error
	)
	dbUsers, err := m.DbUsers()
	if err != nil {
		return err
	}
	err = m.open()
	if err != nil {
		return err
	}
	if user.UserID == 0 {
		//insert
		if user.Email.Valid {
			user.Email.String = strings.ToLower(user.Email.String)

			for i, _ := range dbUsers {
				if dbUsers[i].Email.Valid && dbUsers[i].Email.String == user.Email.String {
					//email not unique!
					return fmt.Errorf("email already existed")
				}
			}
		}
		_, err = m.mysql.Exec(`Insert User (UserName, Password, NickName,
											Email, Phone, RecoverBy,
											LanID, TimeZoneCode, isActive,
											Created, Updated,Salt)
											value(?,?,?,
												?,?,?,
												?,?,?,
												?,?,?)`,
			user.UserName, user.Password, user.NickName,
			user.Email, user.Phone, user.RecoverBy,
			user.LanID, user.TimeZoneCode, user.IsActive,
			time.Now().UTC().Format(MYSQL_DateTimeFmt), time.Now().UTC().Format(MYSQL_DateTimeFmt), "N/A")
		if err != nil {
			log.Printf("Insert User failed with error %s \n", err.Error())
			return err
		}
		//for creating user, we let cache rebuild
		m.cache.Delete(cacheKey)
	}
	if user.UserID != 0 {
		if user.Email.Valid {
			user.Email.String = strings.ToLower(user.Email.String)
		}
		_, err = m.mysql.Exec(`	Update User
							   	set UserName=?, Password=?, NickName=?,
									Email=? , Phone=?, RecoverBy=?,
									LanID=?, TimeZoneCode=?, isActive=?
								where UserID=?`,
			user.UserName, user.Password, user.NickName,
			user.Email, user.Phone, user.RecoverBy,
			user.LanID, user.TimeZoneCode, user.IsActive,
			user.UserID)
		if err != nil {
			log.Printf("Insert User failed with error %s \n", err.Error())
			return err
		}
		for i, _ := range dbUsers {
			//update item into whole cache set.
			if dbUsers[i].UserID == user.UserID {
				//remember don't replace other property on cache, since they are not saved to db.
				dbUsers[i].UserName = user.UserName
				dbUsers[i].Password = user.Password
				dbUsers[i].NickName = user.NickName
				dbUsers[i].Email = user.Email
				dbUsers[i].Phone = user.Phone
				dbUsers[i].RecoverBy = user.RecoverBy
				dbUsers[i].LanID = user.LanID
				dbUsers[i].TimeZoneCode = user.TimeZoneCode
				dbUsers[i].IsActive = user.IsActive
				m.cache.Set(cacheKey, dbUsers, Cache.DefaultExpiration)
				break
			}
		}

	}
	return nil
}

func (m *DbManager) Roles() ([]Role, error) {
	const cacheKey = "DbRoles"
	if c, ok := m.cache.Get(cacheKey); ok {
		return c.([]Role), nil
	}
	var (
		err error
	)
	err = m.open()
	if err != nil {
		return nil, err
	}
	_sql := fmt.Sprintf("Select %s from Role", sqlstruct.Columns(Role{}))
	rows, err := m.mysql.Query(_sql)
	if err != nil {
		log.Printf("DbRoles db query failed: %s", err.Error())
		return nil, err
	}
	dbData := []Role{}
	for rows.Next() {
		dto := Role{}
		err = sqlstruct.Scan(&dto, rows)
		if err != nil {
			log.Printf("DbRoles db query failed: %s", err.Error())
			return nil, err
		}
		dbData = append(dbData, dto)
	}
	m.cache.Add(cacheKey, dbData, Cache.DefaultExpiration)
	return dbData, nil
}

// UserRolesUpdate implemented subitem cache reloading.
func (m *DbManager) UserRolesUpdate(userID int32, roleIDs []int32) error {
	const cacheKey = "UserRoleAll"
	var (
		err error
	)
	err = m.open()
	if err != nil {
		return err
	}
	_roleIDs := make([]int, len(roleIDs), len(roleIDs))
	for i, _ := range roleIDs {
		_roleIDs[i] = int(roleIDs[i])
	}
	sort.Ints(_roleIDs)
	userroles, _ := m.UserRoles()
	var _dbroleIDs []int = []int{}
	if cachedUserRole, ok := userroles[userID]; ok {
		_dbroleIDs = make([]int, len(cachedUserRole), len(cachedUserRole))
		for i, _ := range cachedUserRole {
			_dbroleIDs[i] = int(cachedUserRole[i])
		}
	}
	if len(_dbroleIDs) == len(_roleIDs) {
		findDiff := false
		for i := range _dbroleIDs {
			if _dbroleIDs[i] != _roleIDs[i] {
				findDiff = true
				break
			}
		}
		if findDiff == false {
			//cached role is the same as newroles, nothing to do.
			return nil
		}
	}
	//since you can't execute multiple statement by default connstring, two queries delete and insert need to be send:
	var (
		inserts     []int = []int{}
		deletes     []int = []int{}
		sql_inserts string
		sql_deletes string
	)
	for _, r := range _roleIDs {
		if Slice_Int_Contains(_dbroleIDs, r) == false {
			inserts = append(inserts, r)
		}
	}
	for _, dr := range _dbroleIDs {
		//if cannot find an role-in-db delete
		if Slice_Int_Contains(_roleIDs, dr) == false {
			deletes = append(deletes, dr)
		}
	}

	errors := []error{}
	if len(inserts) > 0 {
		var sql_insertsBuffer bytes.Buffer
		sql_insertsBuffer.WriteString("Insert UserRole (UserID, RoleID) Values ")
		for _, v := range inserts {
			sql_insertsBuffer.WriteString(fmt.Sprintf("(%d,%d),", userID, v))
		}
		sql_inserts = strings.TrimSuffix(sql_insertsBuffer.String(), ",")
		_, err = m.mysql.Exec(sql_inserts)
		if err != nil {
			log.Printf("Inserting user,role failed: %s\n", err.Error())
			errors = append(errors, err)
		}
	}
	if len(deletes) > 0 {
		var sql_deleteBuffer bytes.Buffer
		sql_deleteBuffer.WriteString("Delete From UserRole where (userID, RoleID) IN (")
		for i, v := range deletes {
			if i < len(deletes)-1 {
				sql_deleteBuffer.WriteString(fmt.Sprintf("(%d,%d),", userID, v))
			} else {
				sql_deleteBuffer.WriteString(fmt.Sprintf("(%d,%d))", userID, v))
			}
		}
		sql_deletes = sql_deleteBuffer.String()
		_, err = m.mysql.Exec(sql_deletes)
		if err != nil {
			log.Printf("Deleting user,role failed: %s\n", err.Error())
			errors = append(errors, err)
		}
	}
	if len(errors) == 0 {
		if c, ok := m.cache.Get(cacheKey); ok {
			_cacheObj := c.(map[int32][]int32)
			_cacheObj[userID] = roleIDs
			m.cache.Set(cacheKey, _cacheObj, Cache.DefaultExpiration)
		}
		return nil
	}
	return fmt.Errorf("%v", errors)
}
func (m *DbManager) SetPassword(userID int32, newPassword string) error {
	users, err := m.DbUsers()
	if err != nil {
		return err
	}
	var user DBUser
	for i, _ := range users {
		if users[i].UserID == userID {
			user = users[i]
			break
		}
	}
	if user.UserID == 0 {
		return fmt.Errorf("user not found")
	}
	newPwdHash, _ := CalculatePwdHash(newPassword)
	user.Password = string(newPwdHash)
	user.Updated = time.Now().UTC().Format(MYSQL_DateTimeFmt)
	err = m.UpsertDbUsers(user)
	return err
}

// SignupCount return counts of all sessions between time range, pass zero time to set unlimited search, cache for 5 minutes
// of the same time range, so don't pass time.Now(), pass a real date without time part.
func (m *DbManager) SignupCount(fromDate time.Time, toDate time.Time) (interface{}, error) {
	type dto struct {
		SessionID int32 `json:"sessionID"`
		Count     int32 `json:"count"`
	}
	if fromDate.IsZero() {
		fromDate = time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC)
	}
	if toDate.IsZero() {
		toDate = time.Date(2118, 1, 1, 0, 0, 0, 0, time.UTC)
	}
	var cacheKey = fmt.Sprintf("SignupCountByTimeRange-%s-%s", fromDate.Format(MYSQL_DateTimeFmt), toDate.Format(MYSQL_DateTimeFmt))

	if c, ok := m.cache.Get(cacheKey); ok {
		return c.([]dto), nil
	}

	err := m.open()
	if err != nil {
		return nil, err
	}

	rows, err := m.mysql.Query(`
		Select ss.SessionID SessionID, Count(ss.SignerID) as Count From PrayerSession ps
Join SessionSignup ss on ps.SessionID=ss.SessionID
Where ps.deleted=0 AND ps.Date >=? And ps.Date <=?
Group By ss.SessionID`,
		fromDate.Format(MYSQL_DateTimeFmt), toDate.Format(MYSQL_DateTimeFmt))
	if err != nil {
		log.Printf("Signup count db query error %s\n", err.Error())
		return nil, err
	}
	dtoCollection := []dto{}
	for rows.Next() {
		dto := dto{}
		err = sqlstruct.Scan(&dto, rows)
		if err != nil {
			log.Printf("Signup count db query scan failed: %s", err.Error())
			return nil, err
		}
		dtoCollection = append(dtoCollection, dto)
	}
	m.cache.Add(cacheKey, dtoCollection, time.Minute*5)
	return dtoCollection, nil
}

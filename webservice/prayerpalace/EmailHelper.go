package main

import (
	"crypto/tls"
	"fmt"
	"log"
	"net/smtp"
	"strconv"
	"strings"

	"github.com/tkanos/gonfig"
	"gopkg.in/gomail.v2"
)

type MailHelper struct {
	server  string
	from    string
	account string
	pwd     string
	method  string
}

// LoadConfig should be called before SendEmail
func (mh *MailHelper) LoadConfig() {
	cfg := &AppConfig{}
	gonfig.GetConf("appconfig.json", cfg)
	mh.from = cfg.MAIL.MAILFROM
	mh.server = cfg.MAIL.MAILSMTP
	mh.pwd = cfg.MAIL.MAILPWD
	mh.account = cfg.MAIL.MAILACC
	mh.method = cfg.MAIL.METHOD
}
func (mh *MailHelper) _tlsConfig(host string) tls.Config {
	return tls.Config{
		InsecureSkipVerify: true,
		ServerName:         host,
	}
}

func (mh *MailHelper) _msg(to string, subject string, content string) string {
	msgPart := []string{}
	msgPart = append(msgPart, fmt.Sprintf("To: %s", to))
	msgPart = append(msgPart, fmt.Sprintf("Subject: %s", subject))
	msgPart = append(msgPart, content)
	return strings.Join(msgPart, "\r\n")
}

// SendStartTLS works for gmail 587 smpt
// outlook uses starttls
func (mh *MailHelper) Send(to string, subject string, content string) error {
	if mh.method == "STARTTLS" {
		return mh.sendStartTLS(to, subject, content)
	} else {
		return mh.sendGomail(to, subject, content)
	}
}

func (mh *MailHelper) sendStartTLS(to string, subject string, content string) error {
	srv := strings.Split(mh.server, ":")
	auth := smtp.PlainAuth("", mh.account, mh.pwd, srv[0])
	msg := []byte(mh._msg(to, subject, content))
	tlsconfig := mh._tlsConfig(srv[0])
	//1. dial tcp using tsl
	smtpClient, err := smtp.Dial(mh.server)
	if err != nil {
		return err
	}
	//2. connect email client to it
	smtpClient.StartTLS(&tlsconfig)
	if err != nil {
		return err
	}
	//3. authenticate
	if err = smtpClient.Auth(auth); err != nil {
		return err
	}
	//err = c.Mail(mh.server, auth, mh.from, []string{to}, msg)
	//4. mail header setup
	if err = smtpClient.Mail(mh.from); err != nil {
		return err
	}
	if err = smtpClient.Rcpt(to); err != nil {
		return err
	}
	//5. inject content
	w, err := smtpClient.Data()
	if err != nil {
		return err
	}
	defer w.Close()
	_, err = w.Write([]byte(msg))
	if err != nil {
		log.Printf("[ERROR] Email Sending Failed with %s for %s\n", err.Error(), to)
		return err
	}
	log.Printf("[INFO] Email has been sent to %s", to)
	return nil
}
func (mh *MailHelper) sendGomail(to string, subject string, content string) error {
	srv := strings.Split(mh.server, ":")
	m := gomail.NewMessage()
	m.SetHeader("From", mh.from)
	m.SetHeader("To", to)
	m.SetHeader("Subject", subject)
	m.SetBody("text/html", content)
	port, err := strconv.Atoi(srv[1])
	if err != nil {
		return err
	}
	d := gomail.NewDialer(srv[0], port, mh.account, mh.pwd)
	if err := d.DialAndSend(m); err != nil {
		log.Printf("[ERROR] Email sending failed with %s for %s\n", err.Error(), to)
		return err
	}
	log.Printf("[INFO] Email has been sent to %s", to)
	return nil
}

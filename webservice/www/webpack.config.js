var path = require('path');
const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const PROD = JSON.parse(process.env.PROD_ENV || '0');
console.log(`${PROD?"*** Production build ***":"*** Dev Build ***"}`);
module.exports = {
    resolve: {
        extensions: ['.js', 'min.js']
    },
    devtool:"source-map",
    externals: {
        "jquery": "jQuery",
        /*"kendo-ui-core":"kendo",*/
        "react":"React",
        "react-dom":"ReactDOM"
    },
    entry: './tsdist/index.js',
    output: {
        path:path.resolve(__dirname, 'dist'),
        filename: 'js/bundle.js'
    },
    module: {
        loaders: [
          { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/ },
          { test: /\.jsx$/, loader: 'babel-loader', exclude: /node_modules/ }
        ]
    },
    plugins: (!PROD) ? undefined : [
        new UglifyJsPlugin({
            sourceMap: true,
            uglifyOptions: {
                "ie8": false,
                "ecma": 8
            }
        })
    ]
}

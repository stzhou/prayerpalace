var path = require('path');
module.exports = {
    resolve: {
        extensions: ['.js', 'min.js']
    },
    devtool:"source-map",
    externals: {
        "jquery": "jQuery",
        "kendo-ui-core":"kendo",
        "react":"React",
        "react-dom":"ReactDOM"
    },
    entry: './tsdist/login.js',
    output: {
        path:path.resolve(__dirname, 'dist'),
        filename: 'js/bundlelogin.js'
    },
    module: {
        loaders: [
          { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/ },
          { test: /\.jsx$/, loader: 'babel-loader', exclude: /node_modules/ }
        ]
    }
}

//rxjs stuff
import { Observable } from 'rxjs-es/Observable';
import { BehaviorSubject } from "rxjs-es/BehaviorSubject"
import 'rxjs-es/add/observable/dom/ajax';
import 'rxjs-es/add/observable/combineLatest';
import 'rxjs-es/add/observable/forkJoin';
//import 'rxjs-es/add/operator/debounceTime';
import 'rxjs-es/add/operator/map';
import 'rxjs-es/add/operator/mergeMap';
import { Subscription } from "rxjs-es/Subscription";

//import 'rxjs-es/add/operator/startWith';
import 'rxjs-es/add/operator/filter';
//import 'rxjs-es/add/operator/switchMap';
import 'rxjs-es/add/operator/catch';
import * as Utils from "./utils"

import {GetLanOption} from "./translate"

export interface PrayerSession {
    sessionID: number,
    title:string,
    desc:string,
    date:Date,
    fromHH:number,
    toHH:number,
    recurringToDate:string|null,
    recurring:{
        mode: number,
        value: Array<number>|null
    }|null,
    timeZoneID:number,
    prayerCatID:number,
    colorCode:string,
    deleted:boolean,
    isActive:boolean,
    leaders: number[]|null
}
export interface Cat {
   catID:number,
   catName:string,
   transID:number
}

export interface MyEvent {
    sessionID:number,
    effectiveTo: Date | null,
    signerID: number,
    date: Date
}

export interface MyEventUpdate {
    sessionID:number,
    effectiveTo:Date|null,
    isDel:boolean,
}

export interface AdmUser {
    "userID":number,
    "userName":string,
    "nickName":string|null,
    "email":string |null,
    "phone":string|null,
    "recoverBy":string,
    "isActive":boolean,
    "created":Date,
    "updated":Date,
    "tempPwd":null,
    "roles":number[],
    "lanID":number
}
export interface Leader {
    id: number,
    name:string
}

export interface Role {
    roleID:number,
    roleName:string,
    transID:null|number,
    isBuiltin:boolean
}
export interface AdmUsers {
    users: AdmUser[],
    roles:Role[]
}
export interface Translation {
    id: number,
    lan2:string,
    lan3:string
}
export interface SignupCount { [sesssionID:number]: number}

export interface TranslationDic {[id:string]:Translation}

class Store{
    PrayerSessions: BehaviorSubject<Array<PrayerSession>> = new BehaviorSubject<Array<PrayerSession>>([])
    Myevents: BehaviorSubject<Array<MyEvent>> = new BehaviorSubject<Array<MyEvent>>([])
    Cats:BehaviorSubject<Array<Cat>> = new BehaviorSubject<Array<Cat>>([]);

    Translations:BehaviorSubject<TranslationDic> = new BehaviorSubject<TranslationDic>({});

    AdmUsers:BehaviorSubject<AdmUsers> = new BehaviorSubject<AdmUsers>(null);
    Leaders:BehaviorSubject<Leader[]> = new BehaviorSubject<Leader[]>([])
    Permissions:BehaviorSubject<string[]>  = new BehaviorSubject<string[]>([]);
    SignupCount:BehaviorSubject<SignupCount>  = new BehaviorSubject<SignupCount>({});

    toLoginPage(){
        window.location.assign("/login");
    }
    constructor(){
        this.GetTranslations();
        this.GetPermissions();
        this.GetLeaders();
        this.GetSignupCount();
        window.setInterval(() => {
            this.GetSignupCount();
        }, 5 * 60 * 1000);
    }
    GetCats = () => {
        let _TransSub:Subscription = null;
        _TransSub= this.Translations.subscribe((transData:TranslationDic)=>{
            Observable.ajax("/api/cat").map((res) => {
                return res.response;
            }).subscribe((res:Cat[]) => {
                _TransSub.unsubscribe();
                const lan = GetLanOption();
                const isLanEN = lan=="en";
                res = res.map((cat)=>{
                    if (!isLanEN && transData[cat.transID]) {
                        cat.catName = lan == 'zh' ? transData[cat.transID].lan2 : transData[cat.transID].lan3;
                    }
                    return cat;
                });
                this.Cats.next(res);
            });
        });
    }
    GetTranslations = ()=>{
        Observable.ajax("/api/translation").map((res) => {
            return res.response
        }).subscribe((res:Translation[]) => {
            const dic = {};
            res.forEach((item)=>{
                dic[item.id] = item;
            })
            this.Translations.next(dic);
        });
    }
    GetPrayerSessions=()=>{
        Observable.ajax("/api/session")
            .catch((err, caught) => {
                console.error("GetPrayerSessions", err)
                if (err.status === 401) {
                    this.toLoginPage();
                }
                return [];
            })
            .map((res) => {
                const data = res.response as PrayerSession[];
                data.forEach((ps)=> {
                    /**
                     * Notice it doesn't handle the case that toHH is on another day and at the same time on another timezone.
                     */
                    const utcDayBegin = Utils.parseTZlessTimeAsUtc(ps.date+"")
                    //convert from HH and to HH to browser time:
                    const localTimeFromHH = Utils.getLocalTimeFromDateAndHH(utcDayBegin, ps.fromHH);

                    ps.fromHH = localTimeFromHH.getHours();
                    const localTimeToHH = Utils.getLocalTimeFromDateAndHH(utcDayBegin, ps.toHH);
                    ps.toHH = localTimeToHH.getHours();
                    ps.date = localTimeFromHH;
                    ps.date.setHours(0,0,0,0);
                });
                return data;
            }).subscribe((res)=>this.PrayerSessions.next(res))
    }
    ChangeMyPassword = (payload:{oldPwd:string, newPwd:string}, callback:(res:{ok:boolean, originalResponse:any})=>void)=>{
        Observable.ajax({
            url:'/api/changepwd',
            method:"POST",
            body: JSON.stringify(payload),
            headers:{
                "Content-Type":"application/json"
            }
        })
        .catch((err,caught)=>{
            if(err){
                callback({ok:false, originalResponse:err})
                return null;
            }
        }).subscribe((res)=>{
            callback({
                ok:true,
                originalResponse:res
            })
        });
    }
    UpsertMyevents= (payLoad: MyEventUpdate)=>{
        Observable.ajax.post("/api/myevent",
            JSON.stringify(payLoad),
            { 'Content-Type': "application/json" }).subscribe(() => {
                this.GetMyEvents()
            });
    }
    UpsertSessions=(payLoad:Array<PrayerSession>): Observable<any> =>{
        const observableQueue = [];
        payLoad.forEach((ps)=>{
            if(ps.date){
                //verify it is an object
                if(typeof ps.date['getDate'] !=="function"){
                    console.error('ps.date should be Date type.')
                }else{
                    (<any>ps.date) = `${ps.date.getFullYear()}/${ps.date.getMonth()+1}/${ps.date.getDate()} 00:00:00`
                }
            }
            const ps$=Observable.ajax({
                url: `/api/session${ps.sessionID > 0 ? '/' + ps.sessionID : ''}`,
                method: ps.sessionID > 0 ? "POST" : "PUT",
                body:JSON.stringify(ps),
                headers:{
                    "Content-Type":"application/json"
                }
            });
            observableQueue.push(ps$);
        });
        return Observable.forkJoin(observableQueue)
    };

    GetMyEvents=()=>{
        Observable.ajax("/api/myevent")
            .catch((err, caught) => {
                console.error("GetMyEvents", err)
                if (err.status === 401) {
                    this.toLoginPage();
                }
                return [];
            }).map((res) => {
                const data = res.response as MyEvent[];
                data.forEach((ps)=> {
                    if (ps.effectiveTo){
                        ps.effectiveTo = Utils.parseTZlessTimeAsUtc(ps.effectiveTo+"")
                    }
                    ps.date = Utils.parseTZlessTimeAsUtc(ps.date+"")
                });
                return data;
            }).subscribe((res)=>{
                this.Myevents.next(res);
            })
    }
    GetLeaders = ()=>{
        Observable.ajax("/api/sessionleader").catch((err,caught)=>{
            console.error("sessionleader", err)
            if (err.status==401){
                this.toLoginPage()
            }
            return [];
        }).map( (res)=>{
            return res.response;
        }).subscribe((res)=>{
            this.Leaders.next(res)
        });
    }

    GetPermissions = ()=>{
        Observable.ajax('/api/permission').catch((err, caught)=>{
            console.error("GetPermissions", err);
            if (err.status==401){
                this.toLoginPage()
            }
            return [];
        }).map((res)=>res.response)
        .subscribe((res)=>this.Permissions.next(res));
    }
    GetSignupCount = ()=>{
        Observable.ajax('/api/signupcount').map((res)=>res.response)
        .subscribe((res)=>{
            const dic={};
            res.forEach((item)=>{
                dic[item.sessionID] = item.count;
            });
            this.SignupCount.next(dic);
        });
    }
    /*ADMIN area START*/
    GetAdmUsers = ()=>{
        Observable.ajax("/api/admuser").catch((err,caught)=>{
            console.error("GetAdmUsers", err)
            if (err.status==401){
                this.toLoginPage()
            }
            return [];
        }).map( (res)=>{
            const data = res.response as AdmUsers;
            data.users.forEach((u)=>{
                u.updated = Utils.parseTZlessTimeAsUtc(u.updated+"");
                u.created = Utils.parseTZlessTimeAsUtc(u.created+"")
            });
            return data
        }).subscribe((res)=>{
            this.AdmUsers.next(res)
        });
    }
    UpdateAdmUsers = (user:AdmUser, callback:(result:any)=>void)=>{
        Observable.ajax.post("/api/admuser",JSON.stringify(user),{"Content-Type":"application/json"})
        .catch((err,caught)=>{
            console.error("UpdateAdmUser", err);
            if (err.status==401){
                this.toLoginPage()
            }
            return caught;
        }).subscribe((res)=>{
            callback(res);
        })
    }
    /*ADMIN area END*/

}
const store = new Store();
export {store}
function getBrowserLans(){
    const nav: any = window.navigator;
    let lanDefault = 'en-us';
    if (nav.languages) {
        return nav.languages;
    } else {
        const _lan = nav.userLanguage || nav.language || lanDefault;
        return [_lan];
    }
}
export function GetLanOption(){
    const browserLans = getBrowserLans();
    const isEnOthers = /en\-+/, isZhOthers = /zh\-+/;
    //samples: ["en-US", "en", "zh", "zh-TW", "zh-HK"]
    const supportedLans=[isZhOthers, 'zh', 'en', isEnOthers];
    let lan='en'
    for(let i=0;i<browserLans.length;i++){
        if (isZhOthers.test(browserLans[i])) {
            lan = 'zh-TW';
            break;
        }
        if (browserLans[i] == 'zh') {
            lan = browserLans[i];
            break;
        }
        if( isEnOthers.test(browserLans[i])){
            lan = 'en';
            break;
        }
    }
    console.log('language detected: ', lan)
    return lan;
}
//import "kendo-ui-core"
import * as React from "react"
import * as ReactDOM from "react-dom"
import { BrowserRouter as Router, Route, Link } from "react-router-dom"
import {T} from "t-i18n"
import * as trans from "./translate"
import {en} from "./messages.en"
import {zh} from "./messages.zh"
import {zhtw} from "./messages.zhtw"
const lan = trans.GetLanOption();
console.info(`Applying language:${lan}.`);
T.setup({
    locale: lan,
    messages:{
       en: en,
       zh: zh,
       'zh-TW':zhtw,
    }
});


import { Calendar } from "./Components/Calendar"
import { store } from "./indexStore"
import { SessionEdit } from "./Components/SessionEdit"

import {UserAdmin} from "./Components/UserAdmin"
import {PageHeader} from "./Components/PageHeader"
import {ChangePassword} from "./Components/ChangePassword"



interface Props{

}
interface State{

}

const About = (router)=>(
    <div>
        <PageHeader subTitle={ T("About") } enabled={true} goBackVisible={true} goBack={()=>{router.history.push("/index")}} />
        <h2 style={{textAlign:'center'}}>{T("Prayer Palace")}</h2>
        <p style={{margin:"40px"}}>
            Prayer Palace is the portal application for Omega Ministry - a non-profit Christian organization located in Toronto, Canada.
            We use this portal to publish prayer schedules to our subscribers.
        </p>
    </div>
)
const Logout = (router)=>{
    document.cookie="Auth=;path=/";
    window.location.assign('/login');
    return <div>{T('Log out')} ...</div>
}

const _SessionEdit = ({match})=>{
    const date: string = match.params.date;
    return <SessionEdit date={date} />
};


class Main extends React.Component<Props,State> {
    constructor(props:Props, state:State) {
        super(props, state);
    }

    render() {
        return (
            <Router>
                <>
                <Route exact path="/index" component={Calendar} />
                <Route path="/index/about" component={About} />
                <Route path="/index/sessionedit/:date" component={_SessionEdit} />
                <Route path="/index/useradmin" component={UserAdmin} />
                <Route path="/index/changepassword" component={ChangePassword} />
                <Route path="/index/logout" component={Logout} />
                </>
            </Router>
        )
    }
}


ReactDOM.render(
    <Main />,
    document.getElementById('root')
)

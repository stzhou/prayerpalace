import * as React from "react"
import * as ReactDOM from "react-dom"
import { BrowserRouter as Router, Route, Link } from "react-router-dom"

//rxjs stuff
import { Observable } from 'rxjs-es/Observable';
import 'rxjs-es/add/observable/dom/ajax';
import 'rxjs-es/add/observable/combineLatest';
import 'rxjs-es/add/operator/debounceTime';
import 'rxjs-es/add/operator/map';
import 'rxjs-es/add/operator/startWith';
import 'rxjs-es/add/operator/filter';
import 'rxjs-es/add/operator/switchMap';
import 'rxjs-es/add/operator/catch';


import {LoginTestList} from "./Components/LoginTestList"
import * as Store from "./loginStore"

import {T} from "t-i18n"
import * as trans from "./translate"
import {en} from "./messages.en"
import {zh} from "./messages.zh"
import {zhtw} from "./messages.zhtw"

const lan = trans.GetLanOption();
console.info(`Applying language:${lan}.`);
T.setup({
    locale: lan,
    messages:{
       en: en,
       zh: zh,
       'zh-TW':zhtw,
    }
});

interface Props{

}
interface State{

}

interface DefaultPageState{
    mode: "LOGIN" | "FORGOT" | "SIGNUP"
    , formErrors: string[]
    , notRobot: boolean
    , loginFailed:boolean,
    userNameEmailLogin: string,
    userNameSignup: string,
    userFirstName: string;
    userLastName: string;
    emailSignup: string;
    password: string;
    password2: string;
}
//rulename: 8 chars with 1 U, 1 L, 1 Num.
const passwordRule={
  expr:/(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}/,
  desc:T("8 characters with at least 1 uppercase, 1 lowercase, 1 number.")
};
const emailValidator=/\w+@\w+\.\w+/;
class DefaultPage extends React.Component<{},DefaultPageState   > {
    constructor(props: {}, state: DefaultPageState) {
        super(props, state);
        this.state = {
            mode: "LOGIN",
            formErrors: [],
            notRobot:false,
            loginFailed:false,

            userNameEmailLogin:'',
            userNameSignup: "",
            userFirstName: "",
            userLastName: "",
            emailSignup: "",
            password: "",
            password2: "",
        }
    }


    tryLogin = ()=>{
        const newFormErrors = [];
        this.setState({ formErrors: [], loginFailed:false});
        //1 verification
        if (this.state.notRobot==false){
            newFormErrors.push(T("Not robot is not verified"));
        }
        if(!this.state.userNameEmailLogin){
            newFormErrors.push(T("User name or email can not be empty"));
        }
        if(!this.state.password){
            newFormErrors.push(T("Password can not be empty"));
        }

        //2 display errors and return.
        //prevent set empty array to empty array:
        this.setState({ formErrors: newFormErrors });
        if (newFormErrors.length !==0){
            return;
        }
        Store.store.tryLogin(this.state.userNameEmailLogin, this.state.password, (res)=>{
            if (res.ok) {
                window.location.assign("/");
                this.setState({ loginFailed: false });
            } else {
                this.setState({ loginFailed: true });
            }
        })
    }
    resetPassword = ()=>{
        if (!this.state.userNameEmailLogin){
            alert(T("Enter your username / email to reset your password"));
            return;
        }
        Store.store.resetPassword(this.state.userNameEmailLogin, (res)=>{
            if (!res.ok) {
                alert(T("Sorry, we can't reset your password."))
            } else {
                alert(T("Please check your email shortly for your new password"));
            }
        });
    }
    trySignupByEmail = ()=>{
        this.setState({ formErrors: [], loginFailed:false});
        const newFormErrors = [];
        //1 verification
        if (this.state.notRobot==false){
            newFormErrors.push(T("Not robot is not verified"));
        }
        if (!this.state.userFirstName) {
            newFormErrors.push(T("First name can not be empty"));
        }
        if(!this.state.userNameSignup){
            newFormErrors.push(T("User name can not be empty"));
        }
        if(!this.state.emailSignup){
            newFormErrors.push(T("Email can not be empty"));
        } else if (!emailValidator.test(this.state.emailSignup)) {
            newFormErrors.push(T("Email address is invalid"));
        }
        if(!this.state.password){
            newFormErrors.push(T("New password cannot be empty"))
        }

        if(this.state.password2 !== this.state.password){
            newFormErrors.push(T("Reentered password doesn't match first time"));
        }

        if (!passwordRule.expr.test(this.state.password)){
            newFormErrors.push(T("Password should be")+(" : ") + passwordRule.desc)
        }

        //2 display errors and return.
        //prevent set empty array to empty array:
        this.setState({
            formErrors: newFormErrors
        });
        if (newFormErrors.length !==0){
            return;
        }
        Store.store.registerUser({
            userName: this.state.userNameSignup,
            firstName:this.state.userFirstName,
            lastName:this.state.userLastName||"",
            pwd:this.state.password,
            email:this.state.emailSignup,
            lanid:1,
            tz:''
        }, (res)=>{
            if (res.ok) {
                alert(T("Please login now. Registration succeeded."));
                this.setState({ mode: "LOGIN" })
            } else {
                alert(T("For some reason, we cann't register you for now, sorry."));
                console.error("registration failed ", res.originalResponse)
            }
        });
    }
    componentDidMount(){
        const root = document.getElementById("root");
        const resizeRoot = ()=>{
            const height = window.innerHeight;
            root.setAttribute("style",`height:${height - 60}px`);
        }
        window.addEventListener("resize",()=>{
            resizeRoot();
        });
        resizeRoot();
    }

    notRobotClick = (evt:any)=>{
        if (evt.isTrusted) {
            this.setState({ notRobot: true });
            return;
        }
        this.setState({ notRobot: false });
    };
    switchMode = (mode)=>{
        if(mode==="LOGIN"|| mode==="SIGNUP"){
        }
        this.setState({
            mode: mode,
            userFirstName : "",
            userLastName  : "",
            emailSignup : "",
            password : "",
            password2 : "",
            userNameEmailLogin : "",
            formErrors:[]
        })
    };
    handleEnterSubmit = (e: any)=>{
        if (e.keyCode == 13) {
            switch (this.state.mode) {
                case "LOGIN":
                    this.tryLogin();
                    break;
                case "SIGNUP":
                    this.trySignupByEmail();
                    break;
                case "FORGOT":
                    this.resetPassword();
                    break;
            }
        }
    };


    render(){
        if(this.state.mode=="LOGIN"){
            return (
                <>
                <h3>{T('Welcome to Prayer Palace')}</h3>
                <form className="login" onKeyUp={e=>this.handleEnterSubmit(e)}>
                    {this.state.loginFailed &&(
                        <h3><span className="label label-danger">{T('Sorry, invalid login.')}</span></h3>
                    )}
                    <input name="user-name-email" type="text" placeholder={T('User name or email')} required value={this.state.userNameEmailLogin} onChange={(e) => this.setState({userNameEmailLogin:e.target.value})} />
                    <input name="password"  type='password' placeholder={T('Password')} required value={this.state.password} onChange={(e) => this.setState({password:e.target.value})} />
                    <div>
                        <button type="button" className={`btn ${this.state.formErrors.length?"btn-danger":"btn-primary"}`} onClick={(e)=>this.tryLogin()}>{T('Login')}</button>&nbsp;&nbsp;or&nbsp;&nbsp;
                        <button type="button" className="btn btn-default" onClick={e=>this.switchMode("SIGNUP")}>{T('Singup')}</button>

                        { this.state.loginFailed &&
                         (
                            <>
                            &nbsp;&nbsp;or&nbsp;&nbsp; <button type="button" className="btn btn-warning" onClick={this.resetPassword}>{T('Reset Password')}</button>
                            </>
                         )
                        }
                    </div>
                    <div>
                        <label className='lb-notrobot'><input type='checkbox' className="ck-notrobot" onChange={this.notRobotClick} checked={this.state.notRobot} defaultChecked={false} /> &nbsp;&nbsp; {T('I am not a robot')}</label>
                    </div>
                    {this.state.formErrors.length !== 0 && (
                        <div>{T('Please correct these errors to login')}</div>
                    )}

                    {this.state.formErrors.length !== 0 && (
                        <ul className='ls-err'>
                            {this.state.formErrors.map((p, idx) =>
                                <li key={idx}>{p}</li>
                            )}
                        </ul>
                    )}
                </form>
                </>
            );

        }
        if(this.state.mode=="SIGNUP"){
            return (
                <>
                <h3>{T('Welcome to Prayer Palace')}</h3>
                <form className="login" onKeyUp={e=>this.handleEnterSubmit(e)}>
                    <input name="firstname" type="text" placeholder={T("Your first name")} value={this.state.userFirstName} onChange={e=>this.setState({userFirstName: e.target.value})} />
                    <input name="lastname" type="text" placeholder={T("Your last name (optional)")} value={this.state.userLastName} onChange={e=>this.setState({userLastName: e.target.value})}/>
                    <input name="username" type="text" placeholder={T("Your user name (for login)")} value={this.state.userNameSignup} onChange={e=>this.setState({userNameSignup :e.target.value})} />
                    <input name="email" type='text' placeholder={T("Your email (for login and accout)")} value={this.state.emailSignup} onChange={e => this.setState({emailSignup: e.target.value})} />
                    <input name="pwd" type='password' placeholder={T("Password, e.g. abc9Def2")} value={this.state.password} onChange={e => this.setState({password:e.target.value})} />
                    <input name="pwd2" type='password' placeholder={T("Password again")} value={this.state.password2} onChange={e => this.setState({password2:e.target.value})} />
                    <div>
                        <button type="button" className={`btn ${this.state.formErrors.length?"btn-danger":"btn-primary"}`} onClick={(e) => this.trySignupByEmail()}>{T('Singup')}</button>&nbsp;&nbsp;or&nbsp;&nbsp;
                        <button type="button" className="btn btn-default " onClick={(e) => this.switchMode("LOGIN")}>{T('Login')}</button>
                    </div>
                    <div>
                        <label className='lb-notrobot'><input type='checkbox' className="ck-notrobot" onChange={this.notRobotClick} checked={this.state.notRobot} defaultChecked={false} /> &nbsp;&nbsp; {T('I am not a robot')}</label>
                    </div>
                    {this.state.formErrors.length !== 0 && (
                        <div>{T('Please correct these errors to signup')}</div>
                    )}
                    {this.state.formErrors.length !== 0 && (
                        <ul className='ls-err'>
                            {this.state.formErrors.map((p, idx) =>
                                <li key={idx}>{p}</li>
                            )}
                        </ul>
                    )}
                </form>
                </>
            );
        }
        return (<div></div>);
    }
}

const ForgotPassword = ()=>(
    <div>
        <h2> Be forgiven, forget no more.</h2>
    </div>
)



class LoginMain extends React.Component<Props,State> {
    constructor(props:Props, state:State) {
        super(props, state);
    }

    render() {
        return (
            <Router>
                <>
                    <Route exact path="/login" component={DefaultPage} />
                    <Route path="/login/forgotpassword" component={ForgotPassword} />
                </>
            </Router>
        )
    }
}


ReactDOM.render(
    <LoginMain />,
    document.getElementById('root')
)
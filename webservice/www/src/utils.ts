

export function datesAreEqualInLocal(d1:Date, d2:Date):boolean{
    return (d1.getFullYear() == d2.getFullYear() &&
     d1.getMonth() == d2.getMonth() &&
     d1.getDate() == d2.getDate()
    )
}
export function datesAreEqualInUtc(d1:Date, d2:Date):boolean{
    return (d1.getUTCFullYear() == d2.getUTCFullYear() &&
     d1.getUTCMonth() == d2.getUTCMonth() &&
     d1.getUTCDate() == d2.getUTCDate()
    )
}
/*
* convert to yyyy-mm-dd using padding.
*/
export function dateToInputValueInLocal(d:Date):string{
    return `${d.getFullYear()}-${("0"+(d.getMonth()+1)).slice(-2)}-${ ("0"+d.getDate()).slice(-2)}`;
}
export function parseInputDateInLocal(yyyy_mm_dd:string):Date{
    const segments = yyyy_mm_dd.split('-');
    if (segments.length!==3) throw ("bad string format, date should be seperated by dash");
    const yyyy= parseInt(segments[0]);
    const mm = parseInt(segments[1]);
    const dd = parseInt(segments[2])
    return new Date(yyyy,mm-1,dd);
}
export function getLocalTimeFromDateAndHH(utcTime:Date, hh:number){
    return new Date(utcTime.getTime()+ hh*3600*1000)
}
export function parseTZlessTimeAsUtc(TZlessUtcTime: string): Date {
    return new Date((TZlessUtcTime).replace(" ", "T") + "Z")
}
export function HHtoampm(hh:number, capital:boolean=false):string {
    const am=capital?'AM':'am';
    const pm=capital?'PM':'pm';
    if (hh >=0 && hh <=12) return `${hh}${am}`;
    return `${hh-12}${pm}`;
}

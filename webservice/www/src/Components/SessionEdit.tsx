import * as React from "react"
import * as Store from "../indexStore"
import { BehaviorSubject } from "rxjs-es/BehaviorSubject"
import { Subscription } from "rxjs-es/Subscription"
import * as utils from "../utils"
import {PageHeader} from "./PageHeader"
import {HourPicker} from "./HourPicker"
import {Redirect} from "react-router-dom"
import {RecurringEdit} from "./RecurringEdit"
import {CatPicker} from "./CatPicker"
import {KDatePicker} from "./KDatePicker"
import {LeaderList} from "./LeaderList"
import {T} from "t-i18n"

//this page takes a 'date' from url segment, and list all sessions for editing, plus adding.
//this page will complete and go back to Calendar view.
//Calendar - Day mode will provide button create/edit to link to here.
interface Props {
    /**
     * It is always a date relavant to the user/browser
     */
    date: string
}
interface State {
    prayerSessions: Store.PrayerSession[],
    mode:"Edit"|"NA"|"BACK",
    selectedSessionID:number|null,
    cats: Store.Cat[],
    permissions: string[]
}
export class SessionEdit extends React.Component<Props, State> {
    constructor(props:Props, state:State){
        super(props, state);
        this.state={
            prayerSessions:[],
            mode:"NA",
            selectedSessionID:null,
            cats:[],
            permissions:[]
        }
        this.updatedID=[];
    }
    date: Date|null;
    updatedID:Array<number>;
    prayerSessionsBak: Store.PrayerSession[];

    prayerSessionSubscription: Subscription;
    catSubscription:Subscription;
    permissionSubscription: Subscription;


    cancelClick = (event:any)=>{
        event.preventDefault();
        event.stopPropagation();
        return false;
    }
    //expecting date is yyyy-mm-dd
    parseDate(dateStr:string){
        const curDate = new Date();
        curDate.setHours(0,0,0,0);
        //expecting date is yyyy-mm-dd
        const parts = this.props.date.split('-');
        if (parts.length !=3) return null;
        const yy = parseInt(parts[0]);
        if (yy < curDate.getFullYear()) {
            console.error("Date format year is older than current year.");
            return null;
        }
        const mm = parseInt(parts[1]);
        //month starts with zero
        if (mm > 12 || mm < 1){
            console.error("Date format error for month");
            return null;
        }
        const dd = parseInt(parts[2]);
        if ( dd > 31 || dd < 1){
            console.error("Date format error for day");
            return null;
        }
        const parsedDate = new Date(yy,mm-1,dd);
        if (parsedDate < curDate){
            console.error("Selected a past date for editing, that is illegal");
            return null;
        }
        //return a local date according to browser.
        return parsedDate;
    }
    componentWillMount(){
        this.prayerSessionSubscription = Store.store.PrayerSessions.subscribe((data) => {
            this.setState({ prayerSessions: data })
            const copy = data.map(p=> Object.assign({},p));
            this.prayerSessionsBak = copy;
        });
        this.catSubscription = Store.store.Cats.subscribe((data)=>{
            this.setState({cats:data});
        });
        this.permissionSubscription = Store.store.Permissions.subscribe((data)=>{
            this.setState({permissions:data});

            //Only get Prayer Session after permissions are known, or api will fail.
            Store.store.GetPrayerSessions();
        })

        Store.store.GetCats();

        //expecting date is yyyy-mm-dd
        const parsedDate = this.parseDate(this.props.date);
        this.date = parsedDate;
        if (!parsedDate) {
            this.setState({ mode: "NA" });
        } else {
            this.setState({ mode: "Edit" });
        }
    }

    componentDidMount(){
        this.componentWillReceiveProps(this.props);
    }
    componentWillUnmount(){
        this.prayerSessionSubscription.unsubscribe();
        this.catSubscription.unsubscribe();
        this.permissionSubscription.unsubscribe();
    }

    componentWillReceiveProps(nextProps:Props){
        if (nextProps.date !== this.props.date) {
            const parsedDate = this.parseDate(nextProps.date);
            this.date = parsedDate;
            if (!parsedDate){
                this.setState({mode:"NA"});
            }else{
                this.setState({ mode: "Edit" });
            }
            Store.store.GetPrayerSessions();
        }
    }
    toggleSessionDetail = (sessionID: number)=>{
        if (this.state.selectedSessionID === sessionID){
            this.setState({selectedSessionID:null});
        }else{
            this.setState({selectedSessionID:sessionID});
        }
    }
    updateField  = (sessionID:number, field:any)=>{
        /*if (field['isActive']!==undefined){
            field['isActive'] = field.isActive=="true";
        }*/
        let cursessions= this.state.prayerSessions ||[];
        const len = cursessions.length;
        const itemIdx = cursessions.findIndex(p => p.sessionID == sessionID);
        if (itemIdx > -1) {
            //this will not trigger re-render! array is not re-pointed.
            cursessions = cursessions.slice();
            const updated = Object.assign({}, cursessions[itemIdx], field);
            cursessions.splice(itemIdx, 1, updated);
            this.setState({ prayerSessions: cursessions });
            if (sessionID > 0 && this.updatedID.indexOf(sessionID) == -1) {
                this.updatedID.push(sessionID);
            }
        }
        return true;
    }
    addNewSession = ()=>{
        const newSession:Store.PrayerSession = {
          sessionID: -(this.state.prayerSessions.length +1),
          fromHH:0,
          toHH:0,
          title:T('New Session'),
          desc:'',
          recurringToDate: null,
          timeZoneID:1,
          prayerCatID:1,
          colorCode:'',
          isActive:false,
          date: this.date,
          recurring:null,
          deleted:false,
          leaders:null
        };
        const newArray = [...this.state.prayerSessions, newSession];
        this.setState({
            prayerSessions: newArray,
            selectedSessionID: newSession.sessionID
        });
    }
    CancelChange = ()=>{
        const copy = this.prayerSessionsBak.map(p => Object.assign({}, p));
        this.setState({
            prayerSessions: copy,
            selectedSessionID:null
        });
        this.updatedID=[];
    }
    onGoHome = ()=>{
          this.setState({mode:"BACK"});
    }
    onSave = ()=>{
        //cleanup data and push:
        const payload = [];
        //considering user might change the date, all dates that has unsaved items are to be checked for conflict:
        const involvedDates = new Set([]);
        this.state.prayerSessions.forEach((ps) => {
            if (ps.sessionID > 0 && this.updatedID.indexOf(ps.sessionID) == -1) {
                return;
            }
            const item = Object.assign({}, ps);
            item.timeZoneID = 1;
            item.colorCode = "#000000";
            item.prayerCatID = item.prayerCatID || 1;
            item.recurringToDate = null;
            involvedDates.add(item.date)
            payload.push(item);
        });
        let noConflict=true;
        involvedDates.forEach((da)=>{
            const activeSessions = this.state.prayerSessions.filter(p => utils.datesAreEqualInLocal(p.date, da) && p.isActive);
            noConflict = noConflict && !this.findTimeSlotConflict(activeSessions);
        });
        if (!noConflict) {
            window.setTimeout(() => {
                alert(T("Detected session time overlap, please adjust before saving."))
            }, 0);
            return
        }
        Store.store.UpsertSessions(payload).subscribe((val) => {
            this.updatedID = [];
            this.setState({ mode: "BACK" });
        }, (err) => {
            if (err.status == 409) {
                Store.store.GetPrayerSessions();
                alert(T("Detected session time overlap, overlapped session is saved as inactive."))
            }
            else {
                alert(T("Ooops, unknown error when saving."))
                console.error(err);
            }
        });
    };
    findTimeSlotConflict(sessions: Store.PrayerSession[]){
        if (!sessions || sessions.length===0) return false;
        const activeSessions = sessions.filter(p => p.isActive);
        let hourSlots = new Set([]);
        let hasConflict = false;
        //utilize the fact of exact hour to avoid time range algorithm.
        activeSessions.forEach((session) => {
            const from = session.fromHH; let to = session.toHH;
            if (from > to) { to = to + 24; }
            for (let i = from; i < to; i++) {
                if (hourSlots.has(i)) {
                    hasConflict = true;
                    return hasConflict;
                } else {
                    hourSlots.add(i)
                }
            }
        });
        return hasConflict;
    }
    render(){
        if(this.state.permissions.indexOf("Edit-PrayerSession")==-1){
            return (
                <>
                    <PageHeader subTitle={"Edit Sessions"} enabled={true} goBackVisible={true} goBack={this.onGoHome} />
                    <h4 style={{ textAlign: "center" }}>{T("Seams like you do not have permission for editing sessions")}</h4>
                </>
            )
        }
        if (this.state.mode==="BACK"){
            return <Redirect to='/index' />
        }
        //according to this.state.mode,
        //render component to do Add session / EditORDelete session
        const sessionsOfdate = this.state.prayerSessions.filter(ps=>{
            //remove sessions for other days, unless it is updated now.
            return utils.datesAreEqualInLocal(ps.date, this.date) || this.updatedID.indexOf(ps.sessionID) > -1
        });
        if (this.state.mode=="Edit"){
            //filter all sessions for the date
            //and provide 'Add' button to add session
            return(
                <>
                <PageHeader subTitle={T("Edit Sessions")} enabled={true} goBackVisible={true} goBack={this.onGoHome} />

                <h4 className="event-date"><span className="label label-primary ">{this.date.toLocaleDateString()}</span></h4>
                <ul className="day-session-list">
                {
                    sessionsOfdate.map((ps)=>{
                        const liClassName = [];
                        liClassName.push("session-list-item");
                        const showDetail= ps.sessionID === this.state.selectedSessionID;
                        if (showDetail){
                            liClassName.push("detail")
                        }
                        return (
                            <li key={ps.sessionID} className={liClassName.join(' ')}
                                onClick={(e)=>this.toggleSessionDetail(ps.sessionID)}>
                                <div className={`session ${ps.isActive?"active":""} ${ps.deleted?"deleted":""}` } >
                                    <span className="s1">{ps.title}</span>
                                    <span className="s2">{ utils.HHtoampm(ps.fromHH)}</span>
                                    <span className="s3">-</span>
                                    <span className="s2">{utils.HHtoampm(ps.toHH)}</span>
                                </div>
                                {!showDetail?"":(
                                    <div className="detail edit" onClick={this.cancelClick}>
                                        <label htmlFor="title">{T("Title")}</label>
                                        <input id="title" name='title' value={ps.title} onChange={(e)=>{ this.updateField(ps.sessionID,{title: e.target.value}) }} />
                                        <label htmlFor="description">{T("detail")}</label>
                                        <textarea id='description' name="description" value={ps.desc} onChange={(e)=>{ this.updateField(ps.sessionID,{desc: e.target.value}) }} />
                                        <label className='inline'>
                                            {T("Prayer Category")} <CatPicker cats={this.state.cats} val={ps.prayerCatID} onCatChange={(e) => { this.updateField(ps.sessionID, { prayerCatID: e }) }} />
                                        </label>
                                        <label className='inline'>{T("From Hour")} <HourPicker onSelected={(val) => { this.updateField(ps.sessionID, { fromHH: val }) }} min={0} max={24} val={ps.fromHH} /> </label>
                                        <label className='inline'>{T("To Hour")} <HourPicker onSelected={(val) => { this.updateField(ps.sessionID, { toHH: val }) }} min={0} max={24} val={ps.toHH} /> </label>
                                        <label>{T("Leader")}</label>
                                        <LeaderList UserIDs={ps.leaders} OnUserIDsChanged={(val)=>{ this.updateField(ps.sessionID, {leaders:val})}} />
                                        <label> {T("Recurring")} </label>
                                        <RecurringEdit recurring={ps.recurring} onChange={(data) => { this.updateField(ps.sessionID, { recurring: data });}} />
                                        <label htmlFor='pp-date' className='inline'> {T("Date")}
                                            <KDatePicker value={ps.date} minDate={ new Date()}
                                            onValueChange={(val)=>this.updateField(ps.sessionID,{date:val})} />
                                        </label>

                                        <div className="btn-group" role="group">
                                            <button type="button" className={`btn ${ps.isActive?"btn-warning":"btn-success"}`} onClick={(e)=>{this.updateField(ps.sessionID, {isActive: !ps.isActive})}}>
                                                {ps.isActive?T("Deactivate"):T("Activate")}
                                            </button>
                                            <button type="button" className="btn btn-danger" onClick={(e)=>{this.updateField(ps.sessionID, {deleted: !ps.deleted})}}>
                                                {ps.deleted?T("Undelete"):T("Delete")}</button>
                                        </div>
                                    </div>
                                )}
                            </li>
                        )
                    })
                }
                </ul>
                <div className='btn-group session-actions'>
                    <button className="btn btn-primary" onClick={this.addNewSession}>{T("Add New Session")}</button>
                    <button className="btn btn-default" onClick={this.CancelChange}>{T("Cancel")}</button>
                    <button className="btn btn-success" onClick={this.onSave}>{T("Save")}</button>
                </div>
                </>
            )
        }
        return <div>{T("The url provided invalid parameters. I cann't edit session for you.")}</div>
    }
}
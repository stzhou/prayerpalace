import * as React from "react"
import * as Store from "../indexStore"
import {PageHeader} from "./PageHeader"
import {RouteComponentProps} from "react-router-dom"
import {T} from "t-i18n"

interface State {
    oldPwd:string;
    newPwd:string;
    newPwd2:string;
    done:boolean;
    submitCliked:boolean;
    serverError:string;
}

//Most easy way to expose Router method for routing:
export class ChangePassword extends React.Component<RouteComponentProps<{}>, State> {
    constructor(props:RouteComponentProps<{}>, state:State){
        super(props, state);
        this.errors = [];
        this.state = {
            oldPwd: '',
            newPwd: '',
            newPwd2: '',
            done:false,
            submitCliked:false,
            serverError:''
        };
    }
    passwordRule = {
        expr: /(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}/,
        desc: '8 characters with at least 1 uppercase, 1 lowercase, 1 number.'
    };
    errors:string[];
    serverError=T('Sorry, We cannot reset your password.');
    submit=()=>{
        if (this.state.done) return;
        this.setState({serverError:''});
        this.setState({submitCliked:true}, ()=>{
            //if it is not server error don't submit
            if (this.errors.length>0){
                return;
            }
            Store.store.ChangeMyPassword({ oldPwd: this.state.oldPwd, newPwd: this.state.newPwd }, (res) => {
                if (!res.ok) {
                    this.setState({ serverError: this.serverError })
                }
                this.setState({ done: res.ok });
            })
        });
    }
    componentWillUpdate(nextProps:RouteComponentProps<{}>, nextState:State){
        //sychronize errors from states: should never set state here.
        if (nextState.done==false && nextState.submitCliked==true) {
            const errors = [];
            if (!nextState.newPwd) {
                errors.push(T("New password cannot be empty"))
            }
            if (!nextState.oldPwd) {
                errors.push(T("Old password cannot be empty"))
            }
            if (nextState.newPwd != nextState.newPwd2) {
                errors.push(T("Reentered password doesn't match first time"))
            }
            if (!this.passwordRule.expr.test(nextState.newPwd)) {
                errors.push(T("Password should be") +" : "+ T('8 characters with at least 1 uppercase, 1 lowercase, 1 number.'))
            }
            this.errors = errors;
        } else {
            this.errors=[];
        }
    }
    render() {
        return (
            <>

            <PageHeader subTitle={T("About")} enabled={true} goBackVisible={true} goBack={()=>{this.props.history.push("/index")}} />
            <div className='change-pwd-form'>
            <div>
                <label>{T('Current Password')}</label>
                <input type='password' onChange={(e) => this.setState({ oldPwd: e.target.value })} />
            </div>
            <div>
                <label>{T('New Password')}</label>
                <input type='password' onChange={(e) => this.setState({ newPwd: e.target.value })} />
            </div>
            <div>
                <label>{T('New Password Again')}</label>
                <input type='password' onChange={(e) => this.setState({ newPwd2: e.target.value })} />
            </div>
            <div className='submit-div'><button type='button' className='btn btn-primary' onClick={this.submit} >{T('Submit')}</button></div>
            <div className='errors'>
                {this.errors.length  > 0 && this.state.submitCliked && (
                    <>
                    <div>{T('Please correct errors to continue')}:</div>
                    <ul>
                        {this.errors.map((err, idx) => <li key={idx}>{err}</li>)}
                    </ul>
                    </>
                )}
            </div>
            {this.state.done && (
                <div className="alert alert-success" role="alert"> {T('Reset password is done. Thank you.')}</div>)}
            {this.state.serverError &&(
                <div className="alert alert-warning" role="alert"> {this.state.serverError}</div>
                )}
        </div>
                </>
        )
    }

}
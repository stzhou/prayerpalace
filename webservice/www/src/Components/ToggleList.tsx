import * as React from "react"

export interface ToggleListProps {
    items: ToggleListItem[],
    selected: number[],
    onSelectionChange: (keys: number[]) => void,
    classes:string[],
}
export interface ToggleListItem{
    key:number,
    text:string,
    data?: any
}
interface State {
}
export class ToggleList extends React.Component<ToggleListProps, State> {
    constructor(props:ToggleListProps, state:State){
        super(props,state);
    }
    toggleSelection=(key:number)=>{
        const selected_new = (this.props.selected && this.props.selected.slice()) || [];
        const foundIdx = selected_new.indexOf(key);
        if (foundIdx === -1) {
            selected_new.push(key);
            this.props.onSelectionChange(selected_new);
        } else {
            selected_new.splice(foundIdx, 1);
            this.props.onSelectionChange(selected_new);
        }
    }
    render(){
        return (
            <div className={`my-toggle-list-container ${this.props.classes.join(' ')}` }>
                <ul className='my-toggle-list'>
                    {
                        this.props.items.map((p) => {
                            const isSelected = this.props.selected && this.props.selected.indexOf(p.key) > -1;
                            return (<li key={p.key} onClick={(e)=>this.toggleSelection(p.key)} className={isSelected?"selected":""}>
                                <span className='flag'>
                                    {
                                        isSelected &&
                                        (<span className="glyphicon glyphicon-star" aria-hidden="true"></span>)
                                    }
                                </span>
                                <span className='content'>{p.text}</span>
                            </li>)
                        })
                    }
                </ul>
            </div>
        )
    }
}
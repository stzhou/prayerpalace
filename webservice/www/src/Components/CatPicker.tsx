import * as React from "react"
import * as Store from "../indexStore"
interface Props {
    cats: Store.Cat[],
    val:number,
    onCatChange: (catId: number) => void;
}
interface State {

}

export class CatPicker extends React.Component<Props, State> {
    constructor(props:Props, state:State){
        super(props,state);
    }
    onCatChange=(evt)=>{
        const catID = parseInt(evt.target.value);
        this.props.onCatChange(catID);
    }
    render(){
        return (
            <select onChange={this.onCatChange} value={this.props.val} className="prp-cat-picker">
                { this.props.cats.map((c)=>
                    <option key={c.catID} value={c.catID}>{c.catName}</option>
                )}
            </select>
        )
    }
}
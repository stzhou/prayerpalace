import * as React from "react"
import  {KDatePicker} from "./KDatePicker"
import {T} from "t-i18n"
interface Props{
    sessionID: number;
    hasSignedup: boolean;
    SignedupToDate: Date|null;
    signup:(sessionID: number, date:Date|null)=>void;
    unsign:(sessionID)=>void;
    canSignup:boolean;
}
interface State {
    selectedDate:Date|null
}

export class Signup extends React.Component<Props, State> {
   /*onDateChange = (evt:any)=>{
       const strDate = evt.target.value;
       this.signupToDate = new Date(strDate.replace('-', '/'));
   }*/
   //isignupToDate: Date|null;
   constructor(props:Props, state:State){
       super(props,state)
       this.state = {
           selectedDate: this.props.SignedupToDate
       }
   }
   componentWillReceiveProps(nextProps:Props, nextState:State){
        this.setState({
            selectedDate: nextProps.SignedupToDate
        });
   }

   renderSign = ()=>{
        return (
            <>
                <div>{T('I want to singup until')}</div>
                <div>
                    <KDatePicker minDate={new Date()} value={this.state.selectedDate} onValueChange={(val) => this.setState({selectedDate:val})} />
                    <span className="glyphicon glyphicon-remove-sign remove-date" aria-hidden="true" onClick={(e) => this.setState({ selectedDate: null })}>&nbsp;</span>
                </div>
            {
                this.props.canSignup &&
                <div className="to-right"><button className="btn btn-success btn-sm" onClick={(e) => this.props.signup(this.props.sessionID, this.state.selectedDate)}>{T('Sign me up')}</button></div>
            }
            </>
        )
   }
   renderUnsign= ()=>{
       const signedupToDate = this.props.SignedupToDate;
       let effectiveFmt = "";
       if (signedupToDate) {
           effectiveFmt = signedupToDate.toLocaleDateString()
       }
       return (
           <>
               <div>{signedupToDate ? ` ${T('I signed up until')} ` : ""} {effectiveFmt} </div>
           {
               this.props.canSignup &&
               <div className="to-right"> <button className="btn btn-warning btn-sm" onClick={(e) => this.props.unsign(this.props.sessionID)}>{T('Delete')}</button></div>
           }
           </>
       )
   }
   render(){
       if (this.props.hasSignedup) {
           return this.renderUnsign();
       }
       else {
           return this.renderSign();
       }
  }
}
import * as React from "react"
import * as Store from "../indexStore"
import { BehaviorSubject } from "rxjs-es/BehaviorSubject"
import { Subscription } from "rxjs-es/Subscription"
import {T} from "t-i18n"
interface State {
   leaders: Store.Leader[],
   selectedLeaders:number[]|null
   permissions:string[]
}
interface Props {
    UserIDs: number[],
    OnUserIDsChanged: (userIDs: number[]) => void;
}

export class LeaderList extends React.Component<Props, State> {
    constructor(props:Props, state:State){
        super(props, state);
        this.state = {
            leaders:[],
            selectedLeaders:props.UserIDs||[],
            permissions:[]
        };
    }
    maxLeaders:number=2;
    leaderSubscription: Subscription;
    permissionSubscription: Subscription;
    componentWillMount(){
        this.leaderSubscription = Store.store.Leaders.subscribe((data) => {
            if (data && data.length){
                data.sort(function(a:any,b:any){return a.name - b.name});
            }
            this.setState({ leaders: data });
        });
        this.permissionSubscription = Store.store.Permissions.subscribe((data)=>{
            this.setState({permissions:data})
        })
    }
    componentWillReceiveProps(nextProps:Props, nextState:State){
        this.setState({
            selectedLeaders:nextProps.UserIDs||[]
        });
    }
    onOptionClicked = (evt:any)=> {
        if (this.state.permissions.indexOf("Assign-Leader")===-1){
            window.setTimeout(()=>{
                alert(T("You do not have permission to change leader."));
            },0);
            return;
        }
        const id = parseInt(evt.target.value);
        if (id==0) return;
        const selected = (this.state.selectedLeaders && this.state.selectedLeaders.slice()) ||[];
        const findIdx = selected.findIndex((p) => p == id);
        if (findIdx > -1) {
            //delete
            selected.splice(findIdx, 1);
            this.setState({
                selectedLeaders: selected
            }, () => {
                this.sendOptionsToProps(this.state.selectedLeaders);
            });
        } else {
            //insert
            if (selected.length >= this.maxLeaders) {
                selected.splice(this.maxLeaders - 1, 1, id)
            } else {
                selected.push(id);
            }
            this.setState({
                selectedLeaders: selected
            }, () => {
                this.sendOptionsToProps(this.state.selectedLeaders);
            })
        }
    }
    sendOptionsToProps(leaders:number[]){
        if (!leaders || leaders.length==0){
            this.props.OnUserIDsChanged(null);
        }else {
            this.props.OnUserIDsChanged(leaders)
        }

    }
    componentWillUnmount(){
        this.leaderSubscription.unsubscribe();
        this.permissionSubscription.unsubscribe();
    }
    render(){
        return <div className='leader-picker'>
            <select className="leader-picker-select" onChange={this.onOptionClicked} value={0} >
               <option value={0}>  </option>
            {
                this.state.leaders.map(leader => {
                    const selected = this.state.selectedLeaders.find(p => p == leader.id);
                    return (
                        <option key={leader.id} value={leader.id}> {leader.name} </option>
                    )
                })
            }
            </select>
            <div className='leader-selected-items'>
                {
                    this.state.selectedLeaders.map(
                        (leader) => {
                            const leaderObj = this.state.leaders.find(p=>p.id==leader)
                            return (
                                <span className="label label-default" data-id={leader} key={leader}>{leaderObj && leaderObj.name}</span>
                            )
                    })

                }
                { (!this.state.selectedLeaders || this.state.selectedLeaders.length==0) &&
                    <span className="label label-warning">({T("No Leader Assigned")})</span>
                }

            </div>
        </div>
    }
}
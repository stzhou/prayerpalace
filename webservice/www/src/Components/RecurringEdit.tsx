import * as React from "react"
import * as Store from "../indexStore"
import * as utils from "../utils"
import {T} from "t-i18n"

export interface Props {
    recurring: null |{
        mode:number,
        value: null | number[]
    }
    onChange: (recurring:
        null | {
            mode: number,
            value: null | number[]
        }
    ) => void;
}

interface State {
    newMode: number | null,
    newValue: null | Array<number>,
}
export class RecurringEdit extends React.Component<Props, State> {
    constructor(props:Props, state:State){
        super(props,state);
        const hasRecurring = this.props.recurring !=null;
        this.state = {
            newMode: hasRecurring ? this.props.recurring.mode : 0,
            newValue: hasRecurring ? this.props.recurring.value : null
        };
        this.generatewkdData();
        this.generateDayOfMonth();
    }
    componentWillReceiveProps(nextProps: Props, nextState: State) {
        if (nextProps.recurring !== this.props.recurring) {
            this.setState({
                newMode: nextProps.recurring ? nextProps.recurring.mode : 0,
                newValue: nextProps.recurring ? nextProps.recurring.value : null
            });
        }
    }
    generatewkdData=()=>{
        this.wkdData = [];
        this.wkdData.push({id:0, value:T("No Selection")})
        const ordinals=["",T("1st"),T("2nd"),T("3rd"),T("4th"),T("5th")];
        const wkdNames=[T("Sun"),T("Mon"),T("Tue"),T("Wed"),T("Thu"),T("Fri"),T("Sat")];
        let id=1;
        for(let i=1;i<=5;i++){
            for(let j=0;j<7;j++){
                this.wkdData.push({
                    id:id,
                    value:`${ordinals[i]} ${wkdNames[j]}`
                })
                id++;
                if(id > 31) return;
            }
        }
    }
    generateDayOfMonth = () => {
        this.dayOfMonth = new Array(32);
        for(let i=0;i<32;i++){
            this.dayOfMonth[i]= i
        }
    }
    modeOptionData:Array<{id:number, value:string}> = [{"id":0,"value":T("No Recurring")},{"id":1,"value":T("Day of Month")},
            {"id":2,"value":T("By Week")},{"id":3,"value":T("By Biweek")},{"id":4,"value":T("By Weekdays")}];
    wkdData:Array<{id:number, value:string}>=[];
    dayOfMonth:number[];
    onModeChange = (evt)=>{
       const val=evt.target.value;
       const _newMode = parseInt(val);
       this.setState({
            newMode: _newMode,
       })
        if (_newMode == 2 || _newMode == 3) {
           this.setState({
               newValue:null
           },()=>{
                //if new mode is 2,3 submit changes, no need to wait for Newvalue.
                this.props.onChange({
                    mode:this.state.newMode,
                    value:this.state.newValue
                });
           });
       }else if(_newMode==0){
            this.props.onChange(null);
       }else {
           //dealing with array value: mode 1 and 4
           if (this.state.newValue == null){
               this.setState({ newValue:[0] })
           }
       }
    }
    onDayOfMonthValueChange=(evt)=>{
       const val= parseInt(evt.target.value);
       this.setState({
           newValue:[val]
       }, () => {
           if (this.state.newValue[0] > 0) {
               this.props.onChange({
                   mode: this.state.newMode,
                   value: this.state.newValue
               });
           }else{
               this.onModeChange({target:{
                   value:"0"
               }})
           }
       });
    }

    //collect multiple selections:
    onDayOfWeekValueChange = (evt)=>{
        const val = parseInt(evt.target.value);
        if(val==0){
            this.onModeChange({
                target: {
                    value: "0"
                }
            });
            return;
        }
        let _newValue_clone = this.state.newValue.slice();
        //if doesn't have it, add it, else remove it:
        const findIdx = _newValue_clone.indexOf(val);
        if (findIdx === -1) {
            //_newValue = [val, ..._newValue];
            _newValue_clone.push(val);
        } else {
            _newValue_clone.splice(findIdx, 1);
        }
        this.setState({
            newValue: _newValue_clone
        }, () => {
            this.props.onChange({
                mode: this.state.newMode,
                value: this.state.newValue.filter(p => p > 0),
            });
        });
    }
    /* Mode-1: Day of Month;
    Mode-2: Week;
    Mode-3: BiWeek;
    Mode-4: sequenced-weekday:
    */
    render(){

        return (
            <div className="recurring-editor">
                <select value={this.state.newMode} onChange={this.onModeChange} className='mode-picker'>
                    {
                        this.modeOptionData.map(m =>
                            <option key={m.id} value={m.id}>{m.value}</option>
                        )
                    }
                </select>
                {this.state.newMode==0 &&(<div>No Recurring</div>)}
                {this.state.newMode == 1 && (
                    <select className="day-picker" onChange={this.onDayOfMonthValueChange} value={this.state.newValue[0]} >
                        {
                            this.dayOfMonth.map(d =>
                                <option key={d} value={d}>{d==0?" ":d}</option>
                            )
                        }
                    </select>
                )}
                {this.state.newMode == 4 && (
                    <select className="day-picker" onChange={this.onDayOfWeekValueChange}>
                        {
                            this.wkdData.map(p =>
                                <option key={p.id} value={p.id}>{p.value}</option>
                            )
                        }
                    </select>
                )}
                {this.state.newMode == 4 && (
                    <div>
                        Selected Day of Weeks: {
                            this.state.newValue.map((val) => {
                                if (val==0) return;
                                const wkdData = this.wkdData.find(p => p.id == val);
                                return <span key={val} className="badge">{wkdData && wkdData.value}</span>
                            })
                        }
                    </div>
                )}

            </div>
        )
    }
}
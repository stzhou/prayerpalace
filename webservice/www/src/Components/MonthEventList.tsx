import * as React from "react"
import * as Store from "../indexStore"
import { PrayerSession } from "../indexStore";
import * as utils from "../utils"
import {T} from "t-i18n"

interface Props {
    curDate: Date,
    monthSessions:Store.PrayerSession[];
    onDaySelected:(day:number)=>void;
    categories:Store.Cat[];
    //leadersLookup: Store.Leader[];
    signupCount:Store.SignupCount
}

interface State {
}

export class MonthEventList extends React.Component<Props, State> {
    //this widget aggregate events by date and
    constructor(props:Props, state:State){
        super(props, state);
        this.fillSlot(props);
    }
    componentWillReceiveProps(nextProp:Props, nextState:State){
        this.fillSlot(nextProp)
    }
    dayOfMonthSlots: Array<Array<PrayerSession>> = new Array(32);
    fillSlot = (props:Props)=>{
        const sessions = props.monthSessions;
        this.dayOfMonthSlots = new Array(32);
        sessions.forEach((s)=>{
            const day = s.date.getDate();
            if (Array.isArray(this.dayOfMonthSlots[day])==false){
                this.dayOfMonthSlots[day] = [];
            }
            this.dayOfMonthSlots[day].push(s);
        });
        //todo: we have to calculate localtime's from hh and to hh
    };
    private getCatName(catID:number){
        if (!this.props.categories|| this.props.categories.length==0){
            return "";
        }else{
            const cat = this.props.categories.find(p=>p.catID==catID);
            return cat ? cat.catName : "";
        }
    }
    onDaySelected = (event:any, date:Date)=>{
        event.preventDefault();
        event.stopPropagation();
        this.props.onDaySelected(date.getDate());
    }
    /*private getLeaderName(leaderIDs: number[] | null){
        if (null==leaderIDs || leaderIDs.length==0) return "";
        const ret = [];
        const leaders = this.props.leadersLookup.filter((p) => leaderIDs.indexOf(p.id) !== -1)
        return leaders.map(p => p.name).join(',');
    }*/

    render(){
        let filledSlots = this.dayOfMonthSlots.filter(p => Array.isArray(p));
        return <div className="prp-event-month-view-container">
            {filledSlots.map((slot, idx) => (
                <div className="prp-event-month-view" key={idx}>
                    <div className='header' onClick={(e)=>{this.onDaySelected(e, slot[0].date)}}>{slot[0].date.toLocaleDateString()}</div>
                    <ul>
                        {
                            slot.map((ps) => {
                                const count = this.props.signupCount[ps.sessionID];
                                const sCount = (count || 0).toString();
                                return <li key={ps.sessionID}><span className="event-time">{ utils.HHtoampm(ps.fromHH)} {T('To')} { utils.HHtoampm(ps.toHH)}</span><span className='event-title'>{ps.title}</span>
                                <span className="badge">{sCount}</span>
                                <span className='event-cat'>{this.getCatName(ps.prayerCatID)}</span> </li>
                            })
                        }
                    </ul>
                </div>
            ))}
        </div>
    }
}
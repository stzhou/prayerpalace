import * as React from "react"

interface Props{
    onSelected: (hour: number) => void;
    min: number,
    max: number,
    val:number
}
interface Option {
    id: number,
    name: string
}
interface State {
}
export class HourPicker extends React.Component<Props, State> {
    constructor(props:Props, state:State){
        super(props, state);
        this.items = [];
        for(var i=0;i<24;i++){
            let AMPM = 'AM', offset=0;
            if (i > 11) {
                AMPM = 'PM'
            }
            if(i > 12){
                offset = -12
            }
            let opt:Option = {
                id:i,
                name: `${i + offset} ${AMPM}`
            }
            this.items.push(opt);
        }
        const idxOfVal = this.items.findIndex((p)=>p.id== this.props.val);
        this.state={};
    }
    onChange = (evt:any)=>{
        const id = parseInt(evt.target.value);
        const item = this.items.find((p) => p.id === id);
        this.props.onSelected(id);
    };
    items:Option[]
    render() {
        return (<select onChange={this.onChange} value={this.props.val}>
            {
                this.items.filter(p=>
                    p.id >= this.props.min && p.id <= this.props.max
                ).map( (item)=>{
                    return (
                        <option key={item.id} value={item.id}>{item.name}</option>
                    );
                })
            }
        </select>)
    }
}

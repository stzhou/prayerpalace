import * as React from "react"

export interface MonthPickerProps{
    curDate: Date,
    behind: number,
    ahead: number
    onMonthPicked: (year: number, month: number) => void;
}
interface MonthItem {
    year:number,
    month:number,
    id: string
}
interface State {
    selected: MonthItem | null;
}

export class MonthPicker extends React.Component<MonthPickerProps, State> {

    constructor(props:MonthPickerProps, state:State){
        super(props,state);
        this.state = {
            selected: null
        }
    }

    componentWillMount(){
        this.makeMonthList(this.props.curDate, this.props.behind, this.props.ahead);
    }

    monthList: Array<MonthItem>;

    onChange = (event: any) => {
        const id = event.target.value;
        const item = this.monthList.find((p)=> p.id === id);
        this.setState({ selected: item }, () => {
            this.props.onMonthPicked(item.year, item.month);
        });
    }
    makeMonthList(curDate:Date, behind:number, ahead:number){
         //generate month list:
        const curYear = curDate.getFullYear();
        const curMonth = curDate.getMonth()+1;
        const listLen = behind + 1 + ahead;
        const monthList: Array<MonthItem> = [];
        for (let i = 0; i < listLen; i++) {
            let year = curYear;
            let m = curMonth - behind + i;
            if (m <= 0) {
                year = year - 1;
                m = m + 12;
            } else if (m > 12) {
                year = year + 1;
                m = m - 12;
            }
            monthList.push({
                year: year,
                month: m,
                id: `${year}-${m}`
            });
            if (m === curMonth) {
                this.setState({ selected: monthList[monthList.length - 1] })
            }
        }
        this.monthList = monthList
    }

    componentWillReceiveProps(props: MonthPickerProps) {
        const newYM = props.curDate.getFullYear() * 10000 + props.curDate.getMonth();
        const oldYM = this.props.curDate.getFullYear() * 10000 + props.curDate.getMonth();
        if (this.props.ahead != props.ahead ||
            this.props.behind != props.behind ||
            newYM != oldYM
        ) {
            this.makeMonthList(props.curDate, props.behind, props.ahead);
        }
    }

    shouldComponentUpdate(nextProps: MonthPickerProps, nextState:State){
        return (this.props.ahead != nextProps.ahead ||
            this.props.behind != nextProps.behind ||
            this.props.curDate.getFullYear() != nextProps.curDate.getFullYear() ||
            this.props.curDate.getMonth() != nextProps.curDate.getMonth() ||
            this.state.selected != nextState.selected
        )
    }

    render () {
        return (
            <select className='month-picker' value={this.state.selected.id} onChange={this.onChange}>
                {this.monthList.map(m=>(
                   <option key={m.id} value={m.id}>{m.year+" "+m.month}</option>
                ))}
            </select>
        );
    }
}
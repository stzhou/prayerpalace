import * as React from "react"
import { Link } from "react-router-dom"
import { Subscription } from "rxjs-es/Subscription"

import * as Store from "../indexStore"
import {T} from "t-i18n"

interface State{
    menuShow:boolean;
    menuItems:Array<MenuItem>;
}
interface Props {
   subTitle:string;
   goBack:()=>void;
   enabled: boolean;
   goBackVisible:boolean;
   MenuItems?:Array<MenuItem>;
}
interface MenuItem {
    link:string;
    text:string;
    order:number;
}

export class PageHeader extends React.Component<Props, State> {
    constructor(props:Props, state:State){
        super(props,state);
        this.state = {
            menuShow: false,
            menuItems:[]
        }
        this.commonMenuItems = [];
        this.commonMenuItems.push({ text: T("Home"), link: "/index", order: 0 });
        this.commonMenuItems.push({ text: T("Change Password"), link: "/index/changepassword", order: 1 });
        this.commonMenuItems.push({ text: T("About"), link: "/index/about", order: 9998});
        this.commonMenuItems.push({ text: T("Log out"), link: "/index/logout", order: 9999 })
    }
    permissionSubscription:Subscription;
    componentWillMount(){
        this.permissionSubscription = Store.store.Permissions.subscribe((data)=>{
           if(data && data.length){
               if (data.indexOf("Edit-UserRole")!==-1){
                    const old_menuItems = this.state.menuItems.slice();
                    old_menuItems.push({
                        link:'/index/useradmin',
                        text: T('User Admin'),
                        order: 1
                    });
                    this.setState({menuItems: old_menuItems})
               }
           }
        });
    }


    addExtraMenus = (items: MenuItem[]) => {
        if (!items) return this.commonMenuItems;
        items.sort( (a, b)=>{return a.order - b.order});
        //never alter the common items itself
        const _common = this.commonMenuItems.slice()
        _common.splice(2,0, ...items);
        return _common;
    }
    commonMenuItems:Array<MenuItem> = [];
    mainTitle:string=T("Prayer Palace");
    onBackClick = (evt:any)=>{
        evt.preventDefault();
        evt.stopPropagation();
        if(this.props.enabled){
            this.props.goBack();
        }
    };
    onMenuClick = ()=>{
        this.setState({ menuShow: !this.state.menuShow });
    }
    render(){
        let menus = this.addExtraMenus(this.props.MenuItems);
            menus = this.addExtraMenus(this.state.menuItems);
        return (
            <>
            <header className="prp">
                <span className="glyphicon glyphicon-menu-hamburger prp-menu" aria-hidden="true" onClick={this.onMenuClick}></span>
                <nav><ul className='pager'>
                    {
                        this.props.goBackVisible && (
                            <li className={`previous ${this.props.enabled ? "" : "disabled"}`}>
                                <a href='#' onClick={this.onBackClick}><span className="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
                                {T('Back')} </a>
                            </li>
                        )
                    }
                </ul></nav>
                <h1>{this.mainTitle}</h1> <h2>{this.props.subTitle}</h2>
            </header>
            <header className='prp-fake'></header>
            {this.state.menuShow &&(
                <div className="page-header-menu">
                    {
                        menus.map((m)=>(
                            <div key={m.link} className='menu-item'><Link to={m.link}>{m.text}</Link></div>
                        ))
                    }
                </div>
            )}
            </>
        );
    }
}
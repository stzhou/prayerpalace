import * as React from "react"
//import * as kendo from 'kendo-ui-core/js/kendo.datepicker';
import 'kendo-ui-core/js/kendo.core';
import 'kendo-ui-core/js/kendo.datepicker';
import 'kendo-ui-core/js/cultures/kendo.culture.zh.js'
import 'kendo-ui-core/js/cultures/kendo.culture.zh-TW.js'

import $ from "jquery"

import * as trans from "../translate"

interface Props{
    value:Date|null;
    minDate:Date;
    maxDate?:Date;
    onValueChange:(val:Date)=>void;
}
interface State {
}

export class KDatePicker extends React.Component<Props, State> {
    constructor(props:Props, state:State){
        super(props, state);
        this.state = {
            valueInternal: props.value
        };
        const lan = trans.GetLanOption();
        (window as any).kendo.culture(lan);
    }
    inputContainer:HTMLSpanElement;

    onClick(event:any){
        event.preventDefault();
        event.stopPropagation();
        return false;
    }
    componentDidMount() {
        //here I call kendo to init the widget.
        const that = this;
        let config = {
            depth:'year',
            start:'month',
            value: this.props.value,
            min: this.props.minDate,
            change:function(){
                const val = this.value();
                that.props.onValueChange(val);
            }
        }
        if (this.props.maxDate){
            Object.assign(config,{
                max: this.props.maxDate
            });
        }
        const input: HTMLInputElement = document.createElement("input");
        this.inputContainer.appendChild(input);
        $(input).kendoDatePicker(config);
    }
    shouldComponentUpdate(){
        return false;
    }
    componentWillUnmount(){
        //Kendo should be wrapped in a container, and cleanup the whole container by setting innerHMTL="".
        //or react will crash.
        const input = $(this.inputContainer).find("input");
        if (!input.data('kendoDatePicker')) return;
        input.data('kendoDatePicker').destroy();
        input.remove();
        this.inputContainer.innerHTML = "";
    }
    componentWillReceiveProps(nextProps:Props, nextState:State){
        const input = $(this.inputContainer).find("input");
        input.data('kendo-date-picker').value(nextProps.value);
    }
    render(){
        return (
            <span className='my-k-date-picker' ref={(e) => this.inputContainer = e} onClick={this.onClick} >
            </span>
        )
    }
}
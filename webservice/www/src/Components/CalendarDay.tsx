import * as React from "react"

interface CalenderDayProps {
    date: Date,
    day: number,
    busy:boolean,
    lead:boolean,
    signup:boolean,
    onDayClick :(day:number)=>void;
}
interface State {
    date: Date
}
export class CalendarDay extends React.Component<CalenderDayProps, State> {
    constructor(props:CalenderDayProps, state:State){
        super(props, state);
    }
    onDayClick = (day:number, event:any)=>{
        event.preventDefault();
        event.stopPropagation()
        this.props.onDayClick(day);
    }
    render(){
        const cls=[];
        if(this.props.busy){
            cls.push('busy');
        }
        const both = this.props.signup && this.props.lead;
        const either = this.props.signup || this.props.lead;
        const leadClass = both? "lead both":"lead";
        const signupClass = both? "signup both":"signup";
        return (
        <td className={cls.join(' ')} data-day={this.props.day} onClick={e=>this.onDayClick(this.props.day,e)} >
            <div className={either?"text hl":"text"}>{this.props.day}</div>
            {this.props.busy && <div className='busy'>&middot;</div>}
            {this.props.lead && (<div className={leadClass}></div>)}
            {this.props.signup && <div className={signupClass}></div>}
        </td>)
    }
}
import * as React from "react"

export interface ToggleBtnProps {
    onValueChange: (val:boolean) => void,
    val:boolean;
    classes:string[],
}
interface State {
}
export class ToggleBtn extends React.Component<ToggleBtnProps, State> {
    constructor(props:ToggleBtnProps, state:State){
        super(props, state);
    }
    toggleValue = (event:any)=>{
        event.preventDefault();
        event.stopPropagation();
        this.props.onValueChange(!this.props.val);
    }
    render(){
        return (
            <div className={`my-toggle-btn ${this.props.classes.join(' ')} ${this.props.val ? "on" : "off"}`} onClick={this.toggleValue} style={{ display: 'inline-block'}}>
            &nbsp;
            </div>
        )
    }
}
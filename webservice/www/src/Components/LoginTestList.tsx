import * as React from "react"

interface Props{
    userIDs: number[],
    onUserIDSelected: (userID: number)=>void
}
interface State{

}
export class LoginTestList extends React.Component<Props, State> {
    onUserSelect= (userId, event)=>{
        this.props.onUserIDSelected(userId);
    }
    render() {
        const userIDs = this.props.userIDs;
        return (
            <div id='login-ids'>
                Select one test user to login:
                <ul>
                    {
                         userIDs.map((id)=>{
                            return <li key={id} onClick={e => this.onUserSelect(id, e)}>{id}</li>
                        })
                    }
                </ul>
            </div>
        )
    }
}

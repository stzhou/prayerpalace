
import * as React from "react"
import {Redirect} from "react-router-dom"
import * as Store from "../indexStore"
import { Subscription } from "rxjs-es/Subscription";

import {PageHeader} from "./PageHeader"
import {ToggleList, ToggleListItem} from "./ToggleList"
import {ToggleBtn} from "./ToggleBtn"
import {T} from "t-i18n"

interface Props {

}
interface State {
    search: string,
    users:Store.AdmUser[],
    roles:Store.Role[]
    redirect:boolean,
    selectedUser:number,
}

export class UserAdmin extends React.Component<Props, State> {
    constructor(props:Props, state:State){
        super(props, state);
        this.state = {
            users:[],
            roles:[],
            search:'',
            redirect:false,
            selectedUser:0
        }
    }
    admUersSubscription :Subscription;
    componentWillMount(){
        this.admUersSubscription = Store.store.AdmUsers.subscribe((data)=>{
            this.setState({
                users: this.orderUsers(data && data.users),
                roles: (data && data.roles) || []
            });
        })
        Store.store.GetAdmUsers()
    }
    orderUsers = (users: Store.AdmUser[])=>{
        if (!users || !users.length) return [];
        function compare(a,b, prop){
            if(a[prop]===b[prop]) return 0;
            if(a[prop]< b[prop]) return -1;
            return 1;
        }
        return users.sort(function(a,b){
            let _compare = 0;

            _compare = compare(a, b, 'nickName');
            if (_compare !== 0) return _compare;

            _compare = compare(a, b, 'email');
            if (_compare !== 0) return _compare;

            return 0;
        });
    }
    componentWillUnmount() {
        this.admUersSubscription.unsubscribe();
    }
    filterBySearch = (users: Store.AdmUser[]) :Store.AdmUser[]=>{
        if (!this.state.search){
            return users;
        }
        const _search = this.state.search.toLowerCase();
        return users.filter((p)=>
            (p.email && p.email.indexOf(_search) > -1) ||
            (p.nickName && p.nickName.toLowerCase().indexOf(_search) > -1) ||
            (p.userName && p.userName.toLowerCase().indexOf(_search)> -1) ||
            (p.phone && p.phone.toLowerCase().indexOf(_search)>-1)
        )
    }
    toggleUserDetail = (userID:number)=>{
        const selectedID = this.state.selectedUser;
        if (selectedID == userID) {
            this.setState({ selectedUser: 0 });
        } else {
            this.setState({ selectedUser: userID});
        }
    }

    changeUserRoles = (userID:number, roleIDs:number[])=>{
        const userIdx = this.state.users.findIndex(p => p.userID == userID);
        if(userIdx <0 ) return;
        const users_new = this.state.users.slice();
        const user_new = Object.assign({}, users_new[userIdx], {roles:roleIDs||[], dirty:true});
        users_new.splice(userIdx,1, user_new);
        this.setState({users: this.orderUsers(users_new)});
    }

    changeUserActive = (userID:number, active:boolean)=>{
        const userIdx = this.state.users.findIndex(p => p.userID == userID);
        if(userIdx <0 ) return;
        const users_new = this.state.users.slice();
        const user_new = Object.assign({}, users_new[userIdx], {isActive:active, dirty:true});
        users_new.splice(userIdx,1, user_new);
        this.setState({users: this.orderUsers(users_new)});
    }
    saveUser = (event: any, userID:number) =>{
        event.preventDefault();
        event.stopPropagation();
        const userIdx = this.state.users.findIndex(p => p.userID == userID);
        if(userIdx <0 ) return;
        const payload = this.state.users[userIdx];
        Store.store.UpdateAdmUsers(payload, (result)=>{
            if (result.err) {
                alert(T("Sorry, update failed"));
            } else {
                delete payload['dirty'];
                const users_new = this.state.users.slice();
                users_new.splice(userIdx,1, payload);
                this.setState({users: this.orderUsers(users_new)});
            }
        });
    }
    onGoback = ()=>{
        this.setState({redirect:true});
    }

    cancelClick = (event:any)=>{
        event.preventDefault();
        event.stopPropagation();
        return false;
    }

    render(){
        if (this.state.redirect) {
            return <Redirect to='/index' />
        }

        let users = this.state.users;
        users = this.filterBySearch(users);
        const roleDataSource = this.state.roles.map<ToggleListItem>(p=>{
            return {
                key:p.roleID,
                text:p.roleName
            }
        });
        return (
            <>
                <PageHeader enabled={true} subTitle={T("User Admin")} goBackVisible={true} goBack={this.onGoback} />
                <div className="input-group">
                    <span className="input-group-addon" >
                        <span className="glyphicon glyphicon-search" aria-hidden="true"></span>
                    </span>
                    <input type="text" className="form-control" placeholder="search" onChange={(e)=>{this.setState({search:e.target.value})}} />
                </div>
                <ul className='user-list'>
                    {
                        users.map((u)=>{
                            //test user has role that is not 3.
                            const isSuperuser = u.roles && u.roles.find(p => p !== 3);
                            const classNames=[];
                            if(isSuperuser){
                                classNames.push('super');
                            }
                            if(!u.isActive){
                                classNames.push('inactive');
                            }
                            if(u['dirty']){
                                classNames.push('dirty');
                            }
                            return (
                            <li key={u.userID} user-id={u.userID} className={classNames.join(' ')} onClick={(e)=>this.toggleUserDetail(u.userID)}>

                                <div className={`list-item ${classNames.join(' ')}`}>
                                    {u['dirty'] &&
                                        <span className="glyphicon glyphicon-flag" aria-hidden="true"></span>
                                    }&nbsp;
                                    {u.nickName || u.userName}
                                </div>
                                {
                                    this.state.selectedUser==u.userID && (
                                        <div className='detail edit' onClick={this.cancelClick} >
                                            <div><label>{T("Name")}</label>{u.nickName}</div>
                                            <div><label>{T("User Name")}</label>{u.nickName}</div>
                                            <div><label>{T("Email")}</label> {u.email}</div>
                                            <div><label>{T("Phone")}</label> {u.phone} </div>
                                            <div><label>{T("Since")}</label> {u.created.toLocaleDateString()} </div>
                                            <div><label>{T("Active")}</label> <ToggleBtn val={u.isActive} classes={['user-is-active']}
                                                     onValueChange={(val)=>{this.changeUserActive(u.userID, val)}} /> </div>
                                            <div>
                                                <label>{T("Roles")}</label>
                                                <ToggleList items={roleDataSource}
                                                    selected={u.roles}
                                                    classes={['role-list']}
                                                    onSelectionChange={(newRoles)=>{this.changeUserRoles(u.userID, newRoles) }}
                                                />
                                            </div>
                                            <div>
                                                {u['dirty'] && (
                                                <button type="button" className="btn btn-success save" onClick={(e)=>this.saveUser(e, u.userID)}>{T("Save")}</button>
                                                )}
                                            </div>
                                        </div>
                                    )
                                }
                            </li>)
                        })
                    }
                </ul>
            </>
        )

    }
}
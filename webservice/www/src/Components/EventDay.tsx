import * as React from "react"
import { MonthPicker } from "./MonthPicker"
import { BehaviorSubject } from "rxjs-es/BehaviorSubject"
import * as Store from "../indexStore"
import {Signup} from "./Signup"
import * as utils from "../utils"
import {T} from "t-i18n"
interface Props{
    //userID:number;
    curDate:Date;
    curDaySessions:Store.PrayerSession[];
    curDayMyevents:Store.MyEvent[];
    signup: (sesisonID:number, effectiveTo:Date)=>void;
    cancelSignup:(sesisonID:number) =>void;
    goBack:()=>void;
    leadersLookup:Store.Leader[];
    canSignup:boolean;
}
interface State{
  selectedSessionID: number|null
}

interface SessionItem extends Store.PrayerSession {
    isSigned:boolean,
    effectiveTo: Date|null
}
export class EventDay extends React.Component<Props, State> {
    constructor(props:Props, state:State){
        super(props, state)
        this.state = {
            selectedSessionID:null
        };
    }
    toggleSessionDetail = (sessionID:number)=>{
        if (this.state.selectedSessionID === sessionID){
            this.setState({selectedSessionID:null});
        }else{
            this.setState({selectedSessionID:sessionID});
        }
    };
    signup= (sessionID: number, effectiveTo:Date)=>{
        this.props.signup(sessionID, effectiveTo)
    };

    unsign=(sessionID:number)=>{
        this.props.cancelSignup(sessionID);
    };
    cancelClick = (event:any)=>{
        event.preventDefault();
        event.stopPropagation();
        return false;
    }
    private getLeaderName(leaderIDs: number[] | null){
        if (null==leaderIDs || leaderIDs.length==0) return "";
        const ret = [];
        const leaders = this.props.leadersLookup.filter((p) => leaderIDs.indexOf(p.id) !== -1)
        return leaders.map(p => p.name).join(',');
    }
    render(){
        const sessions = this.props.curDaySessions.sort(function(a,b){ return a.fromHH - b.fromHH});
        const dicMyEvents: { [id: number]: Store.MyEvent } = {};
        const date=this.props.curDate;
        this.props.curDayMyevents.forEach((my)=>{
            dicMyEvents[my.sessionID] = my;
        })
        const ssForCurDay = sessions.map((ps) => {
            const si = ps as SessionItem;
            //assume myevent is already filtered to today's list
            if (dicMyEvents[si.sessionID]){
                return Object.assign({}, si, {
                    isSigned: true,
                    effectiveTo: dicMyEvents[si.sessionID].effectiveTo
                })
            }else{
                return si;
            }
        });
        return (
            <>
            <h4 className="event-date"><span className="label label-primary ">{date.toLocaleDateString()}</span></h4>
            <ul className="day-session-list">
                {
                    ssForCurDay.map((ps)=>{
                        const liClassName = [];
                        liClassName.push("session-list-item");
                        const showDetail= ps.sessionID === this.state.selectedSessionID;
                        if (showDetail){
                            liClassName.push("detail")
                        }
                        if(ps.isSigned){
                            liClassName.push("signup")
                        }
                        return (
                            <li key={ps.sessionID} className={liClassName.join(' ')}
                            onClick={(e)=>this.toggleSessionDetail(ps.sessionID)}>
                                <div className="session">
                                    <span className="s1">{ps.title}</span>
                                    <span className="s2">{utils.HHtoampm(ps.fromHH)}</span>
                                    <span className="s3">-</span>
                                    <span className="s2">{utils.HHtoampm(ps.toHH)}</span>
                                </div>
                                {!showDetail?"":(
                                    <div className="detail" onClick={this.cancelClick}>
                                        <div className="desc">{ps.desc || ps.title} {T("by leader(s)")}: {this.getLeaderName(ps.leaders)} </div>
                                        <Signup sessionID={ps.sessionID}  hasSignedup={ps.isSigned}
                                        SignedupToDate={ps.effectiveTo}
                                        signup={this.signup}
                                        unsign={this.unsign}
                                        canSignup={this.props.canSignup}
                                        />
                                    </div>
                                )}
                            </li>
                        )}
                    )
                }
            </ul>
            </>
        )
    }
}
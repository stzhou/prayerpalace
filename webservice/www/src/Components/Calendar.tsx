import * as React from "react"
import { CalendarDay } from "./CalendarDay"
import { MonthPicker } from "./MonthPicker"
import { BehaviorSubject } from "rxjs-es/BehaviorSubject"
import * as Store from "../indexStore"
import { Subscription } from "rxjs-es/Subscription";
import {EventDay} from "./EventDay"
import {PageHeader} from "./PageHeader"
import {withRouter} from "react-router-dom"
import * as utils from "../utils"
import { MonthEventList } from "./MonthEventList"
import {T} from "t-i18n"
const Header=({txt})=>(
    <td>{txt}</td>
);
const HeaderRow = ()=>{
    let wkdays = [T('SUN'), T('MON'), T('TUE'), T("WED"), T("THU"), T("FRI"), T("SAT")];
    return (
        <tr>
            {wkdays.map(
                (txt, idx)=>( <Header txt={txt} key={idx}/> )
            )}
        </tr>
    )
}
 // prayerSessions: BehaviorSubject<Array<Store.PrayerSession>>
interface State {
    date: Date,
    prayerSessions: Store.PrayerSession[],
    myevents: Store.MyEvent[],
    categories:Store.Cat[],
    leadersLookup:Store.Leader[],
    viewMode: "Month"|"Day",
    permissions:string[],
    signupCounts:Store.SignupCount,
}

export class Calendar extends React.Component<{}, State> {
    constructor(props:{}, state:State){
        super(props, state);
        this.state = {
            date: new Date(),// initial date will be today.
            prayerSessions:[],
            myevents:[],
            viewMode:"Month",
            categories:[],
            leadersLookup:[],
            permissions:[],
            signupCounts:[]
        };
        this.state.date.setHours(0,0,0,0);
        this.getFirstWeekday();
    }
    prayerSessionSubscription: Subscription;
    myeventSubscription: Subscription;
    catSubscription: Subscription;
    leadersSubscription:Subscription;
    permissionsSubscription:Subscription;

    signupCountSubscription:Subscription;

    componentWillMount() {
        this.prayerSessionSubscription = Store.store.PrayerSessions.subscribe((data) => {
            this.setState({ prayerSessions: data })
        });
        this.myeventSubscription= Store.store.Myevents.subscribe((data) => {
            console.log("@Calendartsx, myeventSubscription",data)
            this.setState({ myevents: data })
        });
        this.catSubscription= Store.store.Cats.subscribe((data)=>{
            this.setState({categories:data});
        });
        this.leadersSubscription = Store.store.Leaders.subscribe((data)=>{
            this.setState({leadersLookup:data})
        });

        this.permissionsSubscription = Store.store.Permissions.subscribe((data)=>{
            this.setState({permissions:data});
        });

        this.signupCountSubscription = Store.store.SignupCount.subscribe((data)=>{
            this.setState({signupCounts:data})
        });

        Store.store.GetLeaders();
        Store.store.GetPrayerSessions();
        Store.store.GetMyEvents();
        Store.store.GetCats();
        this.setState({viewMode:"Month"});
    }

    componentWillUnmount() {
        this.prayerSessionSubscription.unsubscribe()
        this.myeventSubscription.unsubscribe();
        this.catSubscription.unsubscribe();
        this.leadersSubscription.unsubscribe();
    }

    getFirstWeekday(){
        const firstDay = new Date(this.state.date.getTime());
        firstDay.setFullYear(firstDay.getFullYear(),firstDay.getMonth(), 1);
        const firstWeekDay = firstDay.getDay();
        return firstWeekDay;
    }
    getMaxDay() {
        const curDate = this.state.date;
        const lastDateOfMonth = new Date(curDate.getFullYear(), curDate.getMonth() + 1, 0);
        return lastDateOfMonth.getDate();
    }
    onMonthPicked = (year: number, month: number)=>{
        this.setState({
            date: new Date(year, month-1)
        });
    }
    onDaySelected = (day:number)=>{
        this.setState({viewMode:"Day"});
        const oldDate = this.state.date;
        let newDate = new Date(oldDate.getFullYear(), oldDate.getMonth(), day);
        this.setState({date: newDate});
    };
    unique = function<T>(arr: Array<T>){
        const dic:{[id:string]:boolean}={};
        const uniqueArr:Array<T>=[];
        arr.forEach( p=>{
            if (dic[p+""]===undefined){
                uniqueArr.push(p);
                dic[p+""]=true;
            }
        })
        return uniqueArr;
    }
    canSignup = ()=>{
        let curDateTimeNow = new Date();
        curDateTimeNow.setHours(0, 0, 0, 0);
        return this.state.date >= curDateTimeNow;
    }
    signup = (sessionID:number, effectiveTo:Date)=>{
        if (! this.canSignup()) {
            window.setTimeout(()=>{
                alert(T("Sorry, this session is already ended."));
            },0);
        } else {
            Store.store.UpsertMyevents({
                sessionID: sessionID,
                effectiveTo: effectiveTo,
                isDel: false
            });
        }
    };

    cancelSignup = (sessionID: number)=>{
        Store.store.UpsertMyevents({
            sessionID:sessionID,
            effectiveTo:null,
            isDel:true
        });
    };

    backToCalender = ()=>{
        this.setState({viewMode:"Month"});
    };

    toManageSession = ()=>{

    };

    render() {
        const canEditSession = this.state.permissions.indexOf('Edit-PrayerSession') >= 0;
        let curDate = new Date();
        curDate.setHours(0, 0, 0, 0);
        if (this.state.viewMode == "Month") {
            const days = [];
            const firstWkDay = this.getFirstWeekday();
            let daysCount = 0;
            const selectedYYMM = {
                localY: this.state.date.getFullYear(),
                localM: this.state.date.getMonth() + 1,
            };
            let busyDays = this.state.prayerSessions.filter((ps) => {
                const psDate = {
                    localY: ps.date.getFullYear(),
                    localM: ps.date.getMonth() + 1,
                    localD: ps.date.getDate()
                }
                return psDate.localM == selectedYYMM.localM &&
                    psDate.localY == selectedYYMM.localY &&
                    ps.isActive
            }).map(p => p.date.getDate());
            busyDays = this.unique(busyDays);

            let leadDays = [];
            let signupDays = this.state.myevents.filter((ps) => {
                const psDate = {
                    localY: ps.date.getFullYear(),
                    localM: ps.date.getMonth() + 1,
                }
                return psDate.localM == selectedYYMM.localM &&
                    psDate.localY == selectedYYMM.localY;
            }).map(p => p.date.getDate());
            signupDays = this.unique(signupDays);
            const maxDay = this.getMaxDay();
            for (let i = 0; i < 6; i++) {
                days.push([])
                for (let j = 0; j < 7; j++) {
                    if (i === 0 && j < firstWkDay) {
                        days[i].push(<Header txt={""} key={i + j} />);
                    } else {
                        daysCount++;
                        if (daysCount > maxDay) break;
                        const isBusy = busyDays.indexOf(daysCount) > -1;
                        const isLead = leadDays.indexOf(daysCount) > -1;
                        const isSignup = signupDays.indexOf(daysCount) > -1;

                        days[i].push(<CalendarDay
                            date={this.state.date}
                            day={daysCount}
                            busy={isBusy}
                            lead={isLead}
                            signup={isSignup}
                            key={i + j}
                            onDayClick={this.onDaySelected}
                        />)
                    }
                }
            }
            const curMonthSessions = this.state.prayerSessions.filter((ps) => {
                return ps.date.getMonth() == this.state.date.getMonth()
                && ps.date.getFullYear() == this.state.date.getFullYear()
                && (canEditSession||ps.isActive) && !ps.deleted
            });
            return (
                <>
                <PageHeader enabled={true} subTitle={T("Events")} goBackVisible={false} goBack={null} />
                <div className='calendar'>
                    <MonthPicker behind={3} ahead={6} curDate={curDate} onMonthPicked={this.onMonthPicked} />
                    <table>
                        <tbody>
                            <HeaderRow />
                            {days.map((wk, wkNum) =>
                                (<tr key={wkNum} data-wk={wkNum + 1}>
                                    {wk.map((d) => d)}
                                </tr>)
                            )}
                        </tbody>
                    </table>
                </div>
                <MonthEventList curDate={curDate} monthSessions={curMonthSessions} onDaySelected={this.onDaySelected} categories={this.state.categories}
                  signupCount={this.state.signupCounts}/>
                </>
            );
        }
        if (this.state.viewMode == "Day") {
            //get all sessions of that day:
            const curDaySessions = this.state.prayerSessions.filter((ps) => {
                return utils.datesAreEqualInLocal(ps.date, this.state.date) && ps.isActive && !ps.deleted
            });
            const curDayEvents = this.state.myevents.filter((evt) => {
                return utils.datesAreEqualInLocal(evt.date, this.state.date)
            });
            let dateUrlParam = `${this.state.date.getFullYear()}-${this.state.date.getMonth()+1}-${this.state.date.getDate()}`
            const Button = withRouter(({ history }) => (
                <button className='btn btn-default btn-sm btn-danger' type='button' onClick={() => history.push('/index/sessionedit/' + dateUrlParam)}>{T("Manage Sessions")}</button>
            ));
            return (
                <>
                  <PageHeader enabled={true} subTitle={T("Today's Events")} goBackVisible={true} goBack={this.backToCalender} />
                  <EventDay curDaySessions={curDaySessions} curDayMyevents={curDayEvents} signup={this.signup} cancelSignup={this.cancelSignup}
                  goBack={this.backToCalender} curDate={this.state.date} leadersLookup={this.state.leadersLookup}  canSignup={this.canSignup()}/>
                    { canEditSession && this.state.date >= curDate && <div className="to-right" style={{ marginTop: 10 }}> <Button /></div>}
                </>
            )
        }
    }
}

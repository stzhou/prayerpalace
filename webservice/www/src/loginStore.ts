//rxjs stuff
import { Observable } from 'rxjs-es/Observable';
import { BehaviorSubject } from "rxjs-es/BehaviorSubject"
import 'rxjs-es/add/observable/dom/ajax';
import 'rxjs-es/add/observable/combineLatest';
import 'rxjs-es/add/observable/forkJoin';
//import 'rxjs-es/add/operator/debounceTime';
import 'rxjs-es/add/operator/map';
//import 'rxjs-es/add/operator/startWith';
import 'rxjs-es/add/operator/filter';
//import 'rxjs-es/add/operator/switchMap';
import 'rxjs-es/add/operator/catch';

class Store {
    tryLogin(acc:string, pw:string, callback:(res:{ok:boolean, originalResponse:any})=>void){
        Observable.ajax.post("/api/login",
            JSON.stringify({
                account: acc,
                pwd: pw
            }),
            { 'Content-Type': "application/json" })
            .catch((err, caught) => {
                if (err) {
                    callback({ok:false, originalResponse:err});
                }
                return null;
            }).subscribe((res) => {
                callback({
                    ok:true,
                    originalResponse:res
                });
            });
    }
    resetPassword(account:string, callback:(res:{ok:boolean, originalResponse:any})=>void){
        Observable.ajax.post('/api/resetpwd', JSON.stringify({account:account}),{ 'Content-Type': "application/json" })
        .catch((err,caught)=>{
            if(err){
                callback({ok:false, originalResponse:err})
                return null;
            }
        }).subscribe((res)=>{
            callback({
                ok:true,
                originalResponse:res
            })
        })
    }

    registerUser(regForm:RegisterDTO, callback:(res:{ok:boolean, originalResponse:any})=>void){
        Observable.ajax.put("/api/user", JSON.stringify(regForm),{ 'Content-Type': "application/json" })
        .catch((err,caught)=>{
            if(err){
                callback({ok:false, originalResponse:err})
                return null;
            }
        }).subscribe((res)=>{
            callback({
                ok:true,
                originalResponse:res
            })
        });
    }
}

interface RegisterDTO {
    firstName: string,
    lastName: string,
    userName: string,
    email: string
    pwd: string
    tz: string
    lanid: number
}

const store = new Store();
export {store}